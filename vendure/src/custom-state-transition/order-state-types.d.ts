/* eslint-disable @typescript-eslint/no-unused-vars */
import { CustomOrderStates } from "@vendure/core";

declare module "@vendure/core" {
  interface CustomOrderStates {
    AwaitingArtwork: never;
    AwaitingPrepress: never;
    InProduction: never;
    AwaitingCollection: never;
    AttentionRequired: never;
  }
}
