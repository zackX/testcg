export interface Product {
  name: string;
  image: string;
  description: string;
  /**
   * Price in cents
   */
  price: number;
  currency: string;
  isInStock: boolean;
}

export interface StructuredData {
  "@context": "http://schema.org";
  "@type": "Product";
  name: string;
  image: string;
  description: string;
  offers: {
    "@type": "Offer";
    price: string;
    priceCurrency: string;
    priceValidUntil: string;
    availability: "InStock" | "SoldOut";
  };
}

/**
 * Get the structured data JSON-LD object for a product
 */
export function getStructuredData(product: Product): StructuredData {
  const oneYearFromNow = new Date();
  oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);
  return {
    "@context": "http://schema.org",
    "@type": "Product",
    name: product.name,
    image: product.image,
    description: getSeoDescription(product.description) ?? "",
    offers: {
      "@type": "Offer",
      price: (product.price / 100).toFixed(2),
      priceCurrency: "EUR",
      priceValidUntil: oneYearFromNow.toISOString(),
      availability: "InStock",
    },
  };
}

/**
 * Replace html tags in given text and truncate to 120 chars
 */
export function getSeoDescription(description?: string): string | undefined {
  const maxlength = 120;
  const minlength = 70;
  if (!description) {
    return;
  }
  description = description.replace(/<[^>]*>?/gm, " "); // replace html
  description = description.replace(/&nbsp;/g, " "); // replace &nbsp;
  description.replace(/\s\s+/g, " "); // replace double spaces
  const sentence = description.substr(0, description.indexOf(".") + 1);
  if (sentence.length > maxlength) {
    // truncate to max
    return sentence.substr(0, maxlength).trim() + "...";
  } else if (sentence.length > minlength) {
    // Between min and max, perfect!
    return sentence.trim();
  } else {
    // Sentence too short, just take first X chars of description
    return description.substring(0, maxlength).trim() + "...";
  }
}
