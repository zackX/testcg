export const filters = {
  price(input: number): string {
    if (isNaN(input)) {
      return `${import.meta.env.PUBLIC_CURRENCY} `;
    }
    return `${import.meta.env.PUBLIC_CURRENCY} ` + (input / 100).toFixed(2);
  },
};
//  Sort products by popularity
export const sortByPopularityScore = (a: any, b: any): number => {
  const scoreA = a.product.customFields.popularityScore || 0;
  const scoreB = b.product.customFields.popularityScore || 0;
  return scoreB - scoreA;
};
