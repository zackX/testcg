import { RequestContext, Product } from "@vendure/core";
import { SelectedChoiceOption, SelectedNumberOption } from "../types";
import { HumanReadableSelectedConfiguratorOption } from "../ui/generated/generated-graphql-types";
import { getFullSelectedOptions } from "./get-full-selected-options";
import { parseConfiguratorOptions } from "./parse-configurator-options";
import { parseSelectedOption } from "./parse-selected-option";

/**
 * Resolves selected optionGroupId's to readable option groups and options
 */
export function resolveHumanReadableOptions(
  ctx: RequestContext,
  product: Product,
  stringifiedSelectedOptions?: string[],
): HumanReadableSelectedConfiguratorOption[] {
  if (!stringifiedSelectedOptions) {
    return [];
  }
  const allAvailableOptionGroups = parseConfiguratorOptions(product);
  const selectedOptionInputs =
    stringifiedSelectedOptions.map(parseSelectedOption);
  const selectedOptions = getFullSelectedOptions(
    allAvailableOptionGroups,
    selectedOptionInputs,
  );
  // Map to human readable options and filter out hidden options
  return selectedOptions
    .filter((option) => !(option as SelectedChoiceOption)?.customFields?.hidden)
    .map((option) => {
      const optionValue: string | number =
        (option as SelectedNumberOption).value ??
        (option as SelectedChoiceOption).name;
      return {
        optionGroupName: option.optionGroupName,
        optionValue: String(optionValue),
      };
    });
}
