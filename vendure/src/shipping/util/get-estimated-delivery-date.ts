import dayjs from "dayjs";
import {
  isPastMidday,
  isThisDayAWeekday,
  shouldSkipDay,
} from "./should-skip-day";
import { ShippingMethod } from "@vendure/core";
import { getTurnaroundDays } from "./turnaround";

export function getEstimatedShippingDate(
  orderPlacedAt: Date,
  turnaround: number,
): Date {
  let date = dayjs(orderPlacedAt);
  let appliedTurnaroundDays = 0;
  if (!isPastMidday(date)) {
    appliedTurnaroundDays++;
  }
  while (appliedTurnaroundDays < turnaround) {
    date = date.add(1, "day");
    if (!shouldSkipDay(date)) {
      appliedTurnaroundDays++;
    }
  }
  return date.toDate();
}

export function getEstimatedDeliveryDate(
  orderPlacedAt: Date,
  turnaround: number,
): Date {
  const shippingDate = getEstimatedShippingDate(orderPlacedAt, turnaround);
  // Delivery is assumed 1 day after shipping, if it is not a weekend
  let deliveryDate = dayjs(shippingDate).add(1, "day");
  while (!isThisDayAWeekday(deliveryDate)) {
    deliveryDate = deliveryDate.add(1, "day");
  }
  return deliveryDate.toDate();
}

/**
 * Get the fallback estimated delivery date, when turnaround is not set.
 */
export function getFallbackEstimatedDeliveryDate(): Date {
  return dayjs(new Date()).add(6, "day").toDate();
}

export function getEstimatedDeliveryDateForTurnaroundShippingMethod(
  shippingMethod: Pick<ShippingMethod, "code">,
): Date {
  const turnaroundDays = getTurnaroundDays(shippingMethod);
  return getEstimatedDeliveryDate(new Date(), turnaroundDays);
}
