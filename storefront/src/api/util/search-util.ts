import Fuse, { type FuseIndexRecords, type IFuseOptions } from "fuse.js";

/**
 * The item that is passed in to the Fuse search engine.
 * This is also the result when searching for products.
 */
export interface SearchItem {
  name: string;
  price: number;
  image: string;
  url: string;
  collections?: string[];
  keywords?: string[];
  facetValues?: Array<{
    code: string;
    name: string;
    facet: { code: string; name: string };
  }>;
}

/**
 * Prebuilt index to instantiate a Fuse Search engine on the client
 */
export interface SearchIndexObject<T extends SearchItem> {
  index: {
    keys: readonly string[];
    records: FuseIndexRecords;
  };
  items: T[];
  keys: KeyWeight[];
}

/**
 * The importance (weight) of each field of a search item
 */
export interface KeyWeight {
  name: "name" | "collections" | "keywords" | string;
  weight: number;
}

/**
 * Create an pre-compiled index object during site build
 */
export function createSearchIndex<T extends SearchItem>(
  items: T[],
  keys: KeyWeight[],
): SearchIndexObject<T> {
  const fuseIndex = Fuse.createIndex(keys, items);
  return {
    index: fuseIndex.toJSON(),
    items,
    keys,
  };
}

/**
 * Create client side Fuse instance for searching
 */
export function createFuse<T extends SearchItem>(
  indexObject: SearchIndexObject<T>,
  options: IFuseOptions<T>,
): Fuse<T> {
  const parsedIndex = Fuse.parseIndex(indexObject.index);
  return new Fuse(
    indexObject.items,
    {
      keys: indexObject.keys,
      ...options,
    },
    parsedIndex,
  );
}
