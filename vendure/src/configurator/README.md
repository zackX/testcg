# Vendure Configurator Plugin

This plugin allows you to turn products into product configurators. You can define custom option groups on products, without the need to generate variants for each of the possible options.

The plugin is based on the following principles:

- Administrators can manage options per product and manage the corresponding option prices via the Admin UI
- Developers will have to specify the available calculations in the plugin's config
- An administrator can specify what calculation is used for what product via the Admin UI

![Configurable product on the storefront](storefront.png)
![Configurable options in Vendure](admin-ui.png)

Products have option groups, and option groups can have selectable options. Products can have the following option group types:

- `ConfiguratorOptionGroupRadio`: An option group that allows selection of one of its options
- `ConfiguratorOptionGroupCheckbox`: An option group that allows selection of multiple of its options
- `ConfiguratorOptionGroupNumber`: An option group that allows customers to enter a numeric value. This option group does not have selectable options.

By default, the plugin options don't define any values for calculating the price of a configured product, but thats where the option custom fields come into play: Each option group and each option can have custom fields, where you can define its prices.

## Example: Printable product with custom size

We have a printable product where we would customers to be able to select a material and custom sizes. For simplicity we use a product with just 2 options and width/height, but typically you would have much more options on a product.

We will create a product named `Custom sign` with the following option groups via the Admin UI (this can be done via the product detail page):

- `material`: A customer can select `cardboard` or `foam` as material to print on.
- `width`: A customer can specify a custom width
- `height`: A customer can specify a custom width

The price calculation will calculated according to the following rules:

- $10 per square meter for `cardboard`
- $25 per square meter for `foam`

So, for a foam printed product of 2 square meter, the price would be $25 \* 2 = $50

We need to store 2 extra custom configurator fields: `price per square meter` on the selectable materials inside the `material

For the option `cardboard` we add the following data as custom field:

```json
{
  "pricePerSqMeter": 1000
}
```

And for the `foam` we add

```json
{
  "pricePerSqMeter": 2500
}
```

Now, when we add the `Custom sign` product to the order with `addItemToOrder`, we pass the following custom fields to the orderline:

```graphql
mutation {
  addItemToOrder(
    productVariantId: "1"
    quantity: 1
    customFields: {
      selectedConfiguratorOptions: [
        "{\"optionGroupId\":\"material\",\"optionId\":\"foam\"}"
        "{\"optionGroupId\":\"height\",\"value\":\"1000\"}"
        "{\"optionGroupId\":\"width\",\"value\":\"1000\"}"
      ]
    }
  ) {
    __typename
    ... on Order {
      id
    }
  }
}
```

Each item in the `selectedConfiguratorOptions` array, is the stringified version of a selected option:

```ts
{ optionGroupId: "material", optionId: "foam" } // Chooses 10mm for material thickness
{ optionGroupId: "height", value: 1000 }
{ optionGroupId: "width", value: 1000 }
```

To preview the price of a configured product without adding it to cart, you can use `calculateConfiguratorPrice` GraphQL query:

```graphql
{
  calculateConfiguratorPrice(
    variantId: 1
    quantity: 1
    selectedOptions: [
      { optionGroupId: "material", optionId: "foam" }
      { optionGroupId: "height", value: 10000 }
      { optionGroupId: "width", value: 1000 }
    ]
  )
}
```

## Calculation

These values will be passed into your custom `ConfiguratorCalculation`, where you can use the price per square meter to return a unit price.

```ts
export const sqmPricing: ConfiguratorCalculation = {
  id: "pricing",
  name: "Pricing per square meter",
  calculateFn: async (ctx, injector, productVariant, selectedOptions) => {
    // TODO actual implementation
    // Example `selectedOptions[0].value` for number inputs
    // `selectedOptions[0].customFields` to access price per sqm for checkboxes and radio options
    return 500;
  },
};
```
