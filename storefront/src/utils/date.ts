export function formatDate(date: string): string {
  return new Date(date).toLocaleDateString("en-GB", {
    weekday: "short",
    month: "short",
    day: "numeric",
  });
}
