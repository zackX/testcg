import {
  Injector,
  LocaleString,
  Logger,
  Product,
  ProductService,
  RequestContext,
  TransactionalConnection,
  UserInputError,
} from "@vendure/core";
import { In, IsNull, Not } from "typeorm";
import { GoogleSheetDataStrategy } from "../pull-data-from-google-sheet/api/strategies/google-sheet-data.strategy";
import {
  ConfiguratorOptionGroup,
  ConfiguratorOptionGroupNumber,
  ConfiguratorOptionGroupRadio,
  ConfiguratorSelectableOption,
} from "../pull-data-from-google-sheet/ui/generated/generated-graphql-types";
import { Price } from "../configurator/api/cg-calculation";

const loggerCtx = "CGOptionSync";

export interface ColumnMetaData {
  colName: string;
  dataRegex: RegExp;
}
/**
 * This Google Sheet strategy is responsible for pulling product options from this sheet:
 * https://docs.google.com/spreadsheets/d/1QumO_VIZJ1zsRAA7j4Eer7EJeGo_ThD-P5Y8k1MTYuw/edit#gid=635504534
 */
export class ColorGraphicsOptionSyncStrategy
  implements GoogleSheetDataStrategy
{
  columnMetadata: ColumnMetaData[] = [
    {
      colName: "Option Group ID",
      dataRegex: /.*/,
    },
    {
      colName: "Option Group name",
      dataRegex: /.*/,
    },
    {
      colName: "Option ID",
      dataRegex: /.*/,
    },
    {
      colName: "Option Name",
      dataRegex: /.*/,
    },
    {
      colName: "Option type",
      dataRegex: /sqm|perimeter|width|height|static|^$/,
    },
    {
      colName: "hidden",
      dataRegex: /TRUE|FALSE|^$/,
    },
    {
      colName: "price <1",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "price >2",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "price >21",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "price >50",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "Min unit price",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "Min order price",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "min",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "max",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "Weight grams per sq/m",
      dataRegex: /^\d*\.?\d*$/,
    },
    {
      colName: "Retail increase",
      dataRegex: /^\d*\.?\d*%$/,
    },
    {
      colName: "products",
      dataRegex: /.*/,
    },
  ];

  priceRangeColumnsIndex: number[] = [];

  async handleSheetData(
    ctx: RequestContext,
    injector: Injector,
    sheetData: string[][],
  ): Promise<void> {
    const headerRow = sheetData[0];
    this.validateSheetStructure(headerRow);
    const priceRange1ColumnIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "price <1",
    );

    const priceRange2ColumnIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "price >2",
    );

    const priceRange3ColumnIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "price >21",
    );

    const priceRange4ColumnIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "price >50",
    );
    this.priceRangeColumnsIndex = [
      priceRange1ColumnIndex,
      priceRange2ColumnIndex,
      priceRange3ColumnIndex,
      priceRange4ColumnIndex,
    ];
    const partialProducts = this.readData(sheetData.slice(1));
    await this.saveProducts(ctx, injector, partialProducts);
  }

  validateSheetStructure(headerRow: string[]): void {
    if (headerRow.length < this.columnMetadata.length) {
      throw new UserInputError(
        `Expected ${this.columnMetadata.length} columns but found ${headerRow.length}`,
      );
    }
    let colIndex = 0;
    for (; colIndex < headerRow.length; colIndex++) {
      if (
        headerRow[colIndex].trim() !== this.columnMetadata[colIndex].colName
      ) {
        throw new UserInputError(
          `Expected column ${
            this.columnMetadata[colIndex].colName
          } in position ${colIndex + 1} but found ${headerRow[colIndex]}`,
        );
      }
    }
  }

  /**
   * Create option groups based on each row,
   * adds the slugs to each group,
   * then loops all option groups again to add them to product inputs.
   */
  readData(sheet: string[][]): Array<Partial<Product>> {
    const optionGroupsWithSlugs: Array<
      ConfiguratorOptionGroup & { slugs?: string[] }
    > = [];
    const optionGroupNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group name",
    );
    const optionGroupIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group ID",
    );
    for (let rowIndex = 0; rowIndex < sheet.length; rowIndex++) {
      const row = sheet[rowIndex];
      if (!row.length) {
        // Empty row
        continue;
      }
      const slugs = row[row.length - 1]?.trim().split(",") ?? [];
      // NumberOptionGroup
      if (this.isCurrentOptionGroupTypeNumber(row)) {
        // Number option groups are 1 per row, so it's always pushed as new option group
        optionGroupsWithSlugs.push({
          ...this.parseNumberOptionGroupOption(row, rowIndex + 2),
          slugs,
        });
        continue;
      }
      // RadioOptionGroup
      if (this.isCurrentOptionGroupTypeRadio(row)) {
        const currentOptionGroupId = row[optionGroupIdColIndex].trim();
        // Check if an option group has already been added based on previous rows
        const existingOptionGroup = optionGroupsWithSlugs.find(
          (og) => og.id === currentOptionGroupId,
        ) ?? {
          // If not, create a new option group with empty options array
          __typename: "ConfiguratorOptionGroupRadio",
          id: currentOptionGroupId,
          name: row[optionGroupNameColIndex],
          options: [],
          slugs,
        };
        (existingOptionGroup as ConfiguratorOptionGroupRadio).options?.push(
          this.parseRadioOptionGroupOption(row, rowIndex + 2),
        );
        optionGroupsWithSlugs.push(existingOptionGroup);
      } else {
        throw new UserInputError(
          `The ${rowIndex + 2}th row contains unrecognized Option Group`,
        );
      }
    }
    const productInputs = new Map<string, Partial<Product>>();
    // Loop option groups, add to product, and delete slug from group
    optionGroupsWithSlugs.forEach((optionGroup) => {
      optionGroup.slugs?.forEach((slug) => {
        // Check if a product input already exists for this slug, or create a new product input with empty option groups array
        const product = productInputs.get(slug) ?? {
          slug: slug as LocaleString,
          customFields: {
            configuratorOptionGroups: JSON.stringify([]),
          },
        };
        // Add our current option group to the existing option groups for this product input
        const existingOptionGroups = JSON.parse(
          product.customFields?.configuratorOptionGroups ?? "[]",
        ) as ConfiguratorOptionGroup[];
        delete optionGroup.slugs; // Don't save the slugs on the option group in the database, they are just use for lookup of products
        existingOptionGroups.push(optionGroup);
        productInputs.set(slug, {
          slug: slug as LocaleString,
          customFields: {
            configuratorOptionGroups: JSON.stringify(existingOptionGroups),
          },
        });
        Logger.info(
          `Added option group '${optionGroup.id}' to '${slug}'`,
          loggerCtx,
        );
      });
    });
    // Only return the map's values, becuase they key (slug), was just used for grouping
    return Array.from(productInputs.values());
  }

  isCurrentOptionGroupTypeNumber(row: string[]): boolean {
    const minColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "min",
    );
    const maxColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "max",
    );
    const optionGroupIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group ID",
    );
    const optionGroupNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group name",
    );
    return (
      row[minColIndex].trim() !== "" &&
      row[maxColIndex].trim() !== "" &&
      row[optionGroupIdColIndex].trim() !== "" &&
      row[optionGroupNameColIndex].trim() !== ""
    );
  }

  isCurrentOptionGroupTypeRadio(row: string[]): boolean {
    const optionGroupIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group ID",
    );
    const optionGroupNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group name",
    );
    const optionIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option ID",
    );
    const optionNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Name",
    );
    return (
      row[optionIdColIndex].trim() !== "" &&
      row[optionNameColIndex].trim() !== "" &&
      row[optionGroupIdColIndex].trim() !== "" &&
      row[optionGroupNameColIndex].trim() !== ""
    );
  }

  parseRadioOptionGroupOption(
    row: string[],
    rowNumber: number,
  ): ConfiguratorSelectableOption {
    const prices: Price[] = [];

    for (const columnNr of this.priceRangeColumnsIndex) {
      const pricePerSqm = this.parseNumericAndCheck(
        row,
        columnNr,
        rowNumber,
        false,
      );
      if (pricePerSqm !== undefined) {
        prices.push({
          min: 0,
          price: pricePerSqm * 100, // convert to cents
        });
      }
    }
    const optionIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option ID",
    );
    const optionNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Name",
    );
    const weightGramPerSquareMeterColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Weight grams per sq/m",
    );
    const optionTypeColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option type",
    );
    const hiddenCostValueColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "hidden",
    );
    const minOrderPriceColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Min order price",
    );
    const minUnitPriceColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Min unit price",
    );
    const retailIncreaseColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Retail increase",
    );
    const minOrderPrice = this.parseNumericAndCheck(
      row,
      minOrderPriceColIndex,
      rowNumber,
      false,
    );
    const minUnitPrice = this.parseNumericAndCheck(
      row,
      minUnitPriceColIndex,
      rowNumber,
      false,
    );
    const retailIncreasePercent = this.parseStringAndCheck(
      row,
      retailIncreaseColIndex,
      rowNumber,
      true,
    );
    return {
      id: this.parseStringAndCheck(row, optionIdColIndex, rowNumber, true),
      name: this.parseStringAndCheck(row, optionNameColIndex, rowNumber, true),
      customFields: {
        prices,
        weightGramsPerSquareMeter: this.parseNumericAndCheck(
          row,
          weightGramPerSquareMeterColIndex,
          rowNumber,
          false,
        ),
        hidden: this.parseBooleanAndCheck(
          row,
          hiddenCostValueColIndex,
          rowNumber,
        ),
        optionType: this.parseStringAndCheck(
          row,
          optionTypeColIndex,
          rowNumber,
          false,
        ),
        minUnitPrice: minUnitPrice ? minUnitPrice * 100 : undefined,
        minOrderPrice: minOrderPrice ? minOrderPrice * 100 : undefined,
        retailIncrease: parseFloat(retailIncreasePercent),
      },
    };
  }

  parseBooleanAndCheck(
    row: string[],
    colIndex: number,
    rowIndex: number,
  ): boolean {
    const errorMessage = `The ${rowIndex}th row under '${this.columnMetadata[colIndex].colName}' column doesn't contain a TRUE or FALSE value`;
    const cellData = row[colIndex]?.trim();
    if (cellData === "TRUE") {
      return true;
    } else if (cellData === "FALSE") {
      return false;
    } else {
      throw new UserInputError(errorMessage);
    }
  }

  // Overloads that define the return type based on the isRequired parameter
  parseNumericAndCheck(
    row: string[],
    colIndex: number,
    rowIndex: number,
    isRequired: true,
  ): number;
  parseNumericAndCheck(
    row: string[],
    colIndex: number,
    rowIndex: number,
    isRequired: false,
  ): number | undefined;
  parseNumericAndCheck(
    row: string[],
    colIndex: number,
    rowIndex: number,
    isRequired: boolean,
  ): number | undefined {
    const errorMessage = `The ${rowIndex}th row under '${this.columnMetadata[colIndex].colName}' column doesn't contain a positive number`;
    const parsedNr = parseFloat(
      this.parse(row, colIndex, errorMessage, isRequired),
    );
    if (isNaN(parsedNr) && isRequired) {
      throw new UserInputError(errorMessage);
    }
    if (isNaN(parsedNr)) {
      return undefined;
    }
    return parsedNr;
  }

  parse(
    row: string[],
    colIndex: number,
    errorMessage: string,
    isRequired: boolean,
  ): string {
    const cellData = row[colIndex]?.trim();
    if (
      !cellData.match(this.columnMetadata[colIndex].dataRegex) ||
      (isRequired && cellData === "")
    ) {
      throw new UserInputError(errorMessage);
    }
    return cellData;
  }

  parseStringAndCheck(
    row: string[],
    colIndex: number,
    rowIndex: number,
    isRequired: boolean,
  ): string {
    return this.parse(
      row,
      colIndex,
      `The ${rowIndex}th row under '${this.columnMetadata[colIndex].colName}' column contains invalid data`,
      isRequired,
    );
  }

  parseNumberOptionGroupOption(
    row: string[],
    rowNumber: number,
  ): ConfiguratorOptionGroupNumber {
    const minColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "min",
    );
    const maxColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "max",
    );
    const optionGroupIdColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group ID",
    );
    const optionGroupNameColIndex = this.columnMetadata.findIndex(
      (s) => s.colName === "Option Group name",
    );
    return {
      __typename: "ConfiguratorOptionGroupNumber",
      id: this.parseStringAndCheck(row, optionGroupIdColIndex, rowNumber, true),
      name: this.parseStringAndCheck(
        row,
        optionGroupNameColIndex,
        rowNumber,
        true,
      ),
      min: this.parseNumericAndCheck(row, minColIndex, rowNumber, true),
      max: this.parseNumericAndCheck(row, maxColIndex, rowNumber, true),
    };
  }

  async saveProducts(
    ctx: RequestContext,
    injector: Injector,
    productInputs: Array<Partial<Product>>,
  ): Promise<void> {
    const productService = injector.get(ProductService);
    const productRepo = injector
      .get(TransactionalConnection)
      .getRepository(ctx, Product);
    const slugs = productInputs.map((p) => p.slug);
    const products = await productRepo.find({
      where: {
        translations: {
          slug: In(slugs),
        },
        deletedAt: IsNull(),
      },
      select: ["id", "deletedAt", "translations"],
      relations: ["translations"],
    });
    const errors: string[] = [];
    await Promise.all(
      productInputs.map(async (productInput) => {
        // Find product based on any of the translated slugs
        const product = products.find((p) =>
          p.translations.find((t) => t.slug === productInput.slug),
        );
        if (!product) {
          errors.push(
            `Product with slug '${productInput.slug}' couldn't be found in the database`,
          );
          return;
        }
        await productService.update(ctx, {
          id: product.id,
          customFields: {
            configuratorOptionGroups:
              productInput.customFields?.configuratorOptionGroups,
          },
        });
        Logger.info(
          `Updated product '${productInput.slug}' (${product.id})`,
          loggerCtx,
        );
      }),
    );
    if (errors.length) {
      throw new UserInputError(errors.join("\n"));
    }
    // Find products that have been removed from the sheet but still exist in the database
    const deletedProducts = await productRepo.find({
      where: {
        translations: {
          slug: Not(In(slugs)),
        },
        deletedAt: IsNull(),
        customFields: {
          configuratorOptionGroups: Not(IsNull()),
        },
      },
      select: ["id", "deletedAt", "translations", "customFields"],
      relations: ["translations"],
    });
    if (deletedProducts.length) {
      deletedProducts.forEach((p) => {
        Logger.info(
          `Removed Configurator Product Option Groups from '${p.translations.find(
            (t) => t.languageCode === ctx.languageCode,
          )?.slug}' (${p.id})`,
          loggerCtx,
        );
        p.customFields.configuratorOptionGroups = undefined;
      });
      await productRepo.save(deletedProducts);
    }
  }
}
