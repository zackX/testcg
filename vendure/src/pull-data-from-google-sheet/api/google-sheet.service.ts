import { Injectable, Inject } from "@nestjs/common";
import { ModuleRef } from "@nestjs/core";
import { PLUGIN_INIT_OPTIONS, loggerCtx } from "../constants";
import {
  Injector,
  InternalServerError,
  Logger,
  RequestContext,
  TransactionalConnection,
  UserInputError,
} from "@vendure/core";
import axios from "axios";
import { GoogleSheetDataStrategy } from "./strategies/google-sheet-data.strategy";
import { GoogleSheetInput } from "../ui/generated/generated-graphql-types";
import { GoogleSheetMetaData } from "./entities/google-sheet-meta-data.entity";
import { PullDataFromGoogleSheetPluginConfig } from "../types";
const GOOGLE_SPREASHEET_URL = `https://sheets.googleapis.com/v4/spreadsheets`;
@Injectable()
export class GoogleSheetService {
  private readonly dataStrategy: GoogleSheetDataStrategy;
  constructor(
    @Inject(PLUGIN_INIT_OPTIONS)
    private readonly config: PullDataFromGoogleSheetPluginConfig,
    private readonly moduleRef: ModuleRef,
    private readonly connection: TransactionalConnection,
  ) {
    if (!config?.dataStrategy || !config?.googleApiKey) {
      Logger.warn("No configured DataStrategy or google api key", loggerCtx);
    }
    this.dataStrategy = config.dataStrategy;
  }

  async readGoogleSheet(
    ctx: RequestContext,
    sheetInput: GoogleSheetInput,
  ): Promise<{ success: boolean }> {
    sheetInput.url = this.sanitizeUrl(sheetInput.url);
    await this.updateActiveGoogleSheetMetaData(ctx, sheetInput);
    if (!this.dataStrategy) {
      throw new InternalServerError("No DataStrategy provided");
    }
    const capturedId = (sheetInput?.url).split("/");
    if (!capturedId?.length) {
      throw new UserInputError("Parsing url fails. No google sheet id found");
    }
    const dIndex = capturedId.findIndex((s) => s === "d");
    const requestUrl = `${GOOGLE_SPREASHEET_URL}/${
      capturedId[dIndex + 1]
    }/values/${sheetInput.sheetName}?key=${this.config.googleApiKey}`;
    const result = await axios.get(requestUrl);
    if (!result?.data?.values || result.status !== 200) {
      throw new UserInputError("Couldn't read from google api");
    }
    await this.dataStrategy.handleSheetData(
      ctx,
      new Injector(this.moduleRef),
      result.data.values as string[][],
    );
    return { success: true };
  }

  async getActiveGoogleSheetMetaData(
    ctx: RequestContext,
  ): Promise<GoogleSheetMetaData | null> {
    const googleSheetMetaDataRepo = this.connection.getRepository(
      ctx,
      GoogleSheetMetaData,
    );
    return await googleSheetMetaDataRepo.findOne({
      where: { channels: { id: ctx.channelId } },
    });
  }

  private async updateActiveGoogleSheetMetaData(
    ctx: RequestContext,
    sheetInput: GoogleSheetInput,
  ): Promise<void> {
    const googleSheetMetaDataRepo = this.connection.getRepository(
      ctx,
      GoogleSheetMetaData,
    );
    let activeGoogleSheetMetaData = await googleSheetMetaDataRepo.findOne({
      where: { channels: { id: ctx.channelId } },
    });
    if (!activeGoogleSheetMetaData) {
      activeGoogleSheetMetaData = new GoogleSheetMetaData();
      activeGoogleSheetMetaData.channels = [ctx.channel];
      activeGoogleSheetMetaData.sheetName = sheetInput.sheetName;
      activeGoogleSheetMetaData.url = sheetInput.url;
      await googleSheetMetaDataRepo.save(activeGoogleSheetMetaData);
      return;
    }
    if (
      activeGoogleSheetMetaData.sheetName !== sheetInput.sheetName ||
      activeGoogleSheetMetaData.url !== sheetInput.url
    ) {
      activeGoogleSheetMetaData.sheetName = sheetInput.sheetName;
      activeGoogleSheetMetaData.url = sheetInput.url;
      await googleSheetMetaDataRepo.save(activeGoogleSheetMetaData);
    }
  }

  sanitizeUrl(url: string): string {
    url.trim();
    if (!url.endsWith("/")) {
      url += `${url}/`;
    }
    return url;
  }
}
