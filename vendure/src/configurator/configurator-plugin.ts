import { LanguageCode, PluginCommonModule, VendurePlugin } from "@vendure/core";
import { AdminUiExtension } from "@vendure/ui-devkit/compiler";
import path from "path";
import { CustomFieldConfig } from "@vendure/common/lib/generated-types";
import { apiSchema } from "./api/api-extension";
import { ConfiguratorCalculation } from "./types";
import { ConfiguratorOrderItemStrategy } from "./api/configurator-order-item-strategy";
import { ConfiguratorService } from "./api/configurator-service";
import { ConfiguratorResolver } from "./api/configurator.resolver";
import {
  addressCustomFields,
  configuratorOrderFields,
  configuratorOrderLineFields,
  configuratorProductFields,
} from "./api/custom-fields";
import { PLUGIN_INIT_OPTIONS } from "./constants";
import { TradeCustomerResolver } from "./api/trade-customer-resolver";
import { TradeCustomerService } from "./api/trade-customer-service";

export interface ConfiguratorPluginOptions {
  calculation: ConfiguratorCalculation;
}

@VendurePlugin({
  imports: [PluginCommonModule],
  controllers: [],
  providers: [
    ConfiguratorService,
    TradeCustomerService,
    {
      provide: PLUGIN_INIT_OPTIONS,
      useFactory: () => ConfiguratorPlugin.options,
    },
  ],
  shopApiExtensions: {
    resolvers: [ConfiguratorResolver, TradeCustomerResolver],
    schema: apiSchema,
  },
  adminApiExtensions: {
    resolvers: [ConfiguratorResolver, TradeCustomerResolver],
    schema: apiSchema,
  },
  entities: [],
  configuration: (config) => {
    config.customFields.Product.push(...configuratorProductFields);
    config.customFields.Order.push(...configuratorOrderFields);
    config.customFields.OrderLine.push(...configuratorOrderLineFields);
    config.customFields.Address.push(...addressCustomFields);
    config.orderOptions.orderItemPriceCalculationStrategy =
      new ConfiguratorOrderItemStrategy();
    return config;
  },
})
export class ConfiguratorPlugin {
  public static uiExtensions: AdminUiExtension = {
    extensionPath: path.join(__dirname, "ui"),

    ngModules: [
      {
        type: "shared" as const,
        ngModuleFileName: "shared.module.ts",
        ngModuleName: "SharedExtensionModule",
      },
    ],
  };

  static options: ConfiguratorPluginOptions;

  static init(options: ConfiguratorPluginOptions): typeof ConfiguratorPlugin {
    // Set available calculations as selectable options
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    configuratorProductFields[0]!.ui.options =
      // options.calculation.map(
      // (c) => (
      [
        {
          value: options.calculation.id,
          label: [
            { languageCode: LanguageCode.en, value: options.calculation.name },
          ],
        },
      ];
    //  /)
    // );
    this.options = options;
    return ConfiguratorPlugin;
  }

  static customProductFields: CustomFieldConfig[] = [
    // ------------------ Selectable calculations ------------------
    {
      name: "configuratorCalculation",
      label: [
        {
          languageCode: LanguageCode.en,
          value: "Calculation",
        },
      ],
      type: "text",
      list: false,
      readonly: false,
      nullable: true,
      ui: {
        component: "select-form-input",
        options: [],
      },
    },
    // ------------------ option groups ------------------
    {
      name: "configuratorOptionGroups",
      label: [
        {
          languageCode: LanguageCode.en,
          value: "Configurator options",
        },
      ],
      type: "text",
      list: false,
      readonly: false,
      nullable: true,
      ui: {
        component: "configurator-option-groups-form-input",
      },
    },
  ];
}
