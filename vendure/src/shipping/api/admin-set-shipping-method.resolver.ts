import { Resolver, Mutation, Args } from "@nestjs/graphql";
import {
  Ctx,
  ErrorResultUnion,
  ID,
  Order,
  OrderService,
  RequestContext,
  Transaction,
} from "@vendure/core";
import { SetOrderShippingMethodResult } from "@vendure/common/lib/generated-shop-types";

@Resolver()
export class AdminSetShippingMethodsResolver {
  constructor(private readonly orderService: OrderService) {}

  @Transaction()
  @Mutation()
  async setOrderShippingMethod(
    @Ctx() ctx: RequestContext,
    @Args() args: { shippingMethodId: ID[]; orderId: ID },
  ): Promise<ErrorResultUnion<SetOrderShippingMethodResult, Order>> {
    return await this.orderService.setShippingMethod(
      ctx,
      args.orderId,
      args.shippingMethodId,
    );
  }
}
