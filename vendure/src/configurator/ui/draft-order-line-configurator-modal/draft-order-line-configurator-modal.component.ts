import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {
  FormArray,
  FormControl,
  FormGroup,
  UntypedFormGroup,
} from "@angular/forms";
import {
  DataService,
  Dialog,
  NotificationService,
} from "@vendure/admin-ui/core";
import { ID } from "@vendure/common/lib/shared-types";
import { take } from "rxjs";
import {
  ADJUST_ORDER_LINE,
  CALCULATE_CONFIGURATOR_PRICE,
} from "../draft-order-configurator-options/draft-order-configurator-options.graphql";
export type MeasurementUnit = "mm" | "cm" | "meters" | "in" | "ft";
@Component({
  selector: "draft-order-line-configurator-modal",
  templateUrl: "./draft-order-line-configurator-modal.component.html",
  styleUrls: ["./draft-order-line-configurator-modal.component.scss"],
})
export class DraftOrderLineConfiguratorModalComponent
  implements Dialog<any>, OnInit
{
  previousUnit: MeasurementUnit;
  retailPrice: number = 0;
  tradePrice: number = 0;
  currencyCode: string;
  presetSizes = [
    { name: "Custom Size", height: 1000, width: 1000 },
    { name: "A0 ~ 841 mm x 1189 mm", height: 841, width: 1189 },
    { name: "A1 ~ 594 mm x 841 mm", height: 594, width: 841 },
    { name: "A2 ~ 420 mm x 594 mm", height: 420, width: 594 },
    { name: "A3 ~ 297 mm x 420 mm", height: 297, width: 420 },
    { name: "A4 ~ 210 mm x 297 mm", height: 210, width: 297 },
  ];

  measurementUnits: Array<{ name: MeasurementUnit; factor: number }> = [
    { name: "mm", factor: 1 },
    { name: "cm", factor: 10 },
    { name: "meters", factor: 1000 },
    { name: "in", factor: 25.4 },
    { name: "ft", factor: 304.8 },
  ];

  selectedLine: any;
  orderId: ID;
  formGroup: UntypedFormGroup;
  resolveWith: (result?: any) => void;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly ns: NotificationService,
    private readonly dataService: DataService,
  ) {}

  ngOnInit(): void {
    this.updateFormGroup();
  }

  getStarterWidth(selectedConfiguratorOptions?: string[]): number {
    return this.getStarterDimension("width", selectedConfiguratorOptions);
  }

  getStarterHeight(selectedConfiguratorOptions?: string[]): number {
    return this.getStarterDimension("height", selectedConfiguratorOptions);
  }

  getStarterDimension(
    dimension: "width" | "height",
    selectedConfiguratorOptions?: string[],
  ): number {
    if (selectedConfiguratorOptions?.length) {
      try {
        const configuratorOptionsList = selectedConfiguratorOptions.map((c) =>
          JSON.parse(c),
        );
        return (
          configuratorOptionsList.find((c) => c.optionGroupId === dimension)
            ?.value ?? 1000
        );
      } catch (e) {
        return 1000;
      }
    }
    const customSize = this.presetSizes.find((p) => p.name === "Custom Size");
    return customSize?.[dimension] ?? 1000;
  }

  updateFormGroup(): void {
    const selectedConfiguratorOptionsCustomField =
      this.selectedLine.customFields.selectedConfiguratorOptions;
    const names = this.presetSizes.map((m) => m.name);
    const measurementUnitsNames: MeasurementUnit[] = this.measurementUnits.map(
      (m) => m.name,
    );
    this.previousUnit = measurementUnitsNames[0];
    this.formGroup = new FormGroup({
      sizeType: new FormControl<(typeof names)[number]>(names[0]),
      quantity: new FormControl<(typeof names)[number]>(
        this.selectedLine.quantity,
      ),
      width: new FormControl<number>(
        this.getStarterWidth(selectedConfiguratorOptionsCustomField),
      ),
      height: new FormControl<number>(
        this.getStarterHeight(selectedConfiguratorOptionsCustomField),
      ),
      measurementUnit: new FormControl<MeasurementUnit>(
        measurementUnitsNames[0],
      ),
      optionGroups: new FormArray(
        this.getConfigurableOptions().map(
          (c) =>
            new FormControl<string>(
              this.getStarterOptionForOptionGroup(
                c,
                selectedConfiguratorOptionsCustomField,
              ),
            ),
        ),
      ),
      additionalInformation: new FormControl<string>(
        this.selectedLine.customFields?.customerNote,
      ),
      customUnitPriceExVAT: new FormControl<number>(
        this.selectedLine.customFields?.customUnitPriceExVAT ?? 0.0,
      ),
    });
    void this.calculateConfiguratorPrice(this.selectedLine.quantity);
    this.cdr.markForCheck();
    this.formGroup.valueChanges.subscribe((value: any) => {
      void this.calculateConfiguratorPrice(value.quantity);
    });
  }

  getConfigurableOptions(): any[] {
    const optionGroups: any[] = [];
    for (const optionGroup of this.selectedLine.productVariant.product
      .configuratorOptionGroups) {
      if (optionGroup?.options?.length) {
        optionGroups.push(optionGroup);
      }
    }
    return optionGroups;
  }

  getStarterOptionForOptionGroup(
    optionGroup: any,
    selectedConfiguratorOptions?: string[],
  ): string {
    if (selectedConfiguratorOptions?.length) {
      try {
        const configuratorOptionsList = selectedConfiguratorOptions.map((c) =>
          JSON.parse(c),
        );
        for (const optionGroupFragment of configuratorOptionsList) {
          if (optionGroupFragment.optionGroupId === optionGroup.id) {
            return optionGroupFragment.optionId;
          }
        }
      } catch (e) {
        return optionGroup.options[0].id;
      }
    }
    return optionGroup.options[0].id;
  }

  async calculateConfiguratorPrice(quantity: number): Promise<void> {
    await new Promise((resolve) => setTimeout(resolve, 10));
    const selectedOptions = this.getOrderLineConfiguratorOptions();
    this.dataService
      .query(CALCULATE_CONFIGURATOR_PRICE, {
        variantId: this.selectedLine.productVariant.id,
        quantity,
        selectedOptions,
      })
      .single$.subscribe((data: any) => {
        this.retailPrice = data.calculateConfiguratorPrice.retail as number;
        this.tradePrice = data.calculateConfiguratorPrice.trade as number;
        this.cdr.markForCheck();
      });
  }

  getOrderLineConfiguratorOptions(): any[] {
    const selectedOptions: any[] = [];
    const optionGroups = this.getConfigurableOptions();
    const optionsIds: string[] = (
      this.formGroup.get("optionGroups") as FormArray
    ).controls.map((c) => c.value);
    for (
      let optionGroupIndex = 0;
      optionGroupIndex < optionGroups.length;
      optionGroupIndex++
    ) {
      const optionGroup = optionGroups[optionGroupIndex];
      selectedOptions.push({
        optionGroupId: optionGroup.id,
        optionId: optionsIds[optionGroupIndex],
      });
    }
    let factor = 1;
    const measurementUnit = this.formGroup.get("measurementUnit")?.value;
    if (measurementUnit) {
      factor =
        this.measurementUnits.find((m) => m.name === measurementUnit)?.factor ??
        1;
    }
    const width = this.formGroup.get("width")?.value * factor;
    const height = this.formGroup.get("height")?.value * factor;
    if (width && height) {
      selectedOptions.push({ optionGroupId: "width", value: width });
      selectedOptions.push({ optionGroupId: "height", value: height });
    }
    return selectedOptions;
  }

  adjustOrderLine(): void {
    const selectedOptions = this.getOrderLineConfiguratorOptions();
    const input = {
      orderLineId: this.selectedLine.id,
      orderId: this.orderId,
      quantity: this.formGroup.get("quantity")?.value,
      selectedConfiguratorOptions: selectedOptions.map((s) =>
        JSON.stringify(s),
      ),
      additionalInformation: this.formGroup.get("additionalInformation")?.value,
      customUnitPriceExVAT: this.formGroup.get("customUnitPriceExVAT")?.value,
    };
    this.resolveWith();
    this.dataService
      .mutate(ADJUST_ORDER_LINE, input)
      .pipe(take(1))
      .subscribe((data: any) => {
        if (data.adjustDraftOrderLine?.id) {
          this.ns.success("OrderLine has been adjusted successfully");
        } else if (data.adjustDraftOrderLine?.message) {
          this.ns.error(data.adjustDraftOrderLine?.message);
        }
        this.cdr.markForCheck();
      });
    this.cdr.markForCheck();
  }

  cancel(): void {
    this.resolveWith();
  }

  onMeasurementUnitChange(meaurementUnitInfo: {
    name: MeasurementUnit;
    factor: number;
  }): void {
    const previousFactor =
      this.measurementUnits.find((pf) => pf.name === this.previousUnit)
        ?.factor ?? 1;
    const currentFactor = meaurementUnitInfo.factor;
    const width = this.formGroup.get("width")?.value;
    const height = this.formGroup.get("height")?.value;
    const interUnitConverstionRatio = previousFactor / currentFactor;
    const newWidth = width * interUnitConverstionRatio;
    const newHeight = height * interUnitConverstionRatio;
    this.previousUnit = meaurementUnitInfo.name;
    this.formGroup.get("width")?.setValue(this.roundToDecimalPlaces(newWidth));
    this.formGroup.get("height")?.setValue(parseFloat(newHeight.toFixed(2)));
  }

  onPresetSizeChange(selected: any): void {
    const selectedSize = this.presetSizes.find((s) => s.name === selected.name);
    const factor =
      this.measurementUnits.find(
        (u) => u.name === this.formGroup.get("measurementUnit")?.value,
      )?.factor ?? 1;
    if (selectedSize) {
      this.formGroup.patchValue({
        ...this.formGroup.getRawValue(),
        sizeType: selected.name,
        width: this.roundToDecimalPlaces(selectedSize.width / factor),
        height: this.roundToDecimalPlaces(selectedSize.height / factor),
      });
      this.cdr.markForCheck();
    }
  }

  roundToDecimalPlaces(value: number, decimalPlace: number = 3): number {
    return parseFloat(value.toFixed(decimalPlace));
  }
}
