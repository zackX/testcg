import { Component, OnInit, ChangeDetectorRef, Type } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  Dialog,
  ModalService,
  FormInputComponent,
} from "@vendure/admin-ui/core";
import { CustomFieldConfig } from "@vendure/common/lib/generated-types";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import {
  ConfiguratorOptionGroup,
  ConfiguratorOptionGroupRadio,
  ConfiguratorOptionGroupCheckBox,
  ConfiguratorOptionGroupNumber,
  ConfiguratorSelectableOption,
} from "../generated/generated-graphql-types";
import { GroupOptionNumberModalComponent } from "../configurator-option-group-number-modal/configurator-option-group-number-modal";
import { GroupOptionModalComponent } from "../configurator-option-group-modal/configurator-option-group-modal";
export interface ConfiguratorOptionGroupModalContent {
  optionGroupFormGroup: FormGroup;
}
@Component({
  templateUrl: "./configurator-options.component.html",
  styleUrls: ["./configurator-options.component.scss"],
})
export class ConfiguratorOptionsComponent
  implements FormInputComponent<CustomFieldConfig>, OnInit
{
  isListInput = false;
  readonly: boolean;
  config: CustomFieldConfig;
  formControl: FormControl;
  options: ConfiguratorOptionGroup[] = [];
  selectedOptionGroupdIndex: number;
  optionGroupFormGroup: FormGroup;

  selectedOptionGroupContent: ConfiguratorSelectableOption;

  constructor(
    private readonly fb: FormBuilder,
    private readonly cdr: ChangeDetectorRef,
    private readonly modalService: ModalService,
  ) {
    this.optionGroupFormGroup = fb.group({
      id: fb.control({}),
      name: fb.control({}),
      options: fb.array([]),
    });
  }

  ngOnInit(): void {
    if (this.formControl.value) {
      this.options = JSON.parse(this.formControl.value);
      for (const configuratorOptionsGroup of this.options) {
        if ((configuratorOptionsGroup as any)?.options) {
          for (const option of (configuratorOptionsGroup as any).options) {
            option.customFields = JSON.stringify(option.customFields);
          }
        }
      }
    }
  }

  deleteOptionGroup(index: number, event: any): void {
    this.options = [
      ...this.options.slice(0, index),
      ...this.options.slice(index + 1, this.options.length),
    ];
    this.parseAndMarkAsDirty();
    event.prevenDefault();
  }

  onDrop(event: CdkDragDrop<string[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
      this.parseAndMarkAsDirty();
    }
  }

  newConfiguratorOptionGroupCheckBox(): void {
    this.popUpModal(
      GroupOptionModalComponent,
      this.fb.group({
        __typename: "ConfiguratorOptionGroupCheckBox",
        id: "",
        name: "",
        options: [],
      }),
    );
    this.selectedOptionGroupdIndex = this.options.length;
  }

  newConfiguratorOptionGroupRadio(): void {
    this.popUpModal(
      GroupOptionModalComponent,
      this.fb.group({
        __typename: "ConfiguratorOptionGroupRadio",
        id: "",
        name: "",
        options: [],
      }),
    );
    this.selectedOptionGroupdIndex = this.options.length;
  }

  newConfiguratorOptionGroupNumber(): void {
    this.popUpModal(
      GroupOptionNumberModalComponent,
      this.fb.group({
        __typename: "ConfiguratorOptionGroupNumber",
        id: "",
        name: "",
        min: 0.01,
        max: 0.02,
      }),
    );
    this.selectedOptionGroupdIndex = this.options.length;
  }

  popUpEditModal(optionIndex: number): void {
    const content = this.options[optionIndex];
    this.selectedOptionGroupdIndex = optionIndex;
    if ("options" in content) {
      this.patchConfiguratorGroupWithOptions(content);
    } else {
      this.patchConfiguratorOptionGroupNumber(content);
    }
  }

  popUpModal(
    modal: Type<
      Dialog<ConfiguratorOptionGroup> & {
        optionGroupFormGroup: FormGroup;
      }
    >,
    content: FormGroup,
  ): void {
    this.modalService
      .fromComponent<
        Dialog<ConfiguratorOptionGroup> & {
          optionGroupFormGroup: FormGroup;
        },
        ConfiguratorOptionGroup
      >(modal, {
        size: "lg",
        locals: {
          optionGroupFormGroup: content,
        },
      })
      .subscribe((editedContent?: ConfiguratorOptionGroup) => {
        if (editedContent) {
          this.options[this.selectedOptionGroupdIndex] = editedContent;
          this.parseAndMarkAsDirty();
        }
      });
  }

  parseAndMarkAsDirty(): void {
    const copiedConst = JSON.parse(JSON.stringify(this.options));
    for (const configuratorOptionsGroup of copiedConst) {
      if (configuratorOptionsGroup?.options) {
        for (const option of configuratorOptionsGroup.options) {
          try {
            option.customFields = JSON.parse(option.customFields);
          } catch (e) {
            // it's an empty string in this case
          }
        }
      }
    }
    this.formControl.setValue(JSON.stringify(copiedConst));
    this.formControl.markAsDirty();
  }

  patchConfiguratorGroupWithOptions(
    fields: ConfiguratorOptionGroupCheckBox | ConfiguratorOptionGroupRadio,
  ): void {
    this.popUpModal(
      GroupOptionModalComponent,
      this.fb.group(
        {
          __typename: fields.__typename,
          id: fields.id,
          name: fields.name,
          options: this.fb.array(
            fields.options.map((x) =>
              this.fb.group({
                id: x.id,
                name: x.name,
                customFields: x.customFields,
              }),
            ),
            { validators: [Validators.required] },
          ),
        },
        { validators: [Validators.required] },
      ),
    );
  }

  patchConfiguratorOptionGroupNumber(
    fields: ConfiguratorOptionGroupNumber,
  ): void {
    this.popUpModal(
      GroupOptionNumberModalComponent,
      this.fb.group({
        __typename: fields.__typename,
        id: fields.id,
        name: fields.name,
        min: fields.min,
        max: fields.max,
      }),
    );
  }
}
