import type { APIContext } from "astro";
import { catalog } from "../api/vendure-catalog";

const { products, allCollections } = await catalog.get();

// Shows a list of all redirects
export async function get(context: APIContext): Promise<{ body: string }> {
  let body = "";
  // Redirect all collections
  allCollections.forEach((collection) => {
    body += `/${collection.slug}/ ${collection.path} 302 \n`;
  });
  body += "\n";
  // Redirect all products
  products.forEach((product) => {
    body += `/${product.slug}/ ${product.path} 302 \n`;
  });

  return {
    body,
  };
}
