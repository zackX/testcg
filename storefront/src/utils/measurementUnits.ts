export type MeasurementUnit = "mm" | "cm" | "meters" | "in" | "ft";
export const measurementUnits: Record<MeasurementUnit, number> = {
  mm: 1,
  cm: 10,
  meters: 1000,
  in: 25.4,
  ft: 304.8,
};

/**
 * Convert the given value from its source measurement unit to its target measurement. E.g convert 10cm to meters
 */
export function convertToMeasurementUnit(
  value: number,
  sourceUnit: MeasurementUnit,
  targetUnit: MeasurementUnit,
): number {
  const factor = measurementUnits[sourceUnit] / measurementUnits[targetUnit];
  return value * factor;
}

/**
 * Convert from mm to [selected unit] and round.
 * This helper also parses option value to float and asserts measurement is a valid MeasurementUnit
 */
export function convertFromMmToUnitAndRound(
  optionValue: string,
  measurement: string,
): number {
  return (
    Math.round(
      convertToMeasurementUnit(
        parseFloat(optionValue),
        "mm",
        measurement as MeasurementUnit,
      ) * 100,
    ) / 100
  );
}

/* Takes value and a measurement unit then converts it to mm and round them */

export function convertToMillimeters(
  value: number,
  unit: string,
): number | string {
  return (
    Math.round(
      convertToMeasurementUnit(value, unit as MeasurementUnit, "mm") * 100,
    ) / 100
  );
}
