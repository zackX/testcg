import { UserInputError } from "@vendure/core";
import { SelectedConfiguratorOption } from "../ui/generated/generated-graphql-types";

export function parseSelectedOption(
  stringifiedOption: string,
): SelectedConfiguratorOption {
  try {
    return JSON.parse(stringifiedOption);
  } catch (e: any) {
    throw new UserInputError(
      `Invalid JSON found for selected option ${stringifiedOption}`,
    );
  }
}
