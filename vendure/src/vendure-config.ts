import { AdminUiPlugin } from "@vendure/admin-ui-plugin";
import { AssetServerPlugin } from "@vendure/asset-server-plugin";
import {
  CollectionEvent,
  DefaultGuestCheckoutStrategy,
  DefaultLogger,
  DefaultSearchPlugin,
  LogLevel,
  OrderProcess,
  ProductEvent,
  ProductVariantEvent,
  VendureConfig,
  VendureLogger,
  defaultOrderProcess,
} from "@vendure/core";
import {
  EmailPlugin,
  emailAddressChangeHandler,
  orderConfirmationHandler,
  passwordResetHandler,
} from "@vendure/email-plugin";
import { MolliePlugin } from "@vendure/payments-plugin/package/mollie";
import { json } from "body-parser";
import path from "path";
import { CloudTasksPlugin } from "@pinelab/vendure-plugin-google-cloud-tasks";
import {
  GoogleStoragePlugin,
  GoogleStorageStrategy,
} from "@pinelab/vendure-plugin-google-storage-assets";
import { ConfiguratorPlugin } from "./configurator/configurator-plugin";
import { cloudLogger } from "./logger";
import { colourGraphicsCalculation } from "./configurator/api/cg-calculation";
import { PullDataFromGoogleSheetPlugin } from "./pull-data-from-google-sheet/google-sheet-plugin";
import { printingOrderProcess } from "./config/printing-order-process";
import { ColorGraphicsOptionSyncStrategy } from "./config/color-graphics-option-sync";
import { WebhookPlugin } from "@pinelab/vendure-plugin-webhook";
import { PrimaryCollectionPlugin } from "@pinelab/vendure-plugin-primary-collection";
import { CGOrderCodeStrategy } from "./config/cg-order-code.strategy";
import { ProductDocumentsPlugin } from "./product-documents/plugin";
import { customFields } from "./config/custom-fields";
import { SimplifiedCollectionsPlugin } from "vendure-simplified-collections-plugin";
import { CustomStateTransitionPlugin } from "./custom-state-transition/custom-state-transition.plugin";
import { PopularityScoresPlugin } from "@pinelab/vendure-plugin-popularity-scores";
import { AnonymizedOrderPlugin } from "@pinelab/vendure-plugin-anonymized-order";
import { ShippingPlugin } from "./shipping/shipping.plugin";
import {
  ShippingExtensionsPlugin,
  UKPostalCodeToGelocationConversionStrategy,
} from "@pinelab/vendure-plugin-shipping-extensions";
import { AdminSocialAuthPlugin } from "@pinelab/vendure-plugin-admin-social-auth";
import { DraftOrderPlugin } from "./draft-order/draft-order-plugin";
import {
  GoogleStorageInvoiceStrategy,
  InvoicePlugin,
} from "@pinelab/vendure-plugin-invoices";
import { PicklistPlugin } from "@pinelab/vendure-plugin-picklist";
import { getConfigurableOrderTotalWeightInGrams } from "./config/configurable-item-weight-calculator";
import { draftOrderCompletionHandler } from "./config/draft-order-completion.handler";
import { purchaseOrderChecker } from "./purchase-order/purchase-order-payment-checker";
import { purchaseOrderHandler } from "./purchase-order/purchase-order-handler";
import { invoiceLoadDataFn } from "./config/invoice-data-loader";
import { picklistLoadDataFn } from "./config/picklist-data-loader";
import { OrderExportPlugin } from "@pinelab/vendure-plugin-order-export";
import { XeroOrderExport } from "./order-export/xero-order-export";
import { ShippingOrderExport } from "./order-export/shipping-order-export";
import { MultiServerDbSessionCachePlugin } from "@pinelab/vendure-plugin-multiserver-db-sessioncache";

let logger: VendureLogger;
export let runningLocal = false;
export let isProd = false;
export let runningInWorker = false;
if (process.env.K_SERVICE) {
  // This means we are in CloudRun
  logger = cloudLogger;
  runningInWorker = process.env.K_SERVICE.includes("worker"); // Name of worker is worker or worker-test
} else {
  logger = new DefaultLogger({ level: LogLevel.Debug });
  runningLocal = true;
}
if (process.env.APP_ENV === "prod") {
  isProd = true;
}

export const config: VendureConfig = {
  logger,
  orderOptions: {
    orderCodeStrategy: new CGOrderCodeStrategy(),
    process: [defaultOrderProcess, printingOrderProcess] as Array<
      OrderProcess<any>
    >,
    guestCheckoutStrategy: new DefaultGuestCheckoutStrategy({
      allowGuestCheckoutForRegisteredCustomers: true,
    }),
  },
  customFields,
  apiOptions: {
    port: (process.env.PORT as unknown as number) ?? 3000,
    adminApiPath: "admin-api",
    adminApiPlayground: !!runningLocal,
    adminApiDebug: false, // turn this off for production
    shopApiPath: "shop-api",
    shopApiPlayground: true, // turn this off for production
    shopApiDebug: false, // turn this off for production
    shopListQueryLimit: 500,
    middleware: [
      {
        route: `/`,
        handler: json({ limit: "1mb" }),
      },
    ],
  },
  authOptions: {
    requireVerification: false,
    superadminCredentials: {
      identifier: process.env.SUPERADMIN_USERNAME,
      password: process.env.SUPERADMIN_PASSWORD,
    },
    tokenMethod: "bearer",
  },
  assetOptions: {
    permittedFileTypes: ["image/*", "video/*", "audio/*", ".pdf", ".epub"],
    uploadMaxFileSize: 36700160,
  },
  dbConnectionOptions: {
    type: "mysql",
    synchronize: false,
    logging: false,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    migrations: [path.join(__dirname, "./migrations/*.ts")],
    socketPath: runningLocal
      ? undefined
      : `/cloudsql/${process.env.SOCKET_CONNECTION_NAME}`,
  },
  taxOptions: {},
  shippingOptions: {},
  paymentOptions: {
    paymentMethodHandlers: [purchaseOrderHandler],
    paymentMethodEligibilityCheckers: [purchaseOrderChecker],
  },
  plugins: [
    AdminSocialAuthPlugin.init({
      google: {
        oAuthClientId: process.env.GOOGLE_OAUTH_CLIENT_ID,
      },
    }),
    PicklistPlugin.init({ loadDataFn: picklistLoadDataFn }),
    DraftOrderPlugin,
    OrderExportPlugin.init({
      // Optionally add your own strategies here
      exportStrategies: [new XeroOrderExport(), new ShippingOrderExport()],
    }),
    ShippingExtensionsPlugin.init({
      weightUnit: "kg",
      customFieldsTab: "Physical properties",
      orderAddressToGeolocationStrategy:
        new UKPostalCodeToGelocationConversionStrategy(),
      weightCalculationFunction: getConfigurableOrderTotalWeightInGrams,
    }),
    InvoicePlugin.init({
      // Used for generating download URLS for the admin ui
      vendureHost: process.env.VENDURE_HOST,
      loadDataFn: invoiceLoadDataFn,
      storageStrategy: new GoogleStorageInvoiceStrategy({
        bucketName: process.env.INVOICES_BUCKET,
      }),
    }),
    ShippingPlugin,
    AnonymizedOrderPlugin,
    SimplifiedCollectionsPlugin,
    PrimaryCollectionPlugin,
    ConfiguratorPlugin.init({
      calculation: colourGraphicsCalculation,
    }),
    ProductDocumentsPlugin,
    WebhookPlugin.init({
      delay: 2000,
      events: [ProductEvent, ProductVariantEvent, CollectionEvent],
    }),
    MolliePlugin.init({
      vendureHost: process.env.VENDURE_HOST,
      useDynamicRedirectUrl: true,
    }),
    CloudTasksPlugin.init({
      taskHandlerHost: process.env.WORKER_HOST,
      projectId: process.env.GCLOUD_PROJECT,
      location: "europe-west2",
      authSecret: process.env.CLOUD_TASKS_SECRET,
      queueSuffix: process.env.APP_ENV,
      clientOptions: {
        fallback: true,
      },
    }),
    // The plugin inlcuscusion is needed to expose the `thumbnail` field on Assets
    GoogleStoragePlugin,
    AssetServerPlugin.init({
      storageStrategyFactory: () =>
        new GoogleStorageStrategy({
          bucketName: process.env.BUCKET,
          thumbnails: {
            height: 500,
            width: 500,
          },
        }),
      route: "assets",
      assetUploadDir: "/tmp/vendure/assets",
    }),
    DefaultSearchPlugin,
    EmailPlugin.init({
      // Only for dev
      // devMode: true,
      // outputPath: path.join(__dirname, '../static/email/test-emails'),
      // route: 'mailbox',
      // Live settings
      transport: {
        type: "smtp",
        host: "smtp.zeptomail.eu",
        port: 587,
        secure: false,
        logging: false,
        debug: true,
        auth: {
          user: "emailapikey",
          pass: process.env.ZEPTOMAIL_KEY,
        },
      },
      handlers: [
        orderConfirmationHandler,
        passwordResetHandler,
        emailAddressChangeHandler,
        draftOrderCompletionHandler,
      ],
      templatePath: path.join(__dirname, "../static/email/templates"),
      globalTemplateVars: {
        fromAddress: '"Colour Graphics" <sales@colourgraphics.com>',
        verifyEmailAddressUrl: `${process.env.STOREFRONT}/verify`,
        passwordResetUrl: `${process.env.STOREFRONT}/password-reset`,
        changeEmailAddressUrl: `${process.env.STOREFRONT}/verify-email-address-change`,
        storefrontUrl: process.env.STOREFRONT,
        vendureHost: process.env.VENDURE_HOST,
      },
    }),
    PopularityScoresPlugin.init({
      endpointSecret: "test",
    }),
    PullDataFromGoogleSheetPlugin.init({
      dataStrategy: new ColorGraphicsOptionSyncStrategy(),
      googleApiKey: process.env.GOOGLE_API_KEY,
    }),
    CustomStateTransitionPlugin,
    // Production ready, precompiled admin UI
    AdminUiPlugin.init({
      route: "admin",
      port: 3002,
      adminUiConfig: {
        loginImageUrl: "https://source.unsplash.com/NOWf9wVANYQ",
        brand: "Colour Graphics",
        hideVendureBranding: true,
        hideVersion: false,
        loginUrl: "/social-auth/login",
      },
      app: {
        path: path.join(__dirname, "../__admin-ui/dist"),
      },
    }),
    MultiServerDbSessionCachePlugin,
  ],
};

if (config.dbConnectionOptions.synchronize) {
  throw Error(
    "Don't ever synchronize the DB!!! Use 'yarn migration:generate:test' to migrate the database",
  );
}
