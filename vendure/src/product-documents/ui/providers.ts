import { registerFormInputComponent } from "@vendure/admin-ui/core";
import { SelectDocumentsComponent } from "./select-documents.component";

export default [
  registerFormInputComponent("select-documents", SelectDocumentsComponent),
];
