import { Logger, UserInputError } from "@vendure/core";
import { SelectedOption } from "../types";
import {
  ConfiguratorOptionGroup,
  SelectedConfiguratorOption,
} from "../ui/generated/generated-graphql-types";
import { loggerCtx } from "../constants";

/**
 * Get the full selected options based on the given optionGroupIds and optionIds selected by customer.
 * Also returns default options if no option is selected for a group
 */
export function getFullSelectedOptions(
  productOptionGroups: ConfiguratorOptionGroup[],
  selectedOptionInputs: SelectedConfiguratorOption[],
): SelectedOption[] {
  const selectedOptions: SelectedOption[] = [];
  productOptionGroups.forEach((optionGroup) => {
    // Find the selected option for current OptionGroup
    const selectedOptionInput = selectedOptionInputs.find(
      (s) => s.optionGroupId === optionGroup.id,
    );
    if (optionGroup.__typename === "ConfiguratorOptionGroupNumber") {
      // Use minimum if no value given
      const value = selectedOptionInput?.value ?? optionGroup.min ?? 0;
      selectedOptions.push({
        optionGroupName: optionGroup.name,
        optionGroupId: optionGroup.id,
        value,
      });
    } else if (optionGroup.__typename === "ConfiguratorOptionGroupCheckBox") {
      // Handle checkbox option groups
      // Checkbox can have multiple selections
      selectedOptionInput?.optionIds?.forEach((optionId) => {
        const selectedOption = optionGroup.options.find(
          (o) => o.id === optionId,
        );
        if (!selectedOption) {
          throw new UserInputError(
            `Option ${selectedOptionInput?.optionId} is not a valid option for checkbox ${optionGroup.name}`,
          );
        }
        selectedOptions.push({
          optionGroupName: optionGroup.name,
          optionGroupId: optionGroup.id,
          id: selectedOption.id,
          name: selectedOption.name,
          customFields: selectedOption.customFields,
        });
      });
    } else if (optionGroup.__typename === "ConfiguratorOptionGroupRadio") {
      // Handle radio option groups
      if (optionGroup.options.length === 0) {
        throw new Error(`No options configured radio ${optionGroup.name}`);
      }
      let selectedOption = optionGroup.options.find(
        (o) => o.id === selectedOptionInput?.optionId,
      );
      if (!selectedOptionInput?.optionId) {
        // No option selected, set default
        const defaultOption = optionGroup.options[0];
        Logger.verbose(
          `Selecting default "${defaultOption.id}" for radio ${optionGroup.name}`,
          loggerCtx,
        );
        selectedOption = defaultOption;
      }
      if (!selectedOption) {
        Logger.error(
          `Option ${selectedOptionInput?.optionId} is not a valid option for radio ${optionGroup.name}`,
        );
        return;
      }
      selectedOptions.push({
        optionGroupName: optionGroup.name,
        optionGroupId: optionGroup.id,
        id: selectedOption.id,
        name: selectedOption.name,
        customFields: selectedOption.customFields,
      });
    } else {
      throw Error(`Unknown option group type ${optionGroup.__typename}`);
    }
  });
  return selectedOptions;
}
