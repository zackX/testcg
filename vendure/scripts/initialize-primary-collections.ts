import { RequestContextService, bootstrap } from "@vendure/core";
import { PrimaryCollectionHelperService } from "@pinelab/vendure-plugin-primary-collection/dist/api/primary-collections-helper.service";

/**
 * This script resets ALL primary collections for products. Sets the first collection as primary collection.
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config({ path: process.env.ENV_FILE });
// eslint-disable-next-line @typescript-eslint/no-floating-promises
import("../src/vendure-config").then(async ({ config }) => {
  console.log("Populating primary collections");
  const app = await bootstrap(config);
  const ctx = await app.get(RequestContextService).create({
    apiType: "admin",
  });
  await app
    .get(PrimaryCollectionHelperService)
    .setPrimaryCollectionForAllProducts(ctx);
  console.log("Populated primary collections");
  process.exit(0);
});
