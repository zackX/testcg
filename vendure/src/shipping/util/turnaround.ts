import { ShippingMethod } from "@vendure/core";

export const turnaroundShippingMethodCodeFragment = "turnaround";
/**
 * Check if a ShippingMethod is a turnaround method.
 */
export function isTurnaroundMethod(
  shippingMethod: Pick<ShippingMethod, "code">,
): boolean {
  return shippingMethod?.code?.includes(turnaroundShippingMethodCodeFragment);
}

/**
 * Get the turnaround days from a ShippingMethod.
 */
export function getTurnaroundDays(
  shippingMethod: Pick<ShippingMethod, "code">,
): number {
  const matches = shippingMethod.code.match(/(\d+)/);
  if (matches) {
    return parseInt(matches[0]);
  }
  return 0;
}
