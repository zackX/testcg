import { GoogleSheetDataStrategy } from "./api/strategies/google-sheet-data.strategy";

export interface PullDataFromGoogleSheetPluginConfig {
  dataStrategy: GoogleSheetDataStrategy;
  googleApiKey: string;
}
