import { gql } from "graphql-tag";

export const READ_GOOGLE_SHEET = gql`
  mutation ReadGoogleSheet($sheetInput: GoogleSheetInput!) {
    readGoogleSheet(sheetInput: $sheetInput) {
      success
    }
  }
`;

export const GET_ACTIVE_GOOGLE_SHEET_META_DATA = gql`
  query GetActiveGoogleSheetMetaData {
    activeGoogleSheetMetaData {
      sheetName
      url
    }
  }
`;
