/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["proxima-soft"],
        serif: ["proxima-soft"],
        mono: ["proxima-soft"],
        display: ["proxima-soft"],
        body: ["proxima-soft"],
      },
      colors: {
        primary: {
          DEFAULT: "#FF007A",
          50: "#FFB8DA",
          100: "#FFA3CF",
          200: "#FF7ABA",
          300: "#FF52A5",
          400: "#FF298F",
          500: "#FF007A",
          600: "#C7005F",
          700: "#8F0044",
          800: "#570029",
          900: "#1F000F",
          950: "#030001",
        },
        alert: {
          DEFAULT: "#FFE6F2",
        },
      },
      screens: {
        mobile: { max: "833px" }, // iphone 11 pro max
        // => @media (max-width: 833px) { ... }

        tablet: "834px", // ipad Pro 11
        // => @media (min-width: 834px) { ... }

        laptop: "1280px", // MacBook Pro
        // => @media (min-width: 1280px) { ... }

        desktop: "1920px",
        // => @media (min-width: 1920px) { ... }

        widescreen: "2880px",
        // => @media (min-width: 2880) { ... }
      },
    },
  },
  plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")],
};
