import { Args, Mutation, Resolver } from "@nestjs/graphql";
import {
  Ctx,
  Order,
  OrderStateTransitionError,
  Transaction,
  RequestContext,
} from "@vendure/core";
import { CustomStateTransitionService } from "./custom-state-transition.service";

@Resolver()
export class CustomStateTransitionShopResolver {
  constructor(
    private readonly customStateTransitionService: CustomStateTransitionService,
  ) {}

  @Mutation()
  @Transaction()
  async transitionOrderToAwaitingPrepress(
    @Ctx() ctx: RequestContext,
    @Args() args: { orderCode: string; emailAddress: string },
  ): Promise<Order | OrderStateTransitionError | undefined> {
    return await this.customStateTransitionService.transitionOrderToAwaitingPrepress(
      ctx,
      args.orderCode,
      args.emailAddress,
    );
  }
}
