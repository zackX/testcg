import { gql } from "graphql-tag";

// Needed for codegen
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const scalars = gql`
  scalar JSON
  scalar Money
`;

export const apiSchema = gql`
  extend type Customer {
    isTradeCustomer: Boolean!
  }

  extend type Product {
    configuratorOptionGroups: [ConfiguratorOptionGroup!]
    isConfigurable: Boolean!
  }

  extend type OrderLine {
    selectedConfiguratorOptions: [HumanReadableSelectedConfiguratorOption!]
  }

  type ConfiguratorPrice {
    trade: Money!
    tradeWithTax: Money!
    retail: Money!
    retailWithTax: Money!
  }

  extend type ProductVariant {
    """
    The default configurator price will have both retail and trade,
    because at build time we dont know which one will be used.
    """
    defaultConfiguratorPrice: ConfiguratorPrice
  }

  union ConfiguratorOptionGroup =
      ConfiguratorOptionGroupRadio
    | ConfiguratorOptionGroupCheckBox
    | ConfiguratorOptionGroupNumber

  """
  Configurator option group that only allows
  selection of one of its options
  """
  type ConfiguratorOptionGroupRadio {
    id: String!
    name: String!
    options: [ConfiguratorSelectableOption!]!
  }

  """
  Configurator option group that allows selection of
  multiple options
  """
  type ConfiguratorOptionGroupCheckBox {
    id: String!
    name: String!
    options: [ConfiguratorSelectableOption!]!
  }

  type ConfiguratorSelectableOption {
    id: String!
    name: String!
    customFields: JSON
  }

  """
  Human readable selected configurator options. Should only be used for displaying purposes
  """
  type HumanReadableSelectedConfiguratorOption {
    optionGroupName: String!
    optionValue: String!
  }

  """
  Option group that allows the user to enter a number
  """
  type ConfiguratorOptionGroupNumber {
    id: String!
    name: String!
    min: Float!
    max: Float!
  }

  """
  A map of option group ID's and option ID's that define what options have been selected by a customer.

  Should either have an optionId for radio option groups,
  multiple optionId's for Checkbox option groups,
  or, a value for number option groups
  """
  input SelectedConfiguratorOption {
    optionGroupId: String!
    optionId: String
    optionIds: [String!]
    value: Float
  }

  extend type Query {
    """
    Calculate the price of a product variant with given selected options.
    Price is incl. tax or ex tax depending on the channel settings.
    """
    calculateConfiguratorPrice(
      variantId: ID!
      quantity: Int
      selectedOptions: [SelectedConfiguratorOption!]!
    ): ConfiguratorPrice!
    storefront: String
  }
`;
