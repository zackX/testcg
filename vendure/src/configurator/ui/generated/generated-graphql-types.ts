export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends " $fragmentName" | "__typename" ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  JSON: { input: any; output: any };
  Money: { input: any; output: any };
  Success: { input: any; output: any };
};

export type ConfiguratorOptionGroup =
  | ConfiguratorOptionGroupCheckBox
  | ConfiguratorOptionGroupNumber
  | ConfiguratorOptionGroupRadio;

/**
 * Configurator option group that allows selection of
 * multiple options
 */
export type ConfiguratorOptionGroupCheckBox = {
  __typename?: "ConfiguratorOptionGroupCheckBox";
  id: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
  options: Array<ConfiguratorSelectableOption>;
};

/** Option group that allows the user to enter a number */
export type ConfiguratorOptionGroupNumber = {
  __typename?: "ConfiguratorOptionGroupNumber";
  id: Scalars["String"]["output"];
  max: Scalars["Float"]["output"];
  min: Scalars["Float"]["output"];
  name: Scalars["String"]["output"];
};

/**
 * Configurator option group that only allows
 * selection of one of its options
 */
export type ConfiguratorOptionGroupRadio = {
  __typename?: "ConfiguratorOptionGroupRadio";
  id: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
  options: Array<ConfiguratorSelectableOption>;
};

export type ConfiguratorPrice = {
  __typename?: "ConfiguratorPrice";
  retail: Scalars["Money"]["output"];
  retailWithTax: Scalars["Money"]["output"];
  trade: Scalars["Money"]["output"];
  tradeWithTax: Scalars["Money"]["output"];
};

export type ConfiguratorSelectableOption = {
  __typename?: "ConfiguratorSelectableOption";
  customFields?: Maybe<Scalars["JSON"]["output"]>;
  id: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
};

export type Customer = {
  __typename?: "Customer";
  isTradeCustomer: Scalars["Boolean"]["output"];
};

export type GoogleSheetInput = {
  sheetName: Scalars["String"]["input"];
  url: Scalars["String"]["input"];
};

export type GoogleSheetMetaData = {
  __typename?: "GoogleSheetMetaData";
  sheetName: Scalars["String"]["output"];
  url: Scalars["String"]["output"];
};

/** Human readable selected configurator options. Should only be used for displaying purposes */
export type HumanReadableSelectedConfiguratorOption = {
  __typename?: "HumanReadableSelectedConfiguratorOption";
  optionGroupName: Scalars["String"]["output"];
  optionValue: Scalars["String"]["output"];
};

export type Mutation = {
  __typename?: "Mutation";
  readGoogleSheet: Scalars["Success"]["output"];
};

export type MutationReadGoogleSheetArgs = {
  sheetInput: GoogleSheetInput;
};

export type OrderLine = {
  __typename?: "OrderLine";
  selectedConfiguratorOptions?: Maybe<
    Array<HumanReadableSelectedConfiguratorOption>
  >;
};

export type Product = {
  __typename?: "Product";
  configuratorOptionGroups?: Maybe<Array<ConfiguratorOptionGroup>>;
  isConfigurable: Scalars["Boolean"]["output"];
};

export type ProductVariant = {
  __typename?: "ProductVariant";
  /**
   * The default configurator price will have both retail and trade,
   * because at build time we dont know which one will be used.
   */
  defaultConfiguratorPrice?: Maybe<ConfiguratorPrice>;
};

export type Query = {
  __typename?: "Query";
  activeGoogleSheetMetaData?: Maybe<GoogleSheetMetaData>;
  /**
   * Calculate the price of a product variant with given selected options.
   * Price is incl. tax or ex tax depending on the channel settings.
   */
  calculateConfiguratorPrice: ConfiguratorPrice;
  storefront?: Maybe<Scalars["String"]["output"]>;
};

export type QueryCalculateConfiguratorPriceArgs = {
  quantity?: InputMaybe<Scalars["Int"]["input"]>;
  selectedOptions: Array<SelectedConfiguratorOption>;
  variantId: Scalars["ID"]["input"];
};

/**
 * A map of option group ID's and option ID's that define what options have been selected by a customer.
 *
 * Should either have an optionId for radio option groups,
 * multiple optionId's for Checkbox option groups,
 * or, a value for number option groups
 */
export type SelectedConfiguratorOption = {
  optionGroupId: Scalars["String"]["input"];
  optionId?: InputMaybe<Scalars["String"]["input"]>;
  optionIds?: InputMaybe<Array<Scalars["String"]["input"]>>;
  value?: InputMaybe<Scalars["Float"]["input"]>;
};
