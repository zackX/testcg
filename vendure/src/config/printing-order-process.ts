import { OrderProcess } from "@vendure/core";

export const printingOrderProcess: OrderProcess<
  | "AwaitingArtwork"
  | "AwaitingPrepress"
  | "InProduction"
  | "AwaitingCollection"
  | "AttentionRequired"
> = {
  transitions: {
    PaymentSettled: {
      to: ["AwaitingArtwork", "AwaitingPrepress", "Cancelled"],
      mergeStrategy: "merge",
    },
    AwaitingArtwork: {
      to: ["AwaitingPrepress", "Cancelled", "Modifying"],
      mergeStrategy: "merge",
    },
    AwaitingPrepress: {
      to: ["InProduction", "AttentionRequired", "Cancelled", "Modifying"],
      mergeStrategy: "merge",
    },
    InProduction: {
      to: [
        "Shipped",
        "AwaitingCollection",
        "AttentionRequired",
        "Cancelled",
        "Modifying",
      ],
      mergeStrategy: "merge",
    },
    AwaitingCollection: {
      to: ["Delivered", "Modifying"],
      mergeStrategy: "merge",
    },
    AttentionRequired: {
      to: ["Cancelled", "AwaitingPrepress", "Modifying"],
      mergeStrategy: "merge",
    },
    Modifying: {
      to: [
        "AwaitingArtwork",
        "AwaitingPrepress",
        "InProduction",
        "AwaitingCollection",
        "AttentionRequired",
      ],
      mergeStrategy: "merge",
    },
    ArrangingAdditionalPayment: {
      to: [
        "AwaitingArtwork",
        "AwaitingPrepress",
        "InProduction",
        "AwaitingCollection",
        "AttentionRequired",
      ],
      mergeStrategy: "merge",
    },
  },
};
