import { gql } from "graphql-tag";

export const OrderFieldsFragment = gql`
  fragment OrderFields on Order {
    id
    orderPlacedAt
    code
    state
    active
    total
    totalWithTax
    subTotal
    shipping
    shippingWithTax
    estimatedDeliveryDate
    shippingAddress {
      fullName
      company
      streetLine1
      streetLine2
      city
      postalCode
      country
    }
    customFields {
      shippingDate
    }
    customer {
      id
      firstName
      lastName
      emailAddress
    }
    lines {
      id
      quantity
      productVariant {
        id
        product {
          id
          name
        }
      }
      discounts {
        adjustmentSource
        amount
        amountWithTax
        description
        type
      }
      selectedConfiguratorOptions {
        optionGroupName
        optionValue
      }
    }
  }
`;

export const SetShippingAddress = gql`
  ${OrderFieldsFragment}
  mutation setOrderShippingAddress($input: CreateAddressInput!) {
    setOrderShippingAddress(input: $input) {
      ... on Order {
        ...OrderFields
      }
    }
  }
`;

export const SetCustomerForOrder = gql`
  ${OrderFieldsFragment}
  mutation setCustomerForOrder($input: CreateCustomerInput!) {
    setCustomerForOrder(input: $input) {
      ... on Order {
        ...OrderFields
      }
    }
  }
`;

export const SetOrderShippingMethod = gql`
  ${OrderFieldsFragment}
  mutation setOrderShippingMethod($id: [ID!]!) {
    setOrderShippingMethod(shippingMethodId: $id) {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const TransitionToState = gql`
  ${OrderFieldsFragment}
  mutation transitionOrderToState($state: String!) {
    transitionOrderToState(state: $state) {
      ... on Order {
        ...OrderFields
      }
      ... on OrderStateTransitionError {
        errorCode
        message
        transitionError
      }
    }
  }
`;

export const AddPaymentToOrder = gql`
  ${OrderFieldsFragment}
  mutation addPaymentToOrder($input: PaymentInput!) {
    addPaymentToOrder(input: $input) {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const UpdateProduct = gql`
  mutation UpdateProduct($input: UpdateProductInput!) {
    updateProduct(input: $input) {
      customFields {
        configuratorOptionGroups
        weight
      }
      configuratorOptionGroups {
        ... on ConfiguratorOptionGroupRadio {
          options {
            customFields
          }
        }
      }
      __typename
    }
  }
`;

export const GetProduct = gql`
  query product($id: ID) {
    product(id: $id) {
      id
      variants {
        defaultConfiguratorPrice {
          retail
          trade
        }
      }
    }
  }
`;

export const AddItemToOrder = gql`
  ${OrderFieldsFragment}
  mutation addItemToOrder(
    $productVariantId: ID!
    $quantity: Int!
    $customFields: OrderLineCustomFieldsInput
  ) {
    addItemToOrder(
      productVariantId: $productVariantId
      quantity: $quantity
      customFields: $customFields
    ) {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const AddItemToDraftOrder = gql`
  ${OrderFieldsFragment}
  mutation addItemToDraftOrder(
    $orderId: ID!
    $input: AddItemToDraftOrderInput!
  ) {
    addItemToDraftOrder(orderId: $orderId, input: $input) {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const CREATE_DRAFT_ORDER = gql`
  ${OrderFieldsFragment}
  mutation CreateDraftOrder {
    createDraftOrder {
      ...OrderFields
    }
  }
`;

export const RemoveAllOrderLines = gql`
  ${OrderFieldsFragment}
  mutation removeAllOrderLines {
    removeAllOrderLines {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const CalculateConfiguratorPrice = gql`
  query calculateConfiguratorPrice(
    $variantId: ID!
    $quantity: Int!
    $selectedOptions: [SelectedConfiguratorOption!]!
  ) {
    calculateConfiguratorPrice(
      variantId: $variantId
      quantity: $quantity
      selectedOptions: $selectedOptions
    ) {
      retail
      trade
    }
  }
`;

export const transitionOrderToState = gql`
  mutation transitionOrderToState($id: ID!, $state: String!) {
    transitionOrderToState(id: $id, state: $state) {
      ... on Order {
        id
        state
      }
      ... on ErrorResult {
        errorCode
        message
      }
      ... on OrderStateTransitionError {
        transitionError
        __typename
      }
      __typename
    }
  }
`;

export const GET_ORDER_BY_ID = gql`
  ${OrderFieldsFragment}
  query GetOrderById($id: ID!) {
    order(id: $id) {
      ...OrderFields
    }
  }
`;

export const TRANSITION_ORDER_TO_AWAITING_PREPRESS = gql`
  ${OrderFieldsFragment}
  mutation TransitionOrderToAwaitingPrepress(
    $orderCode: String!
    $emailAddress: String!
  ) {
    transitionOrderToAwaitingPrepress(
      orderCode: $orderCode
      emailAddress: $emailAddress
    ) {
      ... on Order {
        ...OrderFields
      }
      ... on ErrorResult {
        errorCode
        message
      }
    }
  }
`;

export const ActiveCustomer = gql`
  {
    activeCustomer {
      id
      emailAddress
      isTradeCustomer
    }
  }
`;

export const CreateGroup = gql`
  mutation CreateCustomerGroup($input: CreateCustomerGroupInput!) {
    createCustomerGroup(input: $input) {
      id
      createdAt
      updatedAt
      name
      __typename
    }
  }
`;

export const AddCustomersToGroup = gql`
  mutation AddCustomersToGroup($groupId: ID!, $customerIds: [ID!]!) {
    addCustomersToGroup(customerGroupId: $groupId, customerIds: $customerIds) {
      id
      createdAt
      updatedAt
      name
      customers {
        items {
          id
          emailAddress
        }
      }
    }
  }
`;

export const CREATE_SHIPPING_METHOD = gql`
  mutation CreateShippingMethod($input: CreateShippingMethodInput!) {
    createShippingMethod(input: $input) {
      ... on ShippingMethod {
        id
        code
      }
      __typename
    }
  }
`;
