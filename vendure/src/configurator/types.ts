import { ID, Injector, ProductVariant, RequestContext } from "@vendure/core";

/**
 * An interface to define how the price of a product variant is calculated.
 */
export interface ConfiguratorCalculation {
  // Don't change ID's or remove configs, this will corrupt the DB!
  id: string;
  name: string;
  calculateFn: ConfiguratorCalculationFn;
}

export interface ConfiguratorCalculationResult {
  retail: number;
  trade: number;
}

/**
 * This should always return the price without tax
 */
export type ConfiguratorCalculationFn = (
  ctx: RequestContext,
  injector: Injector,
  productVariant: ProductVariant,
  quantity: number,
  /**
   * The options selected by the client.
   * For Checkbox options, the array will contain multiple selected options with the same groupId
   */
  selectedOptions: SelectedOption[],
) => Promise<ConfiguratorCalculationResult>;

export interface SelectedNumberOption {
  optionGroupName: string;
  optionGroupId: ID;
  value: number;
}

/**
 * ChoiceOption can either be a selected checkbox or radio option
 */
export interface SelectedChoiceOption {
  optionGroupName: string;
  optionGroupId: ID;
  id: string;
  name: string;
  customFields?: any;
}

/**
 * The option selected by the client, including any customFields if defined.
 */
export type SelectedOption = SelectedNumberOption | SelectedChoiceOption;
