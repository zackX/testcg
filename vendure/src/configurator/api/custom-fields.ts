import { CustomFieldConfig, LanguageCode } from "@vendure/core";

export const configuratorProductFields: CustomFieldConfig[] = [
  // ------------------ option groups ------------------
  {
    name: "configuratorOptionGroups",
    label: [
      {
        languageCode: LanguageCode.en,
        value: "Configurator options",
      },
    ],
    type: "text",
    list: false,
    public: true,
    readonly: false,
    nullable: true,
    ui: {
      tab: "Configurator",
      component: "configurator-option-groups-form-input",
    },
  },
];

export const configuratorOrderLineFields: CustomFieldConfig[] = [
  {
    name: "selectedConfiguratorOptions",
    type: "text",
    list: true,
    ui: { component: "draft-order-line-empty-list-ui" },
    description: [
      {
        languageCode: LanguageCode.en,
        value: "A stringified version of a single SelectedConfiguratorOption",
      },
    ],
  },
  {
    name: "customUnitPriceExVAT",
    type: "int",
    nullable: true,
    defaultValue: 0,
    ui: {
      component: "draft-order-line-empty-ui",
    },
    description: [
      {
        languageCode: LanguageCode.en,
        value: "Custom Unit Price Excluding VAT",
      },
    ],
  },
];
export const configuratorOrderFields: CustomFieldConfig[] = [
  {
    name: "artworkReceived",
    type: "boolean",
    public: false,
    label: [
      {
        languageCode: LanguageCode.en,
        value: "Artwork already received",
      },
    ],
    description: [
      {
        languageCode: LanguageCode.en,
        value:
          "Check this if a customer doesn't have to upload artwork themselves",
      },
    ],
  },
  {
    name: "customerReference",
    type: "string",
    label: [
      {
        languageCode: LanguageCode.en,
        value: "Customer Reference (Purchase Order)",
      },
    ],
  },
];

export const addressCustomFields: CustomFieldConfig[] = [
  {
    name: "addressline3",
    type: "string",
    description: [
      {
        languageCode: LanguageCode.en,
        value: "Address line 3",
      },
    ],
  },
];
