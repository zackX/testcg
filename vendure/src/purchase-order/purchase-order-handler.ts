import { LanguageCode, Logger, PaymentMethodHandler } from "@vendure/core";

/**
 * Marks an order as placed, without any payment needed. This is used for purchase orders, where customers need to be in a specific group.
 */
export const purchaseOrderHandler = new PaymentMethodHandler({
  code: "purchase-order",
  description: [
    {
      languageCode: LanguageCode.en,
      value: "Purchase order, no payment needed",
    },
  ],
  args: {},
  createPayment: (ctx, order, amount, args, metadata) => {
    Logger.info(
      `'${order.code}' settled as purchase order for customer '${order.customer?.emailAddress}'`,
      "PurchaseOrderHandler",
    );
    return {
      amount,
      state: "Settled",
      metadata: { public: "Settled as purchase order" },
    };
  },
  settlePayment: () => ({
    success: true,
  }),
});
