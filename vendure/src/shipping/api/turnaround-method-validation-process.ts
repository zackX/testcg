import { EntityHydrator, OrderProcess } from "@vendure/core";
import { isTurnaroundMethod } from "../util/turnaround";

let entityHydrator: EntityHydrator;

export const turnaroundMethodValidationProcess: OrderProcess<"AddingItems"> = {
  init(injector) {
    entityHydrator = injector.get(EntityHydrator);
  },

  async onTransitionStart(fromState, toState, data) {
    if (fromState === "AddingItems" && toState === "ArrangingPayment") {
      await entityHydrator.hydrate(data.ctx, data.order, {
        relations: ["shippingLines.shippingMethod"],
      });
      const hasTurnaroundMethod = data.order.shippingLines.some((line) =>
        isTurnaroundMethod(line.shippingMethod),
      );
      if (!hasTurnaroundMethod) {
        return `Order ${data.order.code} has no turnaround method set`;
      }
    }
  },
};
