import { RequestContext, Injector } from "@vendure/core";

export interface GoogleSheetDataStrategy {
  handleSheetData: (
    ctx: RequestContext,
    injector: Injector,
    sheetData: string[][],
  ) => Promise<void>;
}
