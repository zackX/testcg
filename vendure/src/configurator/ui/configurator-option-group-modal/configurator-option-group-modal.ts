import { Component, ChangeDetectorRef } from "@angular/core";
import { Dialog } from "@vendure/admin-ui/core";
import { FormGroup, FormBuilder, FormArray, Validators } from "@angular/forms";
import { ConfigArgDefinition } from "@vendure/common/lib/generated-types";
import { ConfiguratorOptionGroup } from "../generated/generated-graphql-types";
@Component({
  selector: "app-component-overview",
  templateUrl: "./configurator-option-group-modal.html",
})
export class GroupOptionModalComponent
  implements Dialog<ConfiguratorOptionGroup>
{
  optionGroupFormGroup: FormGroup;
  jsonFormInputConfigArgsDef: ConfigArgDefinition = {
    name: "customFields",
    type: "json",
    list: false,
    required: false,
    ui: { component: "json-editor-form-input" },
  };

  selectedOptionsIndex: number[] = [];
  resolveWith: (result?: ConfiguratorOptionGroup) => void;
  constructor(
    private readonly fb: FormBuilder,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  validateOptionGroupNumberModalForm(): boolean {
    const idControl = this.optionGroupFormGroup.get("id");
    const nameControl = this.optionGroupFormGroup.get("name");
    const optionsControl = this.optionGroupFormGroup.get(
      "options",
    ) as FormArray;
    if (
      optionsControl.controls &&
      typeof optionsControl.controls[Symbol.iterator] === "function"
    ) {
      for (const control of optionsControl.controls) {
        if (!control.valid) {
          return false;
        }
        const customFieldsControl = control.get("customFields");

        if (customFieldsControl?.value) {
          try {
            JSON.parse(customFieldsControl.value);
          } catch (e) {
            return false;
          }
        }
      }
    } else {
      return false;
    }
    if (idControl === null || nameControl === null) {
      return true;
    }
    if (!idControl.valid || !nameControl.valid) {
      return false;
    }
    return true;
  }

  newOptions(): void {
    if ((this.optionGroupFormGroup.controls.options as FormArray).controls) {
      (this.optionGroupFormGroup.controls.options as FormArray).controls.push(
        this.fb.group(
          {
            id: "",
            name: "",
            customFields: JSON.stringify({}),
          },
          { validators: [Validators.required] },
        ),
      );
    } else {
      (this.optionGroupFormGroup.controls.options as FormArray) = this.fb.array(
        [
          this.fb.group(
            {
              id: "",
              name: "",
              customFields: "",
            },
            { validators: [Validators.required] },
          ),
        ],
        { validators: [Validators.required] },
      );
    }
    this.optionGroupFormGroup.markAsDirty();
    this.cdr.detectChanges();
  }

  deleteSelected(): void {
    const optionsArray = this.optionGroupFormGroup.get("options") as FormArray;
    if (this.selectedOptionsIndex.length) {
      for (const index of this.selectedOptionsIndex) {
        optionsArray.removeAt(index);
      }
    }
    if (optionsArray.length) {
      this.optionGroupFormGroup.markAsDirty();
    }
    this.cdr.detectChanges();
  }
}
