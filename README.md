# Colour Graphics

New shop for Colour Graphics built with Vendure and Astro.

- Vendure is accessible at https://vendure-yms3mc4wqa-nw.a.run.app/admin/
- Storefront is available at https://colour-graphics.netlify.app/

# Features

This section describes the most important parts of the Colour Graphics project.

## Configurable Products

Most products are configurable. Customers can configure a product based on specific options. This customization does not use variants, because there would be too many variants.

- Configurable products have option groups (e.g. `Thickness`).
- Those option groups can have options that can be selected by customers (e.g. `3mm` or `5mm`).
- Most products have a width and height.
- Options can have a fixed price, or the price is calculated based on a chosen size. E.g. square meter, or width.
- Option prices are managed in this sheet and synced into Vendure by manual action via the admin API. https://docs.google.com/spreadsheets/d/1QumO_VIZJ1zsRAA7j4Eer7EJeGo_ThD-P5Y8k1MTYuw/edit#gid=635504534

## Turnaround methods

Customers can choose a turnaround time (production time), and so pay extra to speed up production of their order.

- Turnaround methods are based on the built in Shipping Methods
- An order must have 1 shipping method and 1 turnaround method selected

## Draft orders

- Admins can also add configurable products to draft orders
- A custom Order Status page has been created, where customers can pay for their order and see the status. Example: https://colourgraphics.com/order/?code=CG-230224-35L5J&email=martijn@pinelab.studio

# Architecture and services

- Storefront is hosted at Netlify: https://app.netlify.com/sites/colour-graphics/overview
- Vendure backend is managed at Google Cloud:
  - Server logs: https://console.cloud.google.com/logs/?project=colour-graphics-com (The 'saved' tab has more readable log queries)
  - Cloud Run instances: https://console.cloud.google.com/run?project=colour-graphics-com
  - MySQL database https://console.cloud.google.com/sql/instances?project=colour-graphics-com
- DNS is managed at https://us-east-1.console.aws.amazon.com/route53/v2/home
- Uptime checks and log alerts are configured in Google Cloud: https://console.cloud.google.com/monitoring/alerting/policies?project=colour-graphics-com

## Development

- Run `yarn setup` in the root of this repository. This will install the root dependencies and dependencies in `storefront` and `vendure`
- Run `yarn lint:fix` to format and lint your code
- Run `yarn lint:check` to check if your code is properly formatted and linted

### git-crypt

When checking out this repo, the `vendure/.env` file will be encrypted, because it contains sensitive information. To decrypt it:

1. `sudo apt install git-crypt` or `brew install git-crypt`
2. Ask someone with access to the git-crypt key for the `default` keyfile. (`default` is the actual name of the file)
3. Place the file `default` in `.git/git-crypt/keys/`
4. `git-crypt unlock .git/git-crypt/keys/default`

You don't have to lock/unlock everytime you commit, git-crypt will automatically encrypt the env file before committing.

### Running Storefront and Vendure both locally

Both the Storefront and Vendure can be run locally seperately, for more info on that, check the README's in Vendure and Storefront directories.

To run both together locally, you need 2 terminal/console windows:

1. For the Vendure server
2. For the Storefront/Astro

In terminal 1:

1. `cd vendure`
2. `yarn start`
3. The server is now running on `localhost:3000/admin`

In terminal 2:

1. `cd storefront`
2. Add to the bottom of the .env file in `storefront`:
   `PUBLIC_VENDURE_HOST=http://localhost:3000/shop-api`
3. `yarn dev`. This will make Astro fetch data from your local Vendure
4. You can now access the storefront at `localhost:3000`
