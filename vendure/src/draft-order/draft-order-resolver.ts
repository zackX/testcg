import createMollieClient from "@mollie/api-client";
import { CreateParameters } from "@mollie/api-client/dist/types/src/binders/orders/parameters";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import {
  Ctx,
  EntityHydrator,
  Logger,
  OrderService,
  PaymentMethodService,
  RequestContext,
  UserInputError,
  totalCoveredByPayments,
} from "@vendure/core";
import {
  getLocale,
  toAmount,
  toMollieAddress,
  toMollieOrderLines,
} from "@vendure/payments-plugin/package/mollie/mollie.helpers";

interface MollieIntentForOrderInput {
  orderCode: string;
  paymentMethodCode: string;
  redirectUrl: string;
}

const loggerCtx = "DraftOrderPlugin";

// FIXME: This should use the mollieService.createPaymentIntent, but this is only supported after V2.2
@Resolver()
export class DraftOrderResolver {
  constructor(
    // private readonly mollieService: MollieService,
    private readonly orderService: OrderService,
    private readonly paymentMethodService: PaymentMethodService,
    private readonly entityHydrator: EntityHydrator,
  ) {}

  @Mutation()
  async createMolliePaymentIntentForOrder(
    @Ctx() ctx: RequestContext,
    @Args("input")
    { orderCode, paymentMethodCode, redirectUrl }: MollieIntentForOrderInput,
  ): Promise<string> {
    const order = await this.orderService.findOneByCode(ctx, orderCode);
    if (!order) {
      throw new UserInputError(`Order with code ${orderCode} not found`);
    }
    const paymentMethods = await this.paymentMethodService.findAll(ctx);
    const paymentMethod = paymentMethods.items.find(
      (pm) => pm.code === paymentMethodCode,
    );
    if (!paymentMethod) {
      throw new UserInputError(
        `Payment method with code ${paymentMethodCode} not found`,
      );
    }
    const apiKey = paymentMethod.handler.args.find(
      (arg) => arg.name === "apiKey",
    )?.value;
    if (!apiKey) {
      throw new Error(
        `Paymentmethod ${paymentMethod.code} has no apiKey configured`,
      );
    }
    await this.entityHydrator.hydrate(ctx, order, {
      relations: [
        "customer",
        "surcharges",
        "lines.productVariant",
        "shippingLines.shippingMethod",
        "payments",
      ],
    });
    if (!order.customer) {
      throw new UserInputError(`Order with code ${orderCode} has no customer`);
    }
    const mollieClient = createMollieClient({ apiKey });
    const alreadyPaid = totalCoveredByPayments(order);
    const amountToPay = order.totalWithTax - alreadyPaid;
    const billingAddress =
      toMollieAddress(order.billingAddress, order.customer) ??
      toMollieAddress(order.shippingAddress, order.customer);
    if (!billingAddress) {
      throw new UserInputError("Order has no billing or shipping address");
    }
    const orderInput: CreateParameters = {
      orderNumber: order.code,
      amount: toAmount(amountToPay, order.currencyCode),
      redirectUrl,
      webhookUrl: `${process.env.VENDURE_HOST}/payments/mollie/${ctx.channel.token}/${paymentMethod.id}`,
      billingAddress,
      locale: getLocale(billingAddress.country, ctx.languageCode),
      lines: toMollieOrderLines(order, alreadyPaid),
      metadata: {
        languageCode: ctx.languageCode,
      },
    };
    const mollieOrder = await mollieClient.orders.create(orderInput);
    Logger.info(
      `Created Mollie order ${mollieOrder.id} for order ${order.code}`,
      loggerCtx,
    );
    const url = mollieOrder.getCheckoutUrl();
    if (!url) {
      throw Error("Unable to getCheckoutUrl() from Mollie order");
    }
    return url;
  }
}
