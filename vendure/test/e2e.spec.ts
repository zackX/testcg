import { ModuleRef } from "@nestjs/core";
import {
  ChannelService,
  DefaultLogger,
  defaultOrderProcess,
  DefaultSearchPlugin,
  defaultShippingCalculator,
  ID,
  Injector,
  LogLevel,
  mergeConfig,
  Order,
  OrderProcess,
  OrderState,
  ProductService,
  RequestContext,
} from "@vendure/core";
import {
  createTestEnvironment,
  registerInitializer,
  SimpleGraphQLClient,
  SqljsInitializer,
  testConfig,
  TestServer,
} from "@vendure/testing";
import dotenv from "dotenv";
import { afterAll, beforeAll, describe, expect, it } from "vitest";
import { ColorGraphicsOptionSyncStrategy } from "../src/config/color-graphics-option-sync";
import { colourGraphicsCalculation } from "../src/configurator/api/cg-calculation";
import { printingOrderProcess } from "../src/config/printing-order-process";
import { ConfiguratorPlugin } from "../src/configurator/configurator-plugin";
import { CGOrderCodeStrategy } from "../src/config/cg-order-code.strategy";
import { initialData, testPaymentMethod } from "./initial-data";
import { CustomStateTransitionPlugin } from "../src/custom-state-transition/custom-state-transition.plugin";
import {
  createHeightOption,
  createWidthOption,
  materialThickness10mm,
  materialThickness3mm,
  mockOptionGroups,
  mockSheetData,
} from "./mock-data";
import {
  ActiveCustomer,
  AddCustomersToGroup,
  AddItemToDraftOrder,
  AddItemToOrder,
  AddPaymentToOrder,
  CalculateConfiguratorPrice,
  CREATE_DRAFT_ORDER,
  CreateGroup,
  GET_ORDER_BY_ID,
  GetProduct,
  RemoveAllOrderLines,
  SetCustomerForOrder,
  SetOrderShippingMethod,
  SetShippingAddress,
  TRANSITION_ORDER_TO_AWAITING_PREPRESS,
  transitionOrderToState,
  TransitionToState,
  UpdateProduct,
} from "./queries";
import dayjs from "dayjs";
import { ShippingPlugin } from "../src/shipping/shipping.plugin";
import { gql } from "graphql-tag";
import { customFields } from "../src/config/custom-fields";
import {
  createShippingMethodWithWeightChecker,
  createTurnaroundShippingMethod,
  TurnaroundShippingCalculatorArgValues,
  WeightBasedShippingCheckerArgValues,
} from "./helpers";
import {
  ShippingExtensionsPlugin,
  UKPostalCodeToGelocationConversionStrategy,
} from "@pinelab/vendure-plugin-shipping-extensions";
import { getConfigurableOrderTotalWeightInGrams } from "../src/config/configurable-item-weight-calculator";
import { turnaroundShippingMethodCodeFragment } from "../src/shipping/util/turnaround";

/**
 * This should mimic the live env as closely as possible
 */
dotenv.config();
//let's configure the orderPlacedAt field to be 28 March(thursday), 2024 past midday
export const customOrderPlacedAt = dayjs("2024-03-28T15:30:00").toDate();

export const setCustomOrderPlacedAtOrderProcess: OrderProcess<OrderState> = {
  onTransitionEnd: async (_, toState, data) => {
    const { order } = data;
    if (toState === "PaymentSettled") {
      order.orderPlacedAt = customOrderPlacedAt;
    }
  },
};

const config = mergeConfig(testConfig, {
  logger: new DefaultLogger({ level: LogLevel.Debug }),
  orderOptions: {
    process: [
      defaultOrderProcess,
      printingOrderProcess,
      setCustomOrderPlacedAtOrderProcess,
    ] as Array<OrderProcess<any>>,
    orderCodeStrategy: new CGOrderCodeStrategy(),
  },
  paymentOptions: {
    paymentMethodHandlers: [testPaymentMethod],
  },
  apiOptions: {
    port: 3050,
  },
  customFields,
  authOptions: testConfig.authOptions,
  dbConnectionOptions: {
    ...testConfig.dbConnectionOptions,
    synchronize: true,
  },
  shippingOptions: {
    shippingCalculators: [defaultShippingCalculator],
  },
  plugins: [
    ConfiguratorPlugin.init({
      calculation: colourGraphicsCalculation,
    }),
    DefaultSearchPlugin,
    CustomStateTransitionPlugin,
    ShippingPlugin,
    ShippingExtensionsPlugin.init({
      customFieldsTab: "Physical properties",
      orderAddressToGeolocationStrategy:
        new UKPostalCodeToGelocationConversionStrategy(),
      weightCalculationFunction: getConfigurableOrderTotalWeightInGrams,
    }),
  ],
});

describe("Vendure", function () {
  let server: TestServer;
  let adminClient: SimpleGraphQLClient;
  let shopClient: SimpleGraphQLClient;
  let serverStarted = false;
  let threeDayTurnarounShippingMethod;
  let threeDayTurnarounShippingMethodTurnaroundValue: number = 3;
  let threeDayTurnaroundShippingMethodCode;
  let threeDayTurnaroundShippingArgValues: TurnaroundShippingCalculatorArgValues =
    {
      taxRate: 0,
      percentageOfCart: 10,
      minimumPrice: 123,
    };
  let product5WeightInGrams = 12345;
  let upto15KgShippingMethod;
  let weightBasedCheckerArgs: WeightBasedShippingCheckerArgValues = {
    minWeight: 0,
    maxWeight: 15,
    countries: ["NL"],
    excludeCountries: ["GBR"],
  };

  beforeAll(async () => {
    registerInitializer("sqljs", new SqljsInitializer("__data__"));
    ({ server, adminClient, shopClient } = createTestEnvironment(config));
    await server.init({
      initialData: {
        ...initialData,
        paymentMethods: [
          {
            name: testPaymentMethod.code,
            handler: { code: testPaymentMethod.code, arguments: [] },
          },
        ],
      },
      productsCsvPath: "./test/products.csv",
    });
    serverStarted = true;
    await adminClient.asSuperAdmin();
    threeDayTurnaroundShippingMethodCode = `${turnaroundShippingMethodCodeFragment}-${threeDayTurnarounShippingMethodTurnaroundValue}`;
    threeDayTurnarounShippingMethod = await createTurnaroundShippingMethod(
      adminClient,
      threeDayTurnaroundShippingArgValues,
      threeDayTurnaroundShippingMethodCode,
      "3 Day Turnaround Shipping Method",
    );
    upto15KgShippingMethod = await createShippingMethodWithWeightChecker(
      adminClient,
      weightBasedCheckerArgs,
      "upto-15-KG",
      "Up to 15KG",
    );
  }, 60000);

  // -------------- Helpers --------------

  /**
   * Add default shipping address
   */
  async function setShippingAddress(): Promise<Order> {
    const { setOrderShippingAddress } = await shopClient.query(
      SetShippingAddress,
      {
        input: {
          fullName: "Martinho Pinelabio",
          streetLine1: "Verzetsstraat",
          streetLine2: "12a",
          city: "Liwwa",
          postalCode: "8923CP",
          countryCode: "NL",
        },
      },
    );
    return setOrderShippingAddress;
  }
  /**
   * Add default shipping address
   */
  async function setCustomer(): Promise<Order> {
    const { setCustomerForOrder } = await shopClient.query(
      SetCustomerForOrder,
      {
        input: {
          firstName: "Martinho",
          lastName: "Pinelabio",
          emailAddress: "test@pinelab.studio",
        },
      },
    );
    return setCustomerForOrder;
  }

  /**
   * Add shipping method
   */
  async function setShippingMethod(id: ID): Promise<Order> {
    const { setOrderShippingMethod } = await shopClient.query(
      SetOrderShippingMethod,
      { id: [id] },
    );
    return setOrderShippingMethod;
  }

  /**
   * Transition to ArrangingPayment
   */
  async function transitionToArrangingPayment(): Promise<Order> {
    const { transitionOrderToState } = await shopClient.query(
      TransitionToState,
      { state: "ArrangingPayment" },
    );
    return transitionOrderToState;
  }

  /**
   * Add payment by code to active order
   */
  async function addPaymentToOrder(): Promise<Order> {
    const { addPaymentToOrder } = await shopClient.query(AddPaymentToOrder, {
      input: {
        method: testPaymentMethod.code,
        metadata: {
          transactionId: "test",
        },
      },
    });
    return addPaymentToOrder;
  }

  // -------------- Actual testcases --------------

  let placedOrder: Order | undefined;

  it("Should start successfully", async () => {
    expect(serverStarted).toBe(true);
  });

  describe("Anonymous order placement", () => {
    it("Should log out", async () => {
      await shopClient.asAnonymousUser();
    });

    it("Should add item to order", async () => {
      const { addItemToOrder: order } = await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
      });
      const date = dayjs(new Date()).format("DDMMYY");
      expect(order.code).toMatch(new RegExp(`^CG-${date}-\\w{5}\$`));
      expect(order.lines[0].productVariant.id).toBe("T_1");
      expect(order.shippingWithTax).toBe(0);
    });

    it("Should add shipping address to order", async () => {
      const order = await setShippingAddress();
      expect(order.shippingAddress.fullName).toBe("Martinho Pinelabio");
      expect(order.shippingAddress.streetLine1).toBe("Verzetsstraat");
      expect(order.shippingAddress.streetLine2).toBe("12a");
      expect(order.shippingAddress.city).toBe("Liwwa");
      expect(order.shippingAddress.postalCode).toBe("8923CP");
      expect(order.shippingAddress.country).toBe("Nederland");
      expect(order.lines[0].productVariant.id).toBe("T_1");
    });

    it("Should add a customer to order", async () => {
      const order = await setCustomer();
      expect(order.customer?.firstName).toBe("Martinho");
      expect(order.customer?.lastName).toBe("Pinelabio");
      expect(order.customer?.emailAddress).toBe("test@pinelab.studio");
    });

    it("Should set shipping method for order", async () => {
      const order = await setShippingMethod(threeDayTurnarounShippingMethod.id);
      expect(order.shipping).toBe(
        order.subTotal *
          (threeDayTurnaroundShippingArgValues.percentageOfCart / 100),
      );
    });

    it("Should place order by adding payment and", async () => {
      placedOrder = await transitionToArrangingPayment();
      expect(placedOrder.state).toBe("ArrangingPayment");
      placedOrder = await addPaymentToOrder();
      expect(placedOrder.state).toBe("PaymentSettled");
      expect(placedOrder.orderPlacedAt).toBeDefined();
    });
  });

  describe("Authenticated order placement", () => {
    let order: Order;

    it("Should log in", async () => {
      await shopClient.asUserWithCredentials(
        "hayden.zieme12@hotmail.com",
        "test",
      );
    });

    it("Should add item to order", async () => {
      ({ addItemToOrder: order } = await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
      }));
      expect(order.lines[0].productVariant.id).toBe("T_1");
      expect(order.shippingWithTax).toBe(0);
    });

    it("Should have logged in customer details", async () => {
      expect(order.customer?.firstName).toBe("Hayden");
      expect(order.customer?.lastName).toBe("Zieme");
      expect(order.customer?.emailAddress).toBe("hayden.zieme12@hotmail.com");
    });

    it("Should add shipping address to order", async () => {
      const order = await setShippingAddress();
      expect(order.shippingAddress.streetLine1).toBe("Verzetsstraat");
      expect(order.shippingAddress.streetLine2).toBe("12a");
      expect(order.shippingAddress.city).toBe("Liwwa");
      expect(order.shippingAddress.postalCode).toBe("8923CP");
      expect(order.shippingAddress.country).toBe("Nederland");
      expect(order.lines[0].productVariant.id).toBe("T_1");
    });

    it("Should set shipping method for order", async () => {
      const order = await setShippingMethod(threeDayTurnarounShippingMethod.id);
      expect(order.shipping).toBe(
        order.subTotal *
          (threeDayTurnaroundShippingArgValues.percentageOfCart / 100),
      );
    });

    it("Should place order by adding payment", async () => {
      let order = await transitionToArrangingPayment();
      expect(order.state).toBe("ArrangingPayment");
      order = await addPaymentToOrder();
      expect(order.state).toBe("PaymentSettled");
      expect(order.orderPlacedAt).toBeDefined();
    });
  });

  describe("Trade Customers", () => {
    it("Should create Trade Group and add test user Hayden", async () => {
      await adminClient.asSuperAdmin();
      const { createCustomerGroup: group } = await adminClient.query(
        CreateGroup,
        { input: { name: "Trade" } },
      );
      const { addCustomersToGroup } = await adminClient.query(
        AddCustomersToGroup,
        { groupId: group.id, customerIds: ["T_1"] },
      );
      const groupMember = addCustomersToGroup.customers.items[0];
      expect(groupMember.emailAddress).toBe("hayden.zieme12@hotmail.com");
    });

    it("Should see test user Hayden as Trade Customer via Shop API", async () => {
      await shopClient.asUserWithCredentials(
        "hayden.zieme12@hotmail.com",
        "test",
      );
      const { activeCustomer } = await shopClient.query(ActiveCustomer);
      expect(activeCustomer.isTradeCustomer).toBe(true);
    });
  });

  describe("Configurator", () => {
    it("Prepares configurator options on product 1", async () => {
      const { updateProduct } = await adminClient.query(UpdateProduct, {
        input: {
          id: 1,
          customFields: {
            configuratorOptionGroups: JSON.stringify(mockOptionGroups),
          },
        },
      });
      expect(
        updateProduct.configuratorOptionGroups[0]?.options[0].customFields
          ?.retailIncrease,
      ).toBe(10);
    });

    it("Should have default retail and trade configurator price", async () => {
      const { product } = await shopClient.query(GetProduct, {
        id: 1,
      });
      const perimeter = 2 * ((1000 + 1000) / 1000);
      const { retail, trade } = product.variants[0].defaultConfiguratorPrice;
      const expectedTradePrice = 3840 * perimeter;
      expect(retail).toBe(expectedTradePrice * 1.1); // Retail is 10% higher in tests
      expect(trade).toBe(expectedTradePrice);
    });

    it("Should have human readable options on an order line", async () => {
      await shopClient.asAnonymousUser(); // Reset order
      const { addItemToOrder: order } = await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
        customFields: {
          selectedConfiguratorOptions: [
            materialThickness3mm,
            createWidthOption(1234),
            createHeightOption(4567),
          ],
        },
      });
      const readableOptions = order.lines[0].selectedConfiguratorOptions;
      // Validate Material option
      const materialOption = readableOptions.find(
        (r) => r.optionGroupName === "Material Thickness",
      );
      expect(materialOption?.optionValue).toBe(
        "3mm Foamex + HP mounted Latex Print",
      );
      // Validate Width option
      const widthOption = readableOptions.find(
        (r) => r.optionGroupName === "Width",
      );
      expect(widthOption?.optionValue).toBe("1234");
      // Validate Height option
      const HeightOption = readableOptions.find(
        (r) => r.optionGroupName === "Height",
      );
      expect(HeightOption?.optionValue).toBe("4567");
    });

    it("Should have default human readable options, when no options where selected", async () => {
      await shopClient.asAnonymousUser(); // Reset order
      const { addItemToOrder: order } = await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
        customFields: {
          selectedConfiguratorOptions: [],
        },
      });
      const readableOptions = order.lines[0].selectedConfiguratorOptions;
      // Validate Material option
      const materialOption = readableOptions.find(
        (r) => r.optionGroupName === "Material Thickness",
      );
      expect(materialOption?.optionValue).toBe(
        "3mm Foamex + HP mounted Latex Print",
      );
      // Validate Width option
      const widthOption = readableOptions.find(
        (r) => r.optionGroupName === "Width",
      );
      expect(widthOption?.optionValue).toBe("1000");
      // Validate Height option
      const HeightOption = readableOptions.find(
        (r) => r.optionGroupName === "Height",
      );
      expect(HeightOption?.optionValue).toBe("1000");
    });

    describe("Trade customer", () => {
      it("Should use default values when no options selected as Trade customer", async () => {
        // Login as trade customer
        await shopClient.asUserWithCredentials(
          "hayden.zieme12@hotmail.com",
          "test",
        );
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [],
            },
          },
        );
        const perimeter = (2 * (1000 + 1000)) / 1000;
        expect(order.total).toBe(3840 * perimeter);
      });

      it("Should use default values when no options selected as Trade customer", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [],
            },
          },
        );
        const perimeter = (2 * (1000 + 1000)) / 1000;
        expect(order.total).toBe(3840 * perimeter);
      });

      it("Should calculate price for 2 sqm and 3mm as Trade customer", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [
                materialThickness3mm,
                createWidthOption(1000),
                createHeightOption(2000),
              ],
            },
          },
        );
        const perimeter = 2 * ((1000 + 2000) / 1000);
        expect(order.total).toBe(3840 * perimeter);
      });

      it("Should calculate price for 10 sqm and 3mm as Trade customer", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [
                materialThickness3mm,
                createWidthOption(10000),
                createHeightOption(1000),
              ],
            },
          },
        );
        // Price for > 10 sqm is lower (3600)
        const perimeter = 2 * ((10000 + 1000) / 1000);
        expect(order.total).toBe(3600 * perimeter);
      });

      it("Should calculate price for 1 sqm and 10mm as Trade customer", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [
                materialThickness10mm,
                createWidthOption(1000),
                createHeightOption(1000),
              ],
            },
          },
        );
        expect(order.total).toBe(8400);
      });

      it("Should preview price as Trade customer", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { calculateConfiguratorPrice } = await shopClient.query(
          CalculateConfiguratorPrice,
          {
            variantId: 1,
            quantity: 1,
            selectedOptions: [
              { optionGroupId: "material-thickness", optionId: "foamex-10mm" },
              { optionGroupId: "height", value: 1000 },
              { optionGroupId: "width", value: 1000 },
            ],
          },
        );
        expect(calculateConfiguratorPrice.trade).toBe(8400);
      });

      it("Should use Min Order Price", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { calculateConfiguratorPrice } = await shopClient.query(
          CalculateConfiguratorPrice,
          {
            variantId: 1,
            quantity: 1,
            selectedOptions: [
              { optionGroupId: "material-thickness", optionId: "foamex-3mm" },
              { optionGroupId: "height", value: 1 },
              { optionGroupId: "width", value: 1 },
            ],
          },
        );
        expect(calculateConfiguratorPrice.trade).toBe(5000);
      });

      it("Should use Min Order Price with multiple quantities", async () => {
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { calculateConfiguratorPrice } = await shopClient.query(
          CalculateConfiguratorPrice,
          {
            variantId: 1,
            quantity: 3,
            selectedOptions: [
              { optionGroupId: "material-thickness", optionId: "foamex-3mm" },
              { optionGroupId: "height", value: 1 },
              { optionGroupId: "width", value: 1 },
            ],
          },
        );
        // When 3 items are ordered, the price each is the min-order-price divided by 3
        expect(calculateConfiguratorPrice.trade).toBe(1667);
      });

      it("Should use Min Unit Price when enough quantity ", async () => {
        // When 6 units are used, the total is greater than the Min Order Price
        // Thus it should fall back to the Min Unit Price of 1000 per unit
        await shopClient.query(RemoveAllOrderLines); // Reset order
        const { calculateConfiguratorPrice } = await shopClient.query(
          CalculateConfiguratorPrice,
          {
            variantId: 1,
            quantity: 6,
            selectedOptions: [
              { optionGroupId: "material-thickness", optionId: "foamex-3mm" },
              { optionGroupId: "height", value: 1 },
              { optionGroupId: "width", value: 1 },
            ],
          },
        );
        // Keep in mind that this always returns the price per unit
        expect(calculateConfiguratorPrice.trade).toBe(1000);
      });
    });

    describe("Retail customer", () => {
      it("Should use default values when no options selected as Retail customer", async () => {
        await shopClient.asAnonymousUser(); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [],
            },
          },
        );
        const perimeter = (2 * (1000 + 1000)) / 1000;
        expect(order.total).toBe(3840 * perimeter * 1.1); // Retail is 10% higher
      });

      it("Should calculate price for 2 sqm and 3mm as Retail customer", async () => {
        await shopClient.asAnonymousUser(); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [
                materialThickness3mm,
                createWidthOption(1000),
                createHeightOption(2000),
              ],
            },
          },
        );
        const perimeter = 2 * ((1000 + 2000) / 1000);
        expect(order.total).toBe(Math.round(3840 * perimeter * 1.1));
      });

      it("Should calculate price for 10 sqm and 3mm as Retail customer", async () => {
        await shopClient.asAnonymousUser(); // Reset order
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              selectedConfiguratorOptions: [
                materialThickness3mm,
                createWidthOption(10000),
                createHeightOption(1000),
              ],
            },
          },
        );
        // Price for > 10 sqm is lower (3600)
        const perimeter = 2 * ((10000 + 1000) / 1000);
        expect(order.total).toBe(3600 * perimeter * 1.1);
      });

      it("Should use OrderLine custom unit price in admin api", async () => {
        await shopClient.asSuperAdmin(); // Reset order
        const { createDraftOrder } =
          await adminClient.query(CREATE_DRAFT_ORDER);
        const customUnitPrice = 166;
        const { addItemToDraftOrder: order } = await adminClient.query(
          AddItemToDraftOrder,
          {
            orderId: createDraftOrder.id,
            input: {
              productVariantId: 1,
              quantity: 1,
              customFields: {
                customUnitPriceExVAT: customUnitPrice,
              },
            },
          },
        );
        expect(order.total).toBe(customUnitPrice);
      });

      it("Should not use OrderLine custom unit price in shop api", async () => {
        await shopClient.asAnonymousUser(); // Reset order
        const customUnitPrice = 166;
        const { addItemToOrder: order } = await shopClient.query(
          AddItemToOrder,
          {
            productVariantId: 1,
            quantity: 1,
            customFields: {
              customUnitPriceExVAT: customUnitPrice,
            },
          },
        );
        expect(order.total).not.toBe(customUnitPrice);
      });

      it("Should preview price as Retail customer", async () => {
        await shopClient.asAnonymousUser(); // Reset order
        const { calculateConfiguratorPrice } = await shopClient.query(
          CalculateConfiguratorPrice,
          {
            variantId: 1,
            quantity: 1,
            selectedOptions: [
              { optionGroupId: "material-thickness", optionId: "foamex-10mm" },
              { optionGroupId: "height", value: 1000 },
              { optionGroupId: "width", value: 1000 },
            ],
          },
        );
        expect(calculateConfiguratorPrice.retail).toBe(8400 * 1.1);
      });
    });
  });

  describe("Custom order states", () => {
    it("Should transition automatically from PaymentSettled to AwaitingArtwork", async () => {
      await new Promise((resolve) => setTimeout(resolve, 1000));
      const { order } = await adminClient.query(GET_ORDER_BY_ID, {
        id: placedOrder?.id,
      });
      expect(order.state).toBe("AwaitingArtwork");
    });

    it("Should transition from AwaitingArtwork to AwaitingPrepress", async () => {
      const { transitionOrderToState: order } = await adminClient.query(
        transitionOrderToState,
        {
          id: placedOrder?.id,
          state: "AwaitingPrepress",
        },
      );
      expect(order?.state).toBe("AwaitingPrepress");
    });

    it("Should transition from AwaitingPrepress to AttentionRequired", async () => {
      const { transitionOrderToState: order } = await adminClient.query(
        transitionOrderToState,
        {
          id: placedOrder?.id,
          state: "AttentionRequired",
        },
      );
      expect(order?.state).toBe("AttentionRequired");
    });

    it("Should transition from AttentionRequired to AwaitingPrepress", async () => {
      const { transitionOrderToState: order } = await adminClient.query(
        transitionOrderToState,
        {
          id: placedOrder?.id,
          state: "AwaitingPrepress",
        },
      );
      expect(order?.state).toBe("AwaitingPrepress");
    });

    it("Should transition from AwaitingPrepress to InProduction", async () => {
      const { transitionOrderToState: order } = await adminClient.query(
        transitionOrderToState,
        {
          id: placedOrder?.id,
          state: "InProduction",
        },
      );
      expect(order?.state).toBe("InProduction");
    });

    it("Should transition from InProduction to AwaitingCollection", async () => {
      const { transitionOrderToState: order } = await adminClient.query(
        transitionOrderToState,
        {
          id: placedOrder?.id,
          state: "AwaitingCollection",
        },
      );
      expect(order?.state).toBe("AwaitingCollection");
    });

    it("Should be able to manually transition an order to AwaittingPrepress after uploading artwork", async () => {
      const customerEmailAddress = "hayden.zieme12@hotmail.com";
      const customerPassword = "test";
      await shopClient.asUserWithCredentials(
        customerEmailAddress,
        customerPassword,
      );
      const { addItemToOrder: newOrder } = await shopClient.query(
        AddItemToOrder,
        {
          productVariantId: 1,
          quantity: 1,
        },
      );
      await setShippingAddress();
      await setShippingMethod(threeDayTurnarounShippingMethod.id);
      await transitionToArrangingPayment();
      await addPaymentToOrder();
      await new Promise((resolve) => setTimeout(resolve, 1000));
      const { transitionOrderToAwaitingPrepress: updatedOrder } =
        await shopClient.query(TRANSITION_ORDER_TO_AWAITING_PREPRESS, {
          orderCode: newOrder?.code,
          emailAddress: customerEmailAddress,
        });
      expect(updatedOrder?.state).toBe("AwaitingPrepress");
    });

    it("Should fail to manually transition an order to AwaittingPrepress for user other than the owner", async () => {
      const ownerEmailAddress = "hayden.zieme12@hotmail.com";
      const nonOwnerEmailAddress = "michael.zieme12@hotmail.com";
      const customerPassword = "test";
      await shopClient.asUserWithCredentials(
        ownerEmailAddress,
        customerPassword,
      );
      const { addItemToOrder: newOrder } = await shopClient.query(
        AddItemToOrder,
        {
          productVariantId: 1,
          quantity: 1,
        },
      );
      await setShippingAddress();
      await setShippingMethod("T_1");
      await transitionToArrangingPayment();
      await addPaymentToOrder();
      await new Promise((resolve) => setTimeout(resolve, 1000));
      try {
        await shopClient.query(TRANSITION_ORDER_TO_AWAITING_PREPRESS, {
          orderCode: newOrder?.code,
          emailAddress: nonOwnerEmailAddress,
        });
      } catch (e) {
        expect(e.message).toBe(
          "You are not currently authorized to perform this action",
        );
      }
    });
  });

  describe("Pulling Data from Google Sheet", () => {
    it("Should update multiple Configurable products from Google Sheet", async () => {
      const ctx = await getDefaultCtx(server);
      const strategy = new ColorGraphicsOptionSyncStrategy();
      const moduleRef = server.app.get(ModuleRef);
      await strategy.handleSheetData(
        ctx,
        new Injector(moduleRef),
        mockSheetData,
      );
      const productService = server.app.get(ProductService);
      const foamexPrintMount = await productService.findOneBySlug(
        ctx,
        "foamex-print-mount",
      );
      const anotherPrintMount = await productService.findOneBySlug(
        ctx,
        "another-print-mount",
      );
      const simplePrintMount = await productService.findOneBySlug(
        ctx,
        "simple-print-mount",
      );
      const complexPrintMount = await productService.findOneBySlug(
        ctx,
        "complex-print-mount",
      );
      const justPrintMount = await productService.findOneBySlug(
        ctx,
        "just-print-mount",
      );
      const foamexPrintMountConfiguratorOptionGroups = JSON.parse(
        (foamexPrintMount?.customFields as any).configuratorOptionGroups,
      );
      expect(foamexPrintMountConfiguratorOptionGroups.length).toBe(5);
      expect(foamexPrintMountConfiguratorOptionGroups[0].options[1].name).toBe(
        "Gloss Laminate",
      );
      expect(
        foamexPrintMountConfiguratorOptionGroups[1].options[2].customFields
          .prices[2].price,
      ).toBe(120896); // prices are transformed to cents
      expect(foamexPrintMountConfiguratorOptionGroups[2].max).toBe(987679878);
      expect(foamexPrintMountConfiguratorOptionGroups[3].min).toBe(1000);
      expect(
        foamexPrintMountConfiguratorOptionGroups[4].options[2].customFields
          .weightGramsPerSquareMeter,
      ).toBeUndefined();
      const anotherPrintMountConfiguratorOptionGroups = JSON.parse(
        (anotherPrintMount?.customFields as any).configuratorOptionGroups,
      );
      expect(anotherPrintMountConfiguratorOptionGroups.length).toBe(1);
      expect(anotherPrintMountConfiguratorOptionGroups[0].options[0].name).toBe(
        "Matt Selaminate",
      );
      expect(anotherPrintMountConfiguratorOptionGroups[0].options[2].id).toBe(
        "manual-selam",
      );
      const simplePrintMountConfiguratorOptionGroups = JSON.parse(
        (simplePrintMount?.customFields as any).configuratorOptionGroups,
      );
      expect(simplePrintMountConfiguratorOptionGroups.length).toBe(2);
      expect(simplePrintMountConfiguratorOptionGroups[0].id).toBe("simpler");
      expect(simplePrintMountConfiguratorOptionGroups[1].min).toBe(1000);
      const complexPrintMountConfiguratorOptionGroups = JSON.parse(
        (complexPrintMount?.customFields as any).configuratorOptionGroups,
      );
      expect(complexPrintMountConfiguratorOptionGroups.length).toBe(2);
      expect(complexPrintMountConfiguratorOptionGroups[0].options[1].id).toBe(
        "manual-simpler-woman",
      );
      expect(complexPrintMountConfiguratorOptionGroups[1].name).toBe(
        "Weight Man",
      );
    });
  });

  describe("Shipping and Turnaround", () => {
    it("Prepares weight on product 5", async () => {
      const { updateProduct } = await adminClient.query(UpdateProduct, {
        input: {
          id: 5,
          customFields: {
            weight: product5WeightInGrams,
          },
        },
      });
      expect(updateProduct.customFields?.weight).toBe(product5WeightInGrams);
    });
    it("Should preview turnaround prices", async () => {
      //reset session
      await shopClient.asAnonymousUser();
      //create a settled order
      await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
      });
      const order = await setCustomer();
      //check if
      const { eligibleShippingMethods } = await shopClient.query(
        GET_ELIGIBLE_SHIPPING_METHODS,
        {
          shippingMethodId: threeDayTurnarounShippingMethod.id,
        },
      );
      const turnaroundMethod = eligibleShippingMethods.find(
        (sm) => sm.code === threeDayTurnaroundShippingMethodCode,
      );
      const expectedTurnaroundPrice = Math.round(
        order.subTotal *
          (threeDayTurnaroundShippingArgValues.percentageOfCart / 100),
      );
      expect(turnaroundMethod.price).toBe(expectedTurnaroundPrice);
      expect(turnaroundMethod.estimatedDeliveryDate).toBeDefined();
    });

    it("Should correctly populate shipping price", async () => {
      await setShippingMethod(threeDayTurnarounShippingMethod.id);
      const order = await transitionToArrangingPayment();
      const expectedTurnaroundPrice = Math.round(
        order.subTotal *
          (threeDayTurnaroundShippingArgValues.percentageOfCart / 100),
      );
      expect(order.shipping).toBe(expectedTurnaroundPrice);
    });

    it("Should fill shippingDate and estimatedDeliveryDate", async () => {
      const settledOrder = await addPaymentToOrder();
      // Await async
      const { order } = await adminClient.query(GET_ORDER_BY_ID, {
        id: settledOrder.id,
      });
      expect(order.orderPlacedAt).toBe(customOrderPlacedAt.toISOString());
      const expectedShippingDate = dayjs(customOrderPlacedAt)
        .add(1, "day") //since customOrderPlacedAt is past midday, it doesn't count as turnaround day
        .add(1, "day") //since customOrderPlacedAt+1 is UK Bank Holiday(29 March), it doesn't count as turnaround day
        .add(1, "day") //since customOrderPlacedAt+2 is weekend(Saturday), it doesn't count as turnaround day
        .add(1, "day") //since customOrderPlacedAt+3 is also weekend(Sunday), it doesn't count as turnaround day
        .add(1, "day") //for turnaround day 1
        .add(1, "day") //for turnaround day 2
        .add(1, "day"); //for turnaround day 3
      expect(order.customFields?.shippingDate).toBe(
        expectedShippingDate.toISOString(),
      );
      const expectedDeliveryDate = expectedShippingDate.add(1, "day");
      expect(order.estimatedDeliveryDate).toBe(
        expectedDeliveryDate.toISOString(),
      );
    });

    it("Should set shipping method when the total order weight falls with in the allowed range of weightAndCountryChecker", async () => {
      const widthInmm = 123;
      const heightInmm = 456;
      await shopClient.asAnonymousUser();
      await shopClient.query(AddItemToOrder, {
        productVariantId: 1,
        quantity: 1,
        customFields: {
          selectedConfiguratorOptions: [
            materialThickness3mm,
            createWidthOption(widthInmm),
            createHeightOption(heightInmm),
          ],
        },
      });
      await setShippingAddress();
      await setCustomer();
      const order = await setShippingMethod(upto15KgShippingMethod.id);
      const selectedOptionGroup = JSON.parse(materialThickness3mm);
      const optionGroup: any = mockOptionGroups.find(
        (og: any) => og.id === selectedOptionGroup.optionGroupId,
      );
      const option = optionGroup.options.find(
        (op) => op.id === selectedOptionGroup.optionId,
      );
      let weight =
        option.customFields.weightGramsPerSquareMeter *
        (widthInmm / 1000) *
        (heightInmm / 1000);
      weight = weight / 1000;
      expect(weight).toBeLessThan(weightBasedCheckerArgs.maxWeight);
      expect(order.shipping).toBeDefined();
    });

    it("Shouldn't set shipping method when the total order weight falls outside of the allowed range of weightAndCountryChecker", async () => {
      const quantity = 10;
      await shopClient.asAnonymousUser();
      await shopClient.query(AddItemToOrder, {
        productVariantId: 5,
        quantity,
      });
      await setShippingAddress();
      await setCustomer();
      const order = await setShippingMethod(upto15KgShippingMethod.id);
      const totalOrderWeightInKg = (product5WeightInGrams * quantity) / 1000;
      expect(totalOrderWeightInKg).toBeGreaterThan(
        weightBasedCheckerArgs.maxWeight,
      );
      expect(order.shipping).not.toBeDefined();
    });
  });

  async function getDefaultCtx(server: TestServer) {
    const channel = await server.app.get(ChannelService).getDefaultChannel();
    return new RequestContext({
      apiType: "admin",
      isAuthorized: true,
      authorizedAsOwnerOnly: false,
      channel,
    });
  }

  const GET_ELIGIBLE_SHIPPING_METHODS = gql`
    query eligibleShippingMethods {
      eligibleShippingMethods {
        id
        price
        code
        estimatedDeliveryDate
      }
    }
  `;

  afterAll(() => {
    return server.destroy();
  });
});
