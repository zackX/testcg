import { Injectable } from "@nestjs/common";
import {
  CustomerService,
  EntityHydrator,
  RequestContext,
  Customer,
  Order,
} from "@vendure/core";

@Injectable()
export class TradeCustomerService {
  constructor(
    private readonly customerService: CustomerService,
    private readonly entityHydrator: EntityHydrator,
  ) {}

  /**
   * Defines wether the callee or the order's customer is a trade customer.
   * If an order is given, we check
   */
  async isTradeCustomer(ctx: RequestContext, order?: Order): Promise<boolean> {
    if (!ctx.activeUserId) {
      // Only logged in customers can be trade customers. Only logged in customers or admins are allowed to act as Trade customers
      return false;
    }
    if (order) {
      // If an order is given, we check based on the order's customer
      await this.entityHydrator.hydrate(ctx, order, {
        relations: ["customer"],
      });
      return await this.isCustomerInTradeGroup(ctx, order.customer);
    }
    // If no order is given, we check based on the active user
    const customer = await this.customerService.findOneByUserId(
      ctx,
      ctx.activeUserId,
    );
    return await this.isCustomerInTradeGroup(ctx, customer);
  }

  private async isCustomerInTradeGroup(
    ctx: RequestContext,
    customer?: Customer,
  ): Promise<boolean> {
    if (!customer) {
      return false;
    }
    await this.entityHydrator.hydrate(ctx, customer, { relations: ["groups"] });
    return customer.groups.some((group) =>
      group.name.toLocaleLowerCase().includes("trade"),
    );
  }
}
