import { EntityHydrator, Injector, Order, RequestContext } from "@vendure/core";
import {
  SelectedChoiceOption,
  SelectedNumberOption,
} from "../configurator/types";
import { ConfiguratorOptionGroup } from "../configurator/ui/generated/generated-graphql-types";
import { getFullSelectedOptions } from "../configurator/util/get-full-selected-options";
import { parseConfiguratorOptions } from "../configurator/util/parse-configurator-options";
import { parseSelectedOption } from "../configurator/util/parse-selected-option";

export async function getConfigurableOrderTotalWeightInGrams(
  ctx: RequestContext,
  order: Order,
  injector: Injector,
): Promise<number> {
  const entityHydrator = injector.get(EntityHydrator);
  await entityHydrator.hydrate(ctx, order, {
    relations: ["lines.productVariant.product"],
  });
  let totalWeightInGrams = 0;
  for (const line of order.lines) {
    const selectedConfiguratorOptions = (
      line.customFields?.selectedConfiguratorOptions ?? []
    ).map((configuratorOptions) => parseSelectedOption(configuratorOptions));
    const productOptionGroups: ConfiguratorOptionGroup[] =
      parseConfiguratorOptions(line.productVariant.product);
    const selectedoptions = getFullSelectedOptions(
      productOptionGroups,
      selectedConfiguratorOptions,
    );
    const widthInMm =
      (
        selectedoptions.find(
          (option) => option.optionGroupId === "width",
        ) as SelectedNumberOption
      )?.value ?? 1000;
    const heightInMm =
      (
        selectedoptions.find(
          (option) => option.optionGroupId === "height",
        ) as SelectedNumberOption
      )?.value ?? 1000;
    const areaInSquareMeters = (widthInMm * heightInMm) / (1000 * 1000);
    selectedoptions.forEach((selectedOption) => {
      if (!(selectedOption as any).customFields) {
        return;
      }
      totalWeightInGrams +=
        ((selectedOption as SelectedChoiceOption).customFields
          .weightGramsPerSquareMeter ?? 0) *
        areaInSquareMeters *
        line.quantity;
    });
    if (productOptionGroups?.length) {
      continue;
    }
    // if the product is not configurable
    totalWeightInGrams +=
      (line.productVariant?.customFields?.weight ??
        line.productVariant.product?.customFields?.weight ??
        0) * line.quantity;
  }
  return Math.round(totalWeightInGrams) / 1000;
}
