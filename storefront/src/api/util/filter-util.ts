export interface FilterOption {
  id: string;
  label: string;
}

export interface FilterCategory {
  id: string;
  name: string;
  options: FilterOption[];
}

export interface ProductWithFacets {
  facetValues: Array<{
    code: string;
    name: string;
    facet: { code: string; name: string };
  }>;
}
interface FacetValue {
  code: string;
  name: string;
  facet: { code: string; name: string };
}
/**
 * Get filters from items. Doesn't return filter categories with less than 2 options.
 * Items can be Search items or Products, as long as they have facet values with facets
 */
export function getFiltersFromItems<T extends { facetValues?: FacetValue[] }>(
  items: T[],
  excludedFilters: string[] = [],
): FilterCategory[] {
  const filters: FilterCategory[] = [];
  items.forEach((item) => {
    if (!item.facetValues) return;
    item.facetValues.forEach((facetValue) => {
      if (excludedFilters.includes(facetValue.facet.code)) {
        return;
      }
      const filter = filters.find(
        (filter) => filter.id === facetValue.facet.code,
      );
      const filterOption = { id: facetValue.code, label: facetValue.name };
      const optionExists = filter?.options.some(
        (o) => o.id === filterOption.id,
      );
      if (filter && !optionExists) {
        // Push to existing filter
        filter.options.push(filterOption);
      } else {
        // Create new filter
        filters.push({
          id: facetValue.facet.code,
          name: facetValue.facet.name,
          options: [filterOption],
        });
      }
    });
  });
  // Remove any filters with less than 2 options
  return filters.filter((filter) => filter.options.length > 1);
}

export function getItemsWithFilters<T extends { facetValues?: FacetValue[] }>(
  items: T[],
  selectedFilterIds: string[],
): T[] {
  if (selectedFilterIds.length === 0) {
    return items;
  }
  return items.filter((product) =>
    // Select items that have all selected filters
    selectedFilterIds.every(
      (selectedFilter) =>
        product.facetValues?.some((value) => value.code === selectedFilter),
    ),
  );
}
