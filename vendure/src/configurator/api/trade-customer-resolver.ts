import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { Ctx, Customer, RequestContext } from "@vendure/core";
import { TradeCustomerService } from "./trade-customer-service";

@Resolver()
export class TradeCustomerResolver {
  constructor(private readonly tradeCustomerService: TradeCustomerService) {}

  /**
   * Checks if the customer is in a customer group named "Trade", if so, the customer is a trade customer
   */
  @ResolveField()
  @Resolver("Customer")
  async isTradeCustomer(
    @Parent() customer: Customer,
    @Ctx() ctx: RequestContext,
  ): Promise<boolean> {
    return await this.tradeCustomerService.isTradeCustomer(ctx);
  }
}
