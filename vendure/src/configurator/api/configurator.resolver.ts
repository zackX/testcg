import { Args, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import {
  Ctx,
  EntityHydrator,
  Logger,
  OrderLine,
  Product,
  ProductVariant,
  RequestContext,
} from "@vendure/core";
import { loggerCtx } from "../constants";
import {
  ConfiguratorOptionGroup,
  ConfiguratorOptionGroupRadio,
  ConfiguratorPrice,
  HumanReadableSelectedConfiguratorOption,
  QueryCalculateConfiguratorPriceArgs,
} from "../ui/generated/generated-graphql-types";
import {
  ConfiguratorService,
  isConfigurableProduct,
} from "./configurator-service";
import { TradeCustomerService } from "./trade-customer-service";
import { resolveHumanReadableOptions } from "../util/resolve-human-readable-options";

@Resolver()
export class ConfiguratorResolver {
  constructor(
    private readonly configuratorService: ConfiguratorService,
    private readonly tradeCustomerService: TradeCustomerService,
    private readonly entityHydrator: EntityHydrator,
  ) {}

  @Query()
  async calculateConfiguratorPrice(
    @Ctx() ctx: RequestContext,
    @Args() args: QueryCalculateConfiguratorPriceArgs,
  ): Promise<ConfiguratorPrice> {
    return await this.configuratorService.calculate(
      ctx,
      args.variantId,
      args.quantity ?? 1,
      args.selectedOptions,
    );
  }

  @Query()
  storefront(@Ctx() ctx: RequestContext): string {
    return process.env.STOREFRONT;
  }

  @ResolveField("configuratorOptionGroups")
  @Resolver("Product")
  async configuratorOptions(
    @Ctx() ctx: RequestContext,
    @Parent() product: Product,
  ): Promise<ConfiguratorOptionGroup[] | undefined> {
    if (!product.customFields.configuratorOptionGroups) {
      return;
    }
    try {
      const configuratorOptionGroups: ConfiguratorOptionGroup[] = JSON.parse(
        product.customFields.configuratorOptionGroups,
      );
      return configuratorOptionGroups.filter(
        (o) => !this.containsHiddenOptions(o),
      );
    } catch (error) {
      Logger.error(
        `Failed to parse configuratorOptionGroups for product ${product.slug}: ${error}`,
        loggerCtx,
      );
    }
  }

  @ResolveField("isConfigurable")
  @Resolver("Product")
  isConfigurable(
    @Ctx() ctx: RequestContext,
    @Parent() product: Product,
  ): boolean {
    return isConfigurableProduct(product);
  }

  /**
   * Calculate a default price
   */
  @ResolveField("defaultConfiguratorPrice")
  @Resolver("ProductVariant")
  async defaultConfiguratorPrice(
    @Ctx() ctx: RequestContext,
    @Parent() variant: ProductVariant,
  ): Promise<ConfiguratorPrice | undefined> {
    if (!isConfigurableProduct(variant.product)) {
      return;
    }
    try {
      return await this.configuratorService.calculate(ctx, variant.id, 1, []);
    } catch (error) {
      Logger.error(
        `Failed to calculate "ProductVariant.defaultConfiguratorPrice" for variant ${variant.id}: ${error}`,
        loggerCtx,
      );
    }
  }

  /**
   * Parse fields for displaying on active order
   */
  @ResolveField("selectedConfiguratorOptions")
  @Resolver("OrderLine")
  async selectedConfiguratorOptions(
    @Ctx() ctx: RequestContext,
    @Parent() orderLine: OrderLine,
  ): Promise<HumanReadableSelectedConfiguratorOption[] | undefined> {
    await this.entityHydrator.hydrate(ctx, orderLine, {
      relations: ["productVariant.product"],
    });
    if (!isConfigurableProduct(orderLine.productVariant.product)) {
      return;
    }
    return resolveHumanReadableOptions(
      ctx,
      orderLine.productVariant.product,
      orderLine.customFields.selectedConfiguratorOptions,
    );
  }

  /**
   * Returns true if one of the options of the group contains hidden=true
   */
  containsHiddenOptions(optionGroup: ConfiguratorOptionGroup): boolean {
    return (optionGroup as ConfiguratorOptionGroupRadio).options?.some(
      (option) => option.customFields?.hidden,
    );
  }
}
