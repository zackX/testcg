# Colour Graphics Astro storefront

Starter template for a static storefront with Astro for Vendure

## Getting started

1. `yarn`
2. `yarn dev`
3. You can now see your live site on `http://localhost:4321/`

## Generating GraphQL types

Typescript types are generated based on the GraphQL queries defined in `integration/vendure-catalog/catalog-queries.graphql`.
To generate types:

1. `cd vendure`
2. `yarn dev`
3. In a new terminal: `cd storefront`
4. `yarn `

## Storefront structure

When working in Vue components, keep this in mind:

- Always check if there is a composable available for what you need to do. E.g `useActiveOrder` or `useActiveCustomer`. More on composables in the [Vue docs](https://vuejs.org/guide/reusability/composables)

```ts
const {
  // Reactive global active order
  activeOrder,
  // Reactive loading state
  loading,
  // Action for refetching active order. It's already loaded on mount when you use the composable!
  getActiveOrder,
  // More actions to mutate the active order
  setCustomerForOrder,
  setOrderShippingAddress,
  setOrderBillingAddress,
  setOrderShippingMethod,
} = useActiveOrder();
```

- If a composable is not available, you can use the Vendure client directly, like so:

```ts
import { vendureClient } from "../api/graphql/vendure-client.ts";

const {
  data: { availableCountries },
} = vendureClient.AvailableCountries();
```

- All actions are defined in `.graphql` files in `src/api/graphql/*.graphql`.
- In Astro files (server side), you can use `src/api/vendure-catalog`.
