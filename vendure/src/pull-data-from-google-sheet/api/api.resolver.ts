import { Args, Mutation, Resolver, Query } from "@nestjs/graphql";
import { Ctx, Allow, RequestContext, Transaction } from "@vendure/core";
import { Permission } from "@vendure/common/lib/generated-types";
import { GoogleSheetService } from "./google-sheet.service";
import { GoogleSheetInput } from "../ui/generated/generated-graphql-types";
import { GoogleSheetMetaData } from "./entities/google-sheet-meta-data.entity";
@Resolver()
export class PullDataFromGoogleSheetResolver {
  constructor(private readonly googleSheetService: GoogleSheetService) {}

  @Transaction()
  @Mutation()
  @Allow(Permission.UpdateSettings)
  async readGoogleSheet(
    @Ctx() ctx: RequestContext,
    @Args("sheetInput") sheetInput: GoogleSheetInput,
  ): Promise<{ success: boolean }> {
    return await this.googleSheetService.readGoogleSheet(ctx, sheetInput);
  }

  @Query()
  @Allow(Permission.ReadSettings)
  async activeGoogleSheetMetaData(
    @Ctx() ctx: RequestContext,
  ): Promise<GoogleSheetMetaData | null> {
    return await this.googleSheetService.getActiveGoogleSheetMetaData(ctx);
  }
}
