import { NgModule } from "@angular/core";
import {
  SharedModule,
  registerFormInputComponent,
  registerCustomDetailComponent,
  addActionBarItem,
  ORDER_DETAIL_FRAGMENT,
} from "@vendure/admin-ui/core";
import { ConfiguratorOptionsComponent } from "./configurator-options/configurator-options.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { GroupOptionNumberModalComponent } from "./configurator-option-group-number-modal/configurator-option-group-number-modal";
import { GroupOptionModalComponent } from "./configurator-option-group-modal/configurator-option-group-modal";
import { DraftOrderConfiguratorOptionsComponent } from "./draft-order-configurator-options/draft-order-configurator-options.component";
import { DraftOrderLineEmptyUI } from "./draft-order-line-empty-ui/draft-order-line-empty-ui.component";
import { DraftOrderLineConfiguratorModalComponent } from "./draft-order-line-configurator-modal/draft-order-line-configurator-modal.component";
import { NotConfigurableProductComponent } from "./not-configurable-product/not-configurable-product.component";
import { DraftOrderLineEmptyListUI } from "./draft-order-line-empty-list-ui/draft-order-line-empty-list-ui.component";
import { gql } from "graphql-tag";
@NgModule({
  imports: [SharedModule, DragDropModule],
  declarations: [
    ConfiguratorOptionsComponent,
    GroupOptionModalComponent,
    GroupOptionNumberModalComponent,
    DraftOrderConfiguratorOptionsComponent,
    DraftOrderLineConfiguratorModalComponent,
    NotConfigurableProductComponent,
    DraftOrderLineEmptyUI,
    DraftOrderLineEmptyListUI,
  ],
  providers: [
    registerFormInputComponent(
      "configurator-option-groups-form-input",
      ConfiguratorOptionsComponent,
    ),
    registerCustomDetailComponent({
      locationId: "draft-order-detail",
      component: DraftOrderConfiguratorOptionsComponent,
    }),
    // This Component is added for the sole purpose of passing an empty
    // array ([]) by default for draft order line, which will later be configured via
    // the Add Configurator Options custom detail view
    registerFormInputComponent(
      "draft-order-line-empty-ui",
      DraftOrderLineEmptyUI,
    ),
    registerFormInputComponent(
      "draft-order-line-empty-list-ui",
      DraftOrderLineEmptyListUI,
    ),
    addActionBarItem({
      id: "order-confirmation-page-button",
      label: "Open Order Status Page",
      locationId: "order-detail",

      onClick: (_, context) => {
        const orderId = context.route.snapshot.paramMap.get("id");
        context.dataService
          .query(
            gql`
              query GetOrderDetailAndStorefrontURL($id: ID!) {
                order(id: $id) {
                  ...OrderDetail
                  customer {
                    id
                    firstName
                    lastName
                    emailAddress
                  }
                }
                storefront
              }
              ${ORDER_DETAIL_FRAGMENT}
            `,
            { id: orderId },
          )
          .single$.subscribe((orderData: any) => {
            const order = orderData.order;
            const storefront = orderData.storefront;
            if (!storefront) {
              context.notificationService.error(
                "Storefront location not configured",
              );
            }
            if (order?.customer?.emailAddress && order?.code && storefront) {
              const link = `${storefront}/order/?code=${order.code}&email=${order.customer.emailAddress}`;
              window.open(link, "_blank");
            } else {
              context.notificationService.error("Customer not set");
            }
          });
      },
    }),
  ],
})
export class SharedExtensionModule {}
