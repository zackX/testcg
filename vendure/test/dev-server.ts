import { AdminUiPlugin } from "@vendure/admin-ui-plugin";
import { AssetServerPlugin } from "@vendure/asset-server-plugin";
import {
  DefaultJobQueuePlugin,
  DefaultLogger,
  DefaultSearchPlugin,
  JobQueueService,
  LogLevel,
  OrderProcess,
  RequestContextService,
  defaultOrderProcess,
  mergeConfig,
} from "@vendure/core";
import {
  SimpleGraphQLClient,
  SqljsInitializer,
  TestServer,
  createTestEnvironment,
  registerInitializer,
  testConfig,
} from "@vendure/testing";
import { compileUiExtensions } from "@vendure/ui-devkit/compiler";
import path from "path";
import { GoogleStoragePlugin } from "@pinelab/vendure-plugin-google-storage-assets";
import { colourGraphicsCalculation } from "../src/configurator/api/cg-calculation";
import { printingOrderProcess } from "../src/config/printing-order-process";
import { ConfiguratorPlugin } from "../src/configurator/configurator-plugin";
import { ColorGraphicsOptionSyncStrategy } from "../src/config/color-graphics-option-sync";
import { PullDataFromGoogleSheetPlugin } from "../src/pull-data-from-google-sheet/google-sheet-plugin";
import { initialData, testPaymentMethod } from "./initial-data";
import {
  createHeightOption,
  createWidthOption,
  materialThickness10mm,
  mockOptionGroups,
} from "./mock-data";
import { AddItemToOrder, UpdateProduct } from "./queries";
// Local dev server that uses SQLite
import dotenv from "dotenv";
import { DataSource } from "typeorm";
import { GoogleSheetMetaData } from "../src/pull-data-from-google-sheet/api/entities/google-sheet-meta-data.entity";
import { PrimaryCollectionPlugin } from "@pinelab/vendure-plugin-primary-collection/dist/primary-collection-plugin";
import { ProductDocumentsPlugin } from "../src/product-documents/plugin";
import { SimplifiedCollectionsPlugin } from "vendure-simplified-collections-plugin";
import { PopularityScoresPlugin } from "@pinelab/vendure-plugin-popularity-scores";
import { customFields } from "../src/config/custom-fields";
import { CustomStateTransitionPlugin } from "../src/custom-state-transition/custom-state-transition.plugin";
import { ShippingPlugin } from "../src/shipping/shipping.plugin";
import { AnonymizedOrderPlugin } from "@pinelab/vendure-plugin-anonymized-order";
import { MolliePlugin } from "@vendure/payments-plugin/package/mollie";
import { DraftOrderPlugin } from "../src/draft-order/draft-order-plugin";
import {
  TurnaroundShippingCalculatorArgValues,
  createTurnaroundShippingMethod,
} from "./helpers";
import {
  ShippingExtensionsPlugin,
  UKPostalCodeToGelocationConversionStrategy,
} from "@pinelab/vendure-plugin-shipping-extensions";
import { getConfigurableOrderTotalWeightInGrams } from "../src/config/configurable-item-weight-calculator";
import { EmailPlugin, defaultEmailHandlers } from "@vendure/email-plugin";
import { draftOrderCompletionHandler } from "../src/config/draft-order-completion.handler";
import { turnaroundShippingMethodCodeFragment } from "../src/shipping/util/turnaround";
import {
  InvoicePlugin,
  InvoiceService,
} from "@pinelab/vendure-plugin-invoices";
import { PicklistService } from "@pinelab/vendure-plugin-picklist/dist/api/picklist.service";
import { defaultTemplate } from "@pinelab/vendure-plugin-picklist/dist/api/default-template";
import { invoiceLoadDataFn } from "../src/config/invoice-data-loader";
import { PicklistPlugin } from "@pinelab/vendure-plugin-picklist/dist/plugin";
import { picklistLoadDataFn } from "../src/config/picklist-data-loader";
import { MultiServerDbSessionCachePlugin } from "@pinelab/vendure-plugin-multiserver-db-sessioncache";

dotenv.config();
(async () => {
  let server: TestServer;
  let adminClient: SimpleGraphQLClient;
  let shopClient: SimpleGraphQLClient;

  const config = mergeConfig(testConfig, {
    logger: new DefaultLogger({ level: LogLevel.Debug }),
    paymentOptions: {
      paymentMethodHandlers: [testPaymentMethod],
    },
    authOptions: {
      tokenMethod: "bearer",
    },
    customFields,
    apiOptions: {
      port: 3050,
      adminApiPlayground: true,
      shopApiPlayground: true,
      shopListQueryLimit: 1000,
    },
    orderOptions: {
      process: [
        defaultOrderProcess,
        printingOrderProcess,
      ] as OrderProcess<any>[],
    },
    dbConnectionOptions: {
      ...testConfig.dbConnectionOptions,
      synchronize: true,
    },
    importExportOptions: {
      importAssetsDir: path.join(__dirname, "assets"),
    },
    plugins: [
      MolliePlugin.init({
        vendureHost: "dev-server-mock-host",
        useDynamicRedirectUrl: true,
      }),
      DraftOrderPlugin,
      AnonymizedOrderPlugin,
      PrimaryCollectionPlugin,
      GoogleStoragePlugin,
      ConfiguratorPlugin.init({
        calculation: colourGraphicsCalculation,
      }),
      DefaultSearchPlugin,
      ProductDocumentsPlugin,
      AssetServerPlugin.init({
        route: "assets",
        assetUploadDir: "__data__/assets",
      }),
      InvoicePlugin.init({
        // Used for generating download URLS for the admin ui
        vendureHost: "http://localhost:3050",
        loadDataFn: invoiceLoadDataFn,
      }),
      PicklistPlugin.init({ loadDataFn: picklistLoadDataFn }),
      PullDataFromGoogleSheetPlugin.init({
        dataStrategy: new ColorGraphicsOptionSyncStrategy(),
        googleApiKey: process.env.GOOGLE_API_KEY!,
      }),
      PopularityScoresPlugin.init({
        endpointSecret: "test",
      }),
      DefaultJobQueuePlugin.init({}),
      SimplifiedCollectionsPlugin,
      CustomStateTransitionPlugin,
      EmailPlugin.init({
        // Only for dev
        devMode: true,
        outputPath: path.join(__dirname, "../static/email/test-emails"),
        route: "mailbox",
        handlers: [...defaultEmailHandlers, draftOrderCompletionHandler],
        templatePath: path.join(__dirname, "../static/email/templates"),
        globalTemplateVars: {
          fromAddress: '"Colour Graphics" <noreply@pinelab.studio>',
          verifyEmailAddressUrl: `${process.env.STOREFRONT}/verify`,
          passwordResetUrl: `${process.env.STOREFRONT}/password-reset`,
          changeEmailAddressUrl: `${process.env.STOREFRONT}/verify-email-address-change`,
          storefrontUrl: process.env.STOREFRONT,
        },
      }),
      AdminUiPlugin.init({
        route: "admin",
        port: 3002,
        app: process.env.COMPILE_ADMIN
          ? // Run in hot reload mode if COMPILE_ADMIN
            compileUiExtensions({
              outputPath: path.join(__dirname, "./__admin-ui/dist"),
              extensions: [
                ConfiguratorPlugin.uiExtensions,
                ProductDocumentsPlugin.uiExtensions,
                PullDataFromGoogleSheetPlugin.uiExtensions,
                SimplifiedCollectionsPlugin.uiExtensions,
                ShippingPlugin.uiExtensions,
                InvoicePlugin.ui,
                PicklistPlugin.ui,
                {
                  translations: {
                    en: path.join(__dirname, "../src/translations/en.json"),
                  },
                },
              ],
              devMode: true,
            })
          : // Otherwise use precompiled admin
            {
              path: path.join(__dirname, "../__admin-ui/dist"),
            },
      }),
      ShippingPlugin,
      ShippingExtensionsPlugin.init({
        weightUnit: "kg",
        customFieldsTab: "Physical properties",
        orderAddressToGeolocationStrategy:
          new UKPostalCodeToGelocationConversionStrategy(),
        weightCalculationFunction: getConfigurableOrderTotalWeightInGrams,
      }),
      MultiServerDbSessionCachePlugin,
    ],
  });

  registerInitializer("sqljs", new SqljsInitializer("__data__"));
  ({ server, adminClient, shopClient } = createTestEnvironment(config));
  await server.init({
    initialData: {
      ...initialData,
      paymentMethods: [
        {
          name: testPaymentMethod.code,
          handler: { code: testPaymentMethod.code, arguments: [] },
        },
      ],
    },
    productsCsvPath: "./test/products.csv",
  });
  const jobQueueService = server.app.get(JobQueueService);
  await jobQueueService.start();
  await adminClient.asSuperAdmin();
  await adminClient.query(UpdateProduct, {
    input: {
      id: 1,
      customFields: {
        configuratorOptionGroups: JSON.stringify(mockOptionGroups),
      },
    },
  });
  console.log(`Created mock option groups`);
  const ctx = await server.app.get(RequestContextService).create({
    apiType: "admin",
  });
  //create default invoice config
  await server.app.get(InvoiceService).upsertConfig(ctx, { enabled: true });
  // add default picklist Config
  await server.app.get(PicklistService).upsertConfig(ctx, defaultTemplate);
  // Add item to cart
  const order = await shopClient.query(AddItemToOrder, {
    productVariantId: 1,
    quantity: 1,
    customFields: {
      selectedConfiguratorOptions: [
        materialThickness10mm,
        createHeightOption(900),
        createWidthOption(900),
      ],
    },
  });
  await shopClient.asUserWithCredentials("hayden.zieme12@hotmail.com", "test");
  await shopClient.query(AddItemToOrder, {
    productVariantId: 1,
    quantity: 1,
    customFields: {
      selectedConfiguratorOptions: [
        materialThickness10mm,
        createHeightOption(6000),
        createWidthOption(9000),
      ],
    },
  });
  console.log(`Added item to order: ${order.addItemToOrder.totalWithTax}`);
  console.log(
    `Stringified configurator options: ${JSON.stringify(mockOptionGroups)}`,
  );
  // Add default sheet data for testing
  const activeGoogleSheetMetaData = new GoogleSheetMetaData();
  activeGoogleSheetMetaData.channels = [{ id: 1 } as any];
  server.app
    .get(DataSource)
    .getRepository(GoogleSheetMetaData)
    .save({
      channels: [{ id: 1 }],
      sheetName: "Global Options",
      url: process.env.SAMPLE_URL ?? "You can set the dev url in dev-server.ts",
    });
  let threeDayTurnaroundShippingArgValues: TurnaroundShippingCalculatorArgValues =
    {
      taxRate: 0,
      percentageOfCart: 10,
      minimumPrice: 123,
    };
  let twoDayTurnaroundShippingArgValues: TurnaroundShippingCalculatorArgValues =
    {
      taxRate: 0,
      percentageOfCart: 120,
      minimumPrice: 200,
    };
  const threeDayTurnaroundShippingMethodCode = `${turnaroundShippingMethodCodeFragment}-3`;
  const twoDayTurnaroundShippingMethodCode = `${turnaroundShippingMethodCodeFragment}-2`;
  await createTurnaroundShippingMethod(
    adminClient,
    threeDayTurnaroundShippingArgValues,
    threeDayTurnaroundShippingMethodCode,
    "3 Day Turnaround Shipping Method",
  );
  await createTurnaroundShippingMethod(
    adminClient,
    twoDayTurnaroundShippingArgValues,
    twoDayTurnaroundShippingMethodCode,
    "2 Day Turnaround Shipping Method",
  );
})();
