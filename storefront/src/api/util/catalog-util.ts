import { ErrorResult } from "../graphql/generated";

// TODO: these utils should be moved to a publishable package in pinelab-plugins repo

// TODO: document: 1 fetch collections, 2 set path and fullbreadcrumbs for all collections,
// 3 get all products, 4 set path and breadcrumbs for all products based on parent collections

export interface BasicCollection {
  id: number | string;
  name: string;
  slug: string;
  breadcrumbs?: Array<{ id: string | number; name: string; slug: string }>;
  parent?: BasicCollection | null;
  children?: BasicCollection[] | null;
}

/**
 * Interface defining a collection that already has a full path breadcrumb set.
 * See @FullPathBreadcrumb for more info
 */
export interface CollectionsWithPath extends BasicCollection {
  path: string;
}

export interface BasicProduct {
  id: number | string;
  name: string;
  slug: string;
  primaryCollection?: BasicCollection | null;
  variants: Array<{
    priceWithTax: number;
  }>;
  path: string;
}

/**
 * Helper type that defines lowestPrice and highestPrice on a type
 */
export interface PriceRange {
  lowestPrice: number;
  highestPrice: number;
}

/**
 * Breadcrumb with a full path. An extension on Vendure's breadcrumb which only contains the slug
 */
export interface FullPathBreadcrumb {
  id: string | number;
  slug: string;
  path: string;
  name: string;
}

interface MinimalOrder {
  code: string;
  lines: Array<{
    quantity: number;
  }>;
}

/**
 * Throw if result is an ErrorResult
 */
export function throwIfErrorResult<T>(
  result?: T | ErrorResult | null,
): T | null {
  if (!result) {
    return null;
  }
  if (result && (result as ErrorResult).errorCode) {
    const error = result as ErrorResult;
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    throw error;
  }
  // We've verified that result is not an error, so we can safely cast it
  return result as T;
}

/**
 * Helper function to determine how many items were added or removed from the order.
 * Negative number means items were removed, positive means items were added and 0 means no changes
 * Useful when you want to show notifications like "Added 3 items to cart" or "Removed 1 item from cart"
 */
export function howManyItemsAddedorRemoved(
  newOrder?: MinimalOrder,
  oldOrder?: MinimalOrder,
): number {
  if (!newOrder || !oldOrder) {
    return 0;
  }
  const oldTotal = oldOrder.lines.reduce((acc, line) => acc + line.quantity, 0);
  const newTotal = newOrder.lines.reduce((acc, line) => acc + line.quantity, 0);
  return newTotal - oldTotal;
}

/**
 * Remove duplicates from an array of items.
 * Can be anything with an id property: Collections, products, variants, etc.
 */
export function deduplicate<T extends { id: string | number }>(
  products: T[],
): T[] {
  const uniq: Array<string | number> = [];
  return products.filter((prod) => {
    if (!uniq.includes(prod.id)) {
      uniq.push(prod.id);
      return true;
    }
    return false;
  });
}

/**
 * Construct the path based on breadcrumb. E.g "/clothing/t-shirts/"
 */
export function getPathForBreadcrumb(
  breadcrumbs: Array<{ slug: string }>,
): string {
  return (
    `/` +
    breadcrumbs
      .filter(({ slug }) => slug !== "__root_collection__") // Filter out root collection
      .map(({ slug }) => slug)
      .join("/")
  );
}

/**
 * Set product.lowestPrice and product.highestPrice to the lowest and highest price of the product variants.
 */
export function getPriceRange(
  product: Pick<BasicProduct, "variants">,
): PriceRange {
  const lowestPrice = Math.min(...product.variants.map((v) => v.priceWithTax));
  const highestPrice = Math.max(...product.variants.map((v) => v.priceWithTax));
  return {
    lowestPrice,
    highestPrice,
  };
}

/**
 * Construct a tree based on the given list of collections, based on parent
 */
export function createCollectionTree<T extends BasicCollection>(
  allCollections: T[],
): T[] {
  // Get toplevel collections
  const collections = allCollections.filter(
    (col) => col.parent?.name === "__root_collection__",
  );
  // Find and set child collections for all top level collections
  return collections.map((collection) =>
    getChildCollection(collection, allCollections),
  );
}

/**
 * Recursively resolver children of given collection
 */
export function getChildCollection<T extends BasicCollection>(
  collection: T,
  allCollections: T[],
): T {
  const childCollections = collection.children?.map(({ id: childId, name }) => {
    let childCollection = allCollections.find((c) => c.id === childId);
    if (!childCollection) {
      throw Error(`Child collection ${name} is not in allCollections list`);
    }
    if (childCollection?.children?.length) {
      childCollection = getChildCollection(childCollection, allCollections);
    }
    return childCollection;
  });
  return {
    ...collection,
    children: childCollections,
  };
}

export function getBreadcrumbsForCollection(
  collection: BasicCollection,
  allCollections: CollectionsWithPath[],
): FullPathBreadcrumb[] {
  const breadcrumbs: FullPathBreadcrumb[] = [];
  collection.breadcrumbs?.forEach((breadcrumb) => {
    if (breadcrumb.slug === "__root_collection__") {
      return;
    }
    const collectionWithPath = allCollections.find(
      (c) => c.id === breadcrumb.id,
    );
    if (!collectionWithPath) {
      console.error(
        `Collection '${breadcrumb.slug}' is not in allCollections, something is wrong. Maybe it's private?`,
      );
      return;
    }
    breadcrumbs.push({
      id: collectionWithPath.id,
      slug: collectionWithPath.slug,
      name: collectionWithPath.name,
      path: collectionWithPath.path,
    });
  });
  return breadcrumbs;
}

export function getBreadcrumbsForProduct(
  product: BasicProduct,
  allCollections: CollectionsWithPath[],
): FullPathBreadcrumb[] {
  if (!product.primaryCollection) {
    return [
      {
        id: product.id,
        name: product.name,
        path: product.slug,
        slug: product.slug,
      },
    ];
  }
  const collectionWithPath = allCollections.find(
    (c) => c.id === product.primaryCollection?.id,
  );
  if (!collectionWithPath) {
    console.error(
      `Primary Collection '${product.primaryCollection.slug}' of product ${product.slug} is not in allCollections, something is wrong. Maybe it's private?`,
    );
    return [
      {
        id: product.id,
        name: product.name,
        path: product.slug,
        slug: product.slug,
      },
    ];
  }
  const parentCollectionBreadcrumbs = getBreadcrumbsForCollection(
    collectionWithPath,
    allCollections,
  );
  return [
    ...parentCollectionBreadcrumbs,
    {
      id: product.id,
      name: product.name,
      path: product.path,
      slug: product.slug,
    },
  ];
}

export function removeLeadingSlash(path: string): string {
  return path.replace(/^\/+/, "");
}

export function removeTrailingSlash(path: string): string {
  return path.replace(/\/$/, "");
}

export function removeTrailingAndLeadingSlash(path: string): string {
  return removeTrailingSlash(removeLeadingSlash(path));
}
