// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Vue from "vue";
import { filters } from "./src/utils/filters";

declare module "vue" {
  interface ComponentCustomProperties {
    $filters: filters;
  }
}
