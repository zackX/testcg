import { CustomFields, LanguageCode } from "@vendure/core";

export const customFields: CustomFields = {
  OrderLine: [
    {
      name: "customerNote",
      type: "text",
      readonly: false,
      nullable: true,
      ui: { component: "draft-order-line-empty-ui" },
      description: [
        {
          languageCode: LanguageCode.en,
          value: "Special instruction from the customer",
        },
      ],
    },
  ],
  Product: [
    {
      name: "metaTitle",
      type: "localeString",
      label: [
        {
          languageCode: LanguageCode.en,
          value: "Meta Title",
        },
      ],
      ui: {
        tab: "SEO",
      },
    },
    {
      name: "metaDescription",
      type: "localeString",
      label: [
        {
          languageCode: LanguageCode.en,
          value: "Meta Description",
        },
      ],
      ui: {
        tab: "SEO",
        component: "textarea-form-input",
      },
    },
    {
      name: "shortDescription",
      type: "localeString",
      label: [
        {
          languageCode: LanguageCode.en,
          value: "Short Description",
        },
      ],
      ui: {
        component: "rich-text-form-input",
      },
    },
  ],
};
