import type { App } from "vue";
import { filters } from "./utils/filters";

export default function (app: App): void {
  app.config.globalProperties.$filters = filters;
}
