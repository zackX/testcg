import { config } from "dotenv";
import { GraphQLClient, gql } from "graphql-request";
import fs from "fs/promises";
import neatCsv from "neat-csv";

/**
 * This script reads the blogs from the `blogs.csv` file and imports them into Directus via the Graphql API
 * Make sure to set cg_blogs creation to public in Directus before running this script
 *
 * Usage: `yarn script scripts/import-blogs.ts`
 */

const createBlogMutation = gql`
  mutation ($data: create_cg_blogs_input!) {
    create_cg_blogs_item(data: $data) {
      id
    }
  }
`;

(async () => {
  config(); // Load .env variables
  const directus = new GraphQLClient(`${process.env.DIRECTUS_HOST}/graphql`);
  const blogs = (await neatCsv(
    await fs.readFile("./scripts/blogs.csv", "utf-8"),
  )) as CsvLine[];
  for (const blog of blogs) {
    if (!blog.publish_time || blog.publish_time?.startsWith("0000")) {
      // This means its a draft post
      continue;
    }
    const input: BlogInput = {
      title: blog.title,
      slug: blog.identifier,
      metaTitle: blog.meta_title,
      metaDescription: blog.meta_description,
      metaKeywords: blog.meta_keywords,
      content: blog.content,
      publishedAt: blog.publish_time,
      categories: blog.categories.split(","),
      tags: blog.categories.split(","),
    };
    // Remove any inline base64 images to prevent 'Entity Too Large' errors
    input.content = input.content.replace(
      /<img\s[^>]*?src="data:[^"]*?"[^>]*?>/gi,
      " ",
    );
    input.content = input.content.replaceAll(
      "https://www.colourgraphics.com/",
      "/",
    );
    const { create_cg_blogs_item } = await directus.request(
      createBlogMutation,
      { data: input },
    );
    console.log(
      `Created blog '${input.slug}' with id ${create_cg_blogs_item.id}`,
    );
  }
  console.log(`Created ${blogs.length} blogs`);
})();

interface BlogInput {
  title: string;
  slug: string;
  metaTitle: string;
  metaKeywords: string;
  metaDescription: string;
  publishedAt: string;
  content: string;
  categories: string[];
  tags: string[];
}

interface CsvLine {
  post_id: string;
  title: string;
  meta_title: string;
  /**
   * New line separated list of keywords
   */
  meta_keywords: string;
  meta_description: string;
  /**
   * Slug of the blog post
   */
  identifier: string;
  content: string;
  publish_time: string;
  categories: string;
  tags: string;
}
