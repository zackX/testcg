/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  CustomProductFields,
  CustomOrderLineFields,
} from "@vendure/core/dist/entity/custom-entity-fields";

declare module "@vendure/core" {
  interface CustomProductFields extends ProductWithConfiguratorOptions {
    /**
     * Stringified JSON of ConfiguratorOptionGroup[]
     */
    configuratorOptionGroups?: string;
    /**
     * Added by ShippingExtensionsPlugin
     */
    weight?: number;
  }
  interface CustomOrderLineFields {
    /**
     * Stringified JSON of SelectedConfiguratorOption[]
     */
    selectedConfiguratorOptions?: string[];
    customUnitPriceExVAT?: number;
  }
  interface CustomProductVariantFields {
    /**
     * Added by ShippingExtensionsPlugin
     */
    weight?: number;
  }
}
