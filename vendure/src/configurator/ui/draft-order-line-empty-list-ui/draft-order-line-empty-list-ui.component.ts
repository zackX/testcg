import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { StringCustomFieldConfig } from "@vendure/common/lib/generated-types";
import { FormInputComponent } from "@vendure/admin-ui/core";

@Component({
  selector: "draft-order-line-empty-ui",
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraftOrderLineEmptyListUI
  implements OnInit, FormInputComponent<StringCustomFieldConfig>
{
  isListInput?: boolean | undefined = true;
  readonly: boolean;
  formControl: FormControl;
  config: StringCustomFieldConfig;
  constructor(private readonly cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (!this.formControl.value?.length) {
      this.formControl.setValue([]);
    }
  }
}
