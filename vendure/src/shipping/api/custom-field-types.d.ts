/* eslint-disable @typescript-eslint/no-unused-vars */
import { CustomOrderFields } from "@vendure/core/dist/entity/custom-entity-fields";
import { CustomFields } from "@vendure/core/";

declare module "@vendure/core/dist/entity/custom-entity-fields" {
  interface CustomOrderFields {
    shippingDate: Date;
    artworkReceived: boolean;
    /**
     * Purchase order number
     */
    customerReference?: string;
  }
}
