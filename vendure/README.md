# Colour Graphics Vendure

Based on this template: https://github.com/Pinelab-studio/vendure-google-cloud-run-starter (Needs updating)

## Development

Make sure you have a copy of `.env.example` with the correct values.

In the root of the project:

- Run `yarn`

Then in the folder `vendure`:

- Run `yarn`
- `yarn serve:prod`

You should be able to access the Vendure admin via https://localhost:3000/admin
