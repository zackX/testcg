import { Product, Translated, UserInputError } from "@vendure/core";
import { ConfiguratorOptionGroup } from "../ui/generated/generated-graphql-types";

/**
 * Parse the stringified JSON in product.customFields.configuratorOptionGroups
 */
export function parseConfiguratorOptionsOrFail(
  product: Product | Translated<Product>,
): ConfiguratorOptionGroup[] {
  const optionGroupsString = product.customFields.configuratorOptionGroups;
  if (!optionGroupsString) {
    throw Error(`No option groups configured for product ${product.slug}`);
  }
  let optionGroups: ConfiguratorOptionGroup[] = [];
  try {
    // Parse optionGroups, because it's a stringified JSON object
    optionGroups = JSON.parse(optionGroupsString);
  } catch (e: any) {
    throw new UserInputError(
      `Invalid JSON found for option groups of product ${product.slug}: ${e?.message}`,
    );
  }
  return optionGroups;
}
/**
 * The following method does what the above function does but won't complain when a Product has no customFields.configuratorOptionGroups
 */
export function parseConfiguratorOptions(
  product: Product | Translated<Product>,
): ConfiguratorOptionGroup[] {
  return JSON.parse(product.customFields.configuratorOptionGroups ?? "[]");
}
