import dayjs from "dayjs";

export const ukBankHolidays: dayjs.Dayjs[] = [
  dayjs("2024-03-29"), // 29 March, Good Friday
  dayjs("2024-04-01"), // 1 April, Easter Monday
  dayjs("2024-05-06"), // 6 May, Early May bank holiday
  dayjs("2024-27-06"), // 27 May, Spring bank holiday
  dayjs("2024-08-26"), // 26 August, Summer bank holiday
  dayjs("2024-12-25"), // 25 December, Christmas Day
  dayjs("2024-12-26"), // 26 December, Boxing Day
];
export function shouldSkipDay(day: dayjs.Dayjs): boolean {
  return isThisDayAUKBankHoliday(day) || !isThisDayAWeekday(day);
}

function isThisDayAUKBankHoliday(day: dayjs.Dayjs): boolean {
  for (const bankHoliday of ukBankHolidays) {
    if (day.isSame(bankHoliday, "day")) {
      return true;
    }
  }
  return false;
}

export function isThisDayAWeekday(day: dayjs.Dayjs): boolean {
  return day.day() >= 1 && day.day() <= 5;
}

export function isPastMidday(day: dayjs.Dayjs): boolean {
  const noonTime = dayjs(day)
    .set("hour", 12)
    .set("minute", 0)
    .set("second", 0)
    .set("millisecond", 0);
  return day.isAfter(noonTime);
}
