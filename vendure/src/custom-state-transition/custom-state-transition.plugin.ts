import { PluginCommonModule, VendurePlugin } from "@vendure/core";
import { shopApiExtension } from "./schema";
import { CustomStateTransitionService } from "./custom-state-transition.service";
import { CustomStateTransitionShopResolver } from "./custom-state-transition.resolver";

@VendurePlugin({
  imports: [PluginCommonModule],
  providers: [CustomStateTransitionService],
  shopApiExtensions: {
    schema: shopApiExtension,
    resolvers: [CustomStateTransitionShopResolver],
  },
})
export class CustomStateTransitionPlugin {}
