import { ref } from "vue";
import {
  CustomerFieldsFragment,
  ErrorResult,
  OrderFieldsFragment,
} from "./api/graphql/generated";
import { Notification } from "./model";

export const activeOrder = ref<OrderFieldsFragment | undefined | null>(
  undefined,
);
// This error state is global, because we want to be able to listen for these types of errors in the UI
export const activeOrderError = ref<ErrorResult | undefined>(undefined);
export const activeCustomer = ref<CustomerFieldsFragment | undefined | null>(
  undefined,
);

// Notification Message
export const notificationMessage = ref<Notification | null>(null);

// Set Notification
export const setNotification = (message: Notification): void => {
  notificationMessage.value = message;
};
