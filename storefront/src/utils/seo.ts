export function getSeoDescription(description?: string): string {
  const maxlength = 120;
  const minlength = 70;
  if (!description) {
    return "";
  }
  description = description.replace(/<[^>]*>?/gm, " "); // replace html
  description = description.replace(/&nbsp;/g, " "); // replace &nbsp;
  description.replace(/\s\s+/g, " "); // replace double spaces
  const sentence = description.substr(0, description.indexOf(".") + 1);
  if (sentence.length > maxlength) {
    // truncate to max
    return sentence.substring(0, maxlength).trim() + "...";
  } else if (sentence.length > minlength) {
    // Between min and max, perfect!
    return sentence.trim();
  } else {
    // Sentence too short, just take first X chars of description
    return description.substring(0, maxlength).trim() + "...";
  }
}
