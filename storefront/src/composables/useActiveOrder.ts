import { onMounted, ref } from "vue";
import {
  AddItemToOrderMutationVariables,
  AddPaymentToOrderMutationVariables,
  AdjustOrderLineMutationVariables,
  ApplyCouponCodeMutationVariables,
  RemoveAllOrderLinesMutationVariables,
  RemoveCouponCodeMutationVariables,
  SetCustomerForOrderMutationVariables,
  SetOrderBillingAddressMutationVariables,
  SetOrderCustomFieldsMutationVariables,
  SetOrderShippingAddressMutationVariables,
  SetOrderShippingMethodMutationVariables,
  TransitionOrderToStateMutationVariables,
} from "../api/graphql/generated.ts";
import { vendureClient } from "../api/graphql/vendure-client.ts";
import { throwIfErrorResult } from "../api/util/catalog-util.ts";
import { activeOrder, activeOrderError } from "../store";
import { OrderFieldsFragment } from "../api/graphql/generated";

/**
 * Everything you need to manage an active order.
 * @example
 * `const { activeOrder, loading, addItemToOrder } = useActiveOrder();`
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function useActiveOrder() {
  const loading = ref(false);
  activeOrderError.value = undefined;

  async function getActiveOrder(): Promise<void> {
    loading.value = true;
    await vendureClient
      .ActiveOrder()
      .then((result) => throwIfErrorResult(result.data.activeOrder))
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function addItemToOrder(
    variables: AddItemToOrderMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .AddItemToOrder(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(result.data.addItemToOrder),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function adjustOrderLine(
    variables: AdjustOrderLineMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .AdjustOrderLine(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(result.data.adjustOrderLine),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function removeOrderLine(orderLineId: string | number): Promise<void> {
    await adjustOrderLine({ orderLineId, quantity: 0 });
  }

  async function removeAllOrderLines(
    variables: RemoveAllOrderLinesMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .RemoveAllOrderLines(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.removeAllOrderLines,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function applyCouponCode(
    variables: ApplyCouponCodeMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .ApplyCouponCode(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(result.data.applyCouponCode),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function removeCouponCode(
    variables: RemoveCouponCodeMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .RemoveCouponCode(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(result.data.removeCouponCode),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function setCustomerForOrder(
    variables: SetCustomerForOrderMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .SetCustomerForOrder(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.setCustomerForOrder,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function setOrderShippingAddress(
    variables: SetOrderShippingAddressMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .SetOrderShippingAddress(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.setOrderShippingAddress,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function setOrderBillingAddress(
    variables: SetOrderBillingAddressMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .SetOrderBillingAddress(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.setOrderBillingAddress,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function setOrderShippingMethod(
    variables: SetOrderShippingMethodMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .SetOrderShippingMethod(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.setOrderShippingMethod,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function transitionOrderTostate(
    variables: TransitionOrderToStateMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .TransitionOrderToState(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.transitionOrderToState,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  /**
   * Transitions to ArrangingPayment if needed, and adds a payment to the order.
   * If anything fails, it will try to transition back into AddingItems
   * @param variables
   */
  async function addPaymentToOrder(
    variables: AddPaymentToOrderMutationVariables,
  ): Promise<void> {
    loading.value = true;
    try {
      if (activeOrder.value?.state !== "ArrangingPayment") {
        await transitionOrderTostate({ state: "ArrangingPayment" });
        loading.value = true; // Set loading again
      }
      const result = await vendureClient.AddPaymentToOrder(variables);
      const activeOrderResult = throwIfErrorResult<OrderFieldsFragment>(
        result.data.addPaymentToOrder,
      );
      activeOrder.value = activeOrderResult;
    } catch (err: any) {
      activeOrderError.value = {
        message: err.message,
        errorCode: err.code,
      };
      await transitionOrderTostate({ state: "AddingItems" });
    } finally {
      loading.value = false;
    }
    await vendureClient
      .AddPaymentToOrder(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(result.data.addPaymentToOrder),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function setOrderCustomFields(
    variables: SetOrderCustomFieldsMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .SetOrderCustomFields(variables)
      .then((result) =>
        throwIfErrorResult<OrderFieldsFragment>(
          result.data.setOrderCustomFields,
        ),
      )
      .then((result) => (activeOrder.value = result))
      .catch(
        (err) =>
          (activeOrderError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  // Populate active order on mount
  onMounted(async () => {
    await getActiveOrder();
  });

  return {
    /**
     * Reactive active order object. The primary state of this composable.
     * Is updated based on the mutations you do through this composable.
     */
    activeOrder,
    // Supportive states
    error: activeOrderError,
    loading,
    // Queries and Mutations
    getActiveOrder,
    addItemToOrder,
    adjustOrderLine,
    removeOrderLine,
    removeAllOrderLines,
    applyCouponCode,
    removeCouponCode,
    setCustomerForOrder,
    setOrderShippingAddress,
    setOrderBillingAddress,
    setOrderShippingMethod,
    transitionOrderTostate,
    setOrderCustomFields,
    addPaymentToOrder,
  };
}
