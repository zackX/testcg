import { PluginCommonModule, VendurePlugin } from "@vendure/core";
import { DraftOrderResolver } from "./draft-order-resolver";
import { schemaExtension } from "./api-extensions";

/**
 * This plugin adds the ability to pay for an order by code, so non-active orders
 * This is needed for draft orders, so that a customer can pay for any order that is in ArranginPayment
 */
@VendurePlugin({
  imports: [PluginCommonModule],
  shopApiExtensions: {
    resolvers: [DraftOrderResolver],
    schema: schemaExtension,
  },
})
export class DraftOrderPlugin {}
