import { SdkFunctionWrapper } from "./generated";

const LOCALSTORAGE_TOKEN = "vendure-auth-token";

/**
 * Get existing token from local storage
 */
function getAuthHeader(): Record<string, string> | undefined {
  if (!import.meta.env.SSR) {
    const token = window?.localStorage.getItem(LOCALSTORAGE_TOKEN);
    if (!token) {
      return;
    }
    return { Authorization: `Bearer ${token}` };
  }
}

/**
 *
 * Store token from response, if received, in local storage
 */
function storeAuthToken(response: any): void {
  if (!import.meta.env.SSR) {
    const authToken = response?.headers.get("vendure-auth-token");
    if (authToken) {
      window?.localStorage.setItem(LOCALSTORAGE_TOKEN, authToken);
    }
  }
}

/**
 * Graphql-request Wrapper to handle Vendure auth and channel headers
 */
export const vendureRequestWrapper: SdkFunctionWrapper = async (action) => {
  // Make sure we send auth token in request
  const headers = getAuthHeader() ?? {};
  headers["vendure-token"] = import.meta.env.PUBLIC_VENDURE_CHANNEL_TOKEN;
  const result = await action(headers);
  storeAuthToken(result);
  return result;
};
