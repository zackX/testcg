import {
  CustomOrderLineFields,
  EntityHydrator,
  Injector,
  Logger,
  Order,
  OrderItemPriceCalculationStrategy,
  PriceCalculationResult,
  ProductVariant,
  RequestContext,
  UserInputError,
} from "@vendure/core";
import { DefaultOrderItemPriceCalculationStrategy } from "@vendure/core/dist/config/order/default-order-item-price-calculation-strategy";
import { loggerCtx } from "../constants";
import { SelectedConfiguratorOption } from "../ui/generated/generated-graphql-types";
import {
  ConfiguratorService,
  isConfigurableProduct,
} from "./configurator-service";
import { TradeCustomerService } from "./trade-customer-service";

export class ConfiguratorOrderItemStrategy
  implements OrderItemPriceCalculationStrategy
{
  private configuratorService!: ConfiguratorService;
  private tradeCustomerService!: TradeCustomerService;
  private entityHydrator!: EntityHydrator;
  private defaultStrategy!: DefaultOrderItemPriceCalculationStrategy;

  async calculateUnitPrice(
    ctx: RequestContext,
    productVariant: ProductVariant,
    orderLineCustomFields: CustomOrderLineFields,
    order: Order,
    quantity: number,
  ): Promise<PriceCalculationResult> {
    // FIXME: This is a temporary solution untill we can set `public: false` on the custom field - https://github.com/vendure-ecommerce/vendure/issues/2750
    if (orderLineCustomFields.customUnitPriceExVAT && ctx.apiType !== "shop") {
      return {
        price: Math.round(orderLineCustomFields.customUnitPriceExVAT),
        priceIncludesTax: ctx.channel.pricesIncludeTax,
      };
    }
    const [isTradeCustomer] = await Promise.all([
      this.tradeCustomerService.isTradeCustomer(ctx, order),
      this.entityHydrator.hydrate(ctx, productVariant, {
        relations: ["product"],
      }),
    ]);
    if (!isConfigurableProduct(productVariant.product)) {
      // Not a configurator product, use default calculation
      return await this.defaultStrategy.calculateUnitPrice(ctx, productVariant);
    }
    Logger.info(
      `Calculating unit price for Configurator product ${productVariant.product.slug}`,
      loggerCtx,
    );
    const selectedOptions: SelectedConfiguratorOption[] = [];
    let index: number = 0;
    try {
      // Parse each selected option, because it's a stringified JSON object
      orderLineCustomFields.selectedConfiguratorOptions?.forEach(
        (optionString, i) => {
          index = i;
          selectedOptions.push(JSON.parse(optionString));
        },
      );
    } catch (e: any) {
      throw new UserInputError(
        `Could not parse orderLine.selectedConfiguratorOptions[${index}]: ${e?.message}`,
      );
    }
    const price = await this.configuratorService.calculate(
      ctx,
      productVariant.id,
      quantity,
      selectedOptions,
    );
    // Decide if trade customer or not
    return {
      price: Math.round(isTradeCustomer ? price.trade : price.retail),
      priceIncludesTax: false,
    };
  }

  init(injector: Injector): void {
    this.defaultStrategy = new DefaultOrderItemPriceCalculationStrategy();
    this.configuratorService = injector.get(ConfiguratorService);
    this.entityHydrator = injector.get(EntityHydrator);
    this.tradeCustomerService = injector.get(TradeCustomerService);
  }
}
