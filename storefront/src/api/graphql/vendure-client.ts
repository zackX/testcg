import { GraphQLClient } from "graphql-request";
import { getSdk } from "./generated";
import { vendureRequestWrapper } from "./vendure-request-wrapper";

const client = new GraphQLClient((import.meta as any).env.PUBLIC_VENDURE_HOST);
export const vendureClient = getSdk(client, vendureRequestWrapper);
