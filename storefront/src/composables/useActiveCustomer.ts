import { onMounted, ref } from "vue";
import { vendureClient } from "../api/graphql/vendure-client.ts";
import { throwIfErrorResult } from "../api/util/catalog-util.ts";
import { activeCustomer } from "../store.ts";
import {
  LoginMutationVariables,
  ErrorResult,
  UpdateCustomerMutationVariables,
} from "../api/graphql/generated.ts";

/**
 * Everything you need to manage the currently logged in customer
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function useActiveCustomer() {
  const loading = ref(true);
  const activeCustomerError = ref<ErrorResult | undefined>(undefined);
  const loginError = ref<ErrorResult | undefined>(undefined);

  async function getActiveCustomer(): Promise<void> {
    loading.value = true;
    await vendureClient
      .ActiveCustomer()
      .then((result) => throwIfErrorResult(result.data.activeCustomer))
      .then((result) => (activeCustomer.value = result))
      .catch(
        (err) =>
          (activeCustomerError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function login(variables: LoginMutationVariables): Promise<void> {
    loading.value = true;
    await vendureClient
      .Login(variables)
      .then((result) => throwIfErrorResult(result.data.login))
      // Fetch activeCustomer after login
      .then(async (result) => {
        await getActiveCustomer();
      })
      .catch(
        (err) =>
          (loginError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function updateCustomer(
    variables: UpdateCustomerMutationVariables,
  ): Promise<void> {
    loading.value = true;
    await vendureClient
      .UpdateCustomer(variables)
      .then((result) => throwIfErrorResult(result.data.updateCustomer))
      .then((result) => (activeCustomer.value = result))
      .catch(
        (err) =>
          (activeCustomerError.value = {
            message: err.message,
            errorCode: err.code,
          }),
      )
      .finally(() => (loading.value = false));
  }

  async function logout(): Promise<void> {
    loading.value = true;
    await vendureClient
      .Logout()
      .then((result) => throwIfErrorResult(result.data.logout))
      .then(() => (activeCustomer.value = null))
      .then(() => (window.location.href = "/"))
      .finally(() => (loading.value = false));
  }

  // Populate active customer on mount
  onMounted(async () => {
    await getActiveCustomer();
  });

  return {
    // Primary state
    activeCustomer,
    // Supportive states
    loginError,
    activeCustomerError,
    loading,
    // Queries and Mutations
    updateCustomer,
    getActiveCustomer,
    login,
    logout,
  };
}
