import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { Order } from "@vendure/core";
import { ShippingMethodQuote } from "@vendure/common/lib/generated-types";
import dayjs from "dayjs";
import {
  getEstimatedDeliveryDateForTurnaroundShippingMethod,
  getFallbackEstimatedDeliveryDate,
} from "../util/get-estimated-delivery-date";
import { isTurnaroundMethod } from "../util/turnaround";

export class EstimatedDeliveryDateResolver {
  @ResolveField("estimatedDeliveryDate")
  @Resolver("Order")
  estimatedOrderDeliveryDate(@Parent() order: Order): Date {
    if (order.customFields?.shippingDate) {
      // If a shipping date is set, assume delivery will be 1 day later
      return dayjs(order.customFields.shippingDate).add(1, "day").toDate();
    }
    const turnaroundMethod = order.shippingLines?.find((line) =>
      isTurnaroundMethod(line.shippingMethod),
    )?.shippingMethod;
    if (!turnaroundMethod) {
      return getFallbackEstimatedDeliveryDate();
    }
    return getEstimatedDeliveryDateForTurnaroundShippingMethod(
      turnaroundMethod,
    );
  }

  @ResolveField("estimatedDeliveryDate")
  @Resolver("ShippingMethodQuote")
  estimatedShippingDeliveryDate(
    @Parent() shippingQuote: ShippingMethodQuote,
  ): Date | undefined {
    if (!isTurnaroundMethod(shippingQuote)) {
      // Only turnaround methods have an estimated delivery date
      return undefined;
    }
    return getEstimatedDeliveryDateForTurnaroundShippingMethod(shippingQuote);
  }
}
