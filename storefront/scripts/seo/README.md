# SEO site migration

Find urls that are on the old site that return a 404 on the new site.

1. Create the folder `data` in `storefront/scripts/seo`. This will hold the fetched URL's
2. Run `yarn fetch https://colourgraphics.com`. This will save a list of paths of the old site in `colourgraphics_com.csv`
3. Run `yarn check-urls https://colour-graphics.netlify.app colourgraphics_com.csv`. This will check all the
   paths from the CSV file and write to a new file containing all paths that return a non successful http status on the
   new site. `/feed/,/product-tag/` are optional excluded URL's.
