import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import {
  Asset,
  FormInputComponent,
  Permission,
  SharedModule,
  AssetChange,
} from "@vendure/admin-ui/core";
import { RelationCustomFieldConfig } from "@vendure/core";

@Component({
  standalone: true,
  imports: [SharedModule],
  template: `
    <vdr-assets
      [assets]="documents"
      [featuredAsset]="documents?.[0]"
      [updatePermissions]="updatePermissions"
      (change)="documentsChanged($event)"
    ></vdr-assets>
  `,
})
export class SelectDocumentsComponent
  implements FormInputComponent<RelationCustomFieldConfig>, OnInit
{
  isListInput?: boolean | undefined = true;
  readonly: boolean;
  config: RelationCustomFieldConfig;
  documents: Asset[];
  formControl: FormControl<Asset[]>;
  public readonly updatePermissions = [
    Permission.UpdateCatalog,
    Permission.UpdateProduct,
  ];

  ngOnInit(): void {
    if (this.formControl.value) {
      this.documents = this.formControl.value;
    }
  }

  documentsChanged(event: AssetChange): void {
    this.formControl.setValue(event.assets);
    this.formControl.markAsDirty();
  }
}
