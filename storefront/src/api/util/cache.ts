/**
 * A simple cache class that prevents concurrent loading of data
 */
export class Cache<T> {
  /**
   * Cached data
   */
  private cache: T | undefined;

  /**
   * Prevent concurrent loading of data
   */
  private readonly cachePopulation: Promise<void> | undefined;

  constructor(cachePopulateFn: () => Promise<T>) {
    this.cachePopulation = cachePopulateFn().then((data) => {
      this.cache = data;
    });
  }

  async get(): Promise<T> {
    await this.cachePopulation;
    if (!this.cache) {
      throw new Error("Cache not populated!");
    }
    return this.cache;
  }
}
