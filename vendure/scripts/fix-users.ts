import { Customer, User, bootstrap } from "@vendure/core";
import { DataSource } from "typeorm";

/**
 * Sets user.identifier of all customers to customer.emailaddress.
 * Fix for wrongfully migrated customers: Identifiers where not equal to the email address.
 *
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config({ path: process.env.ENV_FILE });
// eslint-disable-next-line @typescript-eslint/no-floating-promises
import("../src/vendure-config").then(async ({ config }) => {
  console.log("Populating primary collections");
  const app = await bootstrap(config);

  let userCount = 0;
  const ds = await app.get(DataSource);
  let hasMore = true;
  let skip = 0;
  const take = 100;
  while (hasMore) {
    const customers = await ds
      .getRepository(Customer)
      .createQueryBuilder("customer")
      .where("userId IS NOT NULL")
      .take(take)
      .skip(skip)
      .getRawMany();
    if (customers.length === 0) {
      hasMore = false;
    }
    skip += take;
    await Promise.all(
      customers.map(
        async ({ customer_userId: userId, customer_emailAddress: email }) => {
          const result = await ds
            .getRepository(User)
            .update(userId, { identifier: email });
          if (result.affected) {
            // console.log(`Updated user ${userId} with email ${email}`);
          } else {
            console.log(`Failed to update user ${userId} with email ${email}`);
          }
          userCount++;
        },
      ),
    );

    console.log(`Fixed users ${skip} - ${skip + take}.`);
  }

  console.log(`Fixed a total of ${userCount} users.`);
  process.exit(0);
});
