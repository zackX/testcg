const baseUrl = `https://api.addressy.com`;
const key = import.meta.env.PUBLIC_LOQATE_KEY;

export interface AddressLookupResult {
  Id: string;
  Type: "Address" | "Street" | "Postcode";
  Text: string;
  Highlight: string;
  Description: string;
}

export interface FullAddressResult {
  Id: string;
  DomesticId: string;
  Language: string;
  LanguageAlternatives: string;
  Department: string;
  Company: string;
  SubBuilding: string;
  BuildingNumber: string;
  BuildingName: string;
  SecondaryStreet: string;
  Street: string;
  Block: string;
  Neighbourhood: string;
  District: string;
  City: string;
  Line1: string;
  Line2: string;
  Line3: string;
  Line4: string;
  Line5: string;
  AdminAreaName: string;
  AdminAreaCode: string;
  Province: string;
  ProvinceName: string;
  ProvinceCode: string;
  PostalCode: string;
  CountryName: string;
  CountryIso2: string;
  CountryIso3: string;
  CountryIsoNumber: string;
  SortingNumber1: string;
  SortingNumber2: string;
  Barcode: string;
  POBoxNumber: string;
  Label: string;
  Type: string;
  DataLevel: string;
}

/**
 * Find addresses by search query. Returns only type Address results.
 * Based on https://www.loqate.com/developers/api/Capture/Interactive/Find/1.1/
 */
export async function findAddresses(
  searchQuery: string,
  /**
   * This can be used to drill down into Postcodes or Streets.
   */
  containerId?: string,
): Promise<AddressLookupResult[]> {
  let url = `${baseUrl}/Capture/Interactive/Find/v1.10/json3.ws?Key=${key}&Countries=GB&Limit=20&Text=${searchQuery}`;
  if (containerId) {
    url += `&Container=${containerId}`;
  }
  const result = await fetch(url);
  if (!result.ok) {
    const error = await result.text();
    console.error(error);
    return [];
  }
  const data = await result.json();
  if (!data.Items) {
    console.error(data);
    return [];
  }
  if (data.Items?.[0]?.Error) {
    console.error(data.Items[0]);
    return [];
  }
  const items = data.Items as AddressLookupResult[];
  if (requiresDrillDown(items[0])) {
    // Resolve to underlying addresses
    const additionalResults = await findAddresses(searchQuery, items[0].Id);
    items.shift(); // Remove the first item, because we have resolved that result into multiple addresses
    items.unshift(...additionalResults); // Add the additional results to the front of the array
  }
  return items.filter((item) => item.Type === "Address");
}

/**
 * Get full address based on Id from findAddresses.
 * Based on https://www.loqate.com/developers/api/Capture/Interactive/Retrieve/1.2/
 */
export async function retrieveAddress(
  id: string,
): Promise<FullAddressResult | undefined> {
  const url = `${baseUrl}/Capture/Interactive/Retrieve/v1.2/json3.ws?Key=${key}&Id=${id}`;
  const result = await fetch(url);
  if (!result.ok) {
    const error = await result.text();
    console.error(error);
    return undefined;
  }
  const data = await result.json();
  if (!data.Items) {
    console.error(data);
    return undefined;
  }
  if (data.Items?.[0]?.Error) {
    console.error(data.Items[0]);
    return undefined;
  }
  return data.Items[0] as FullAddressResult;
}

/**
 * Checks if these result types require another API call to resolve to an Address.
 * This is the case for Postcodes and Streets, because Postcodes can hold multiple addresses. Same for Streets
 */
export function requiresDrillDown(result?: AddressLookupResult): boolean {
  return result?.Type === "Postcode" || result?.Type === "Street";
}
