import type {
  CollectionFieldsFragment,
  ProductFieldsFragment,
} from "./api/graphql/generated";
import type { FullPathBreadcrumb } from "./api/util/catalog-util";
import { SearchItem } from "./api/util/search-util";

/**
 * These are the types that are used in the storefront.
 */

export interface Product extends ProductFieldsFragment {
  path: string;
  breadcrumbs: FullPathBreadcrumb[];
  retailPriceWithTax: number;
  tradePriceWithTax: number;
}

export interface Collection extends CollectionFieldsFragment {
  path: string;
  breadcrumbs: FullPathBreadcrumb[];
  children?: Collection[] | null;
}

export interface CGSearchItem extends SearchItem {
  tradePrice: number;
  retailPrice: number;
}

export interface Notification {
  message: string;
  error?: boolean;
}
