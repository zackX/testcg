import {
  Injector,
  Logger,
  ProductVariant,
  RequestContext,
  UserInputError,
  roundMoney,
} from "@vendure/core";
import { loggerCtx } from "../constants";
import {
  ConfiguratorCalculation,
  ConfiguratorCalculationResult,
  SelectedNumberOption,
  SelectedOption,
} from "../types";
import {
  ConfiguratorOptionGroupCheckBox,
  ConfiguratorOptionGroupRadio,
  ConfiguratorSelectableOption,
} from "../ui/generated/generated-graphql-types";

// This is the Colour Graphics specific configurator calculation

/**
 * Price per square meter. The more squaremeters, the lower the price.
 */
export interface Price {
  min: number;
  price: number;
}

export interface SelectableOptionWithPrice
  extends ConfiguratorSelectableOption {
  customFields: {
    prices: Price[];
    hidden: boolean;
    minOrderPrice: number;
    minUnitPrice: number;
    optionType: "sqm" | "perimeter" | "static" | "width" | "height";
    retailIncrease: number;
    weightGramsPerSquareMeter: number;
  };
}

export interface RadioOptionGroup extends ConfiguratorOptionGroupRadio {
  options: SelectableOptionWithPrice[];
}

export interface CheckBoxOptionGroup extends ConfiguratorOptionGroupCheckBox {
  options: SelectableOptionWithPrice[];
}

function getProductVariantWidthAndHeight(selectedOptions: SelectedOption[]): {
  height?: number;
  width?: number;
} {
  const width = (
    selectedOptions.find(
      (option) => option.optionGroupId === "width",
    ) as SelectedNumberOption
  )?.value;
  const height = (
    selectedOptions.find(
      (option) => option.optionGroupId === "height",
    ) as SelectedNumberOption
  )?.value;
  return { height, width };
}

export function getSquareMeter(width: number, height: number): number {
  return (width * height) / 1000000;
}

export function getPerimeter(width: number, height: number): number {
  return (2 * (width + height)) / 1000;
}

/**
 * Get the lowest price per sq meter allowed for given sqMeter
 */
export function getLowestPrice(benchmark: number, prices: Price[]): number {
  let lowestPrice = prices[0]?.price ?? 0; // Price is 0, when no price per sqMeter is given, which is allowed
  prices.forEach((pricePerOptionType) => {
    if (
      benchmark >= pricePerOptionType.min &&
      pricePerOptionType.price < lowestPrice
    ) {
      lowestPrice = pricePerOptionType.price;
    }
  });
  return lowestPrice;
}

export const colourGraphicsCalculation: ConfiguratorCalculation = {
  id: "pricing",
  name: "Pricing",
  calculateFn: calculateVariantPrice,
};

async function calculateVariantPrice(
  ctx: RequestContext,
  injector: Injector,
  productVariant: ProductVariant,
  quantity: number,
  selectedOptions: SelectedOption[],
): Promise<ConfiguratorCalculationResult> {
  // All options
  let tradePrice = 0;
  let retailPrice = 0;
  // Different types of options
  const staticOptions = selectedOptions.filter(
    (option) =>
      (option as SelectableOptionWithPrice).customFields?.optionType ===
      "static",
  ) as SelectableOptionWithPrice[];
  const sqmOptions = selectedOptions.filter(
    (option) =>
      (option as SelectableOptionWithPrice).customFields?.optionType === "sqm",
  ) as SelectableOptionWithPrice[];
  const perimeterOptions = selectedOptions.filter(
    (option) =>
      (option as SelectableOptionWithPrice).customFields?.optionType ===
      "perimeter",
  ) as SelectableOptionWithPrice[];
  const widthOptions = selectedOptions.filter(
    (option) =>
      (option as SelectableOptionWithPrice).customFields?.optionType ===
      "width",
  ) as SelectableOptionWithPrice[];
  const heightOptions = selectedOptions.filter(
    (option) =>
      (option as SelectableOptionWithPrice).customFields?.optionType ===
      "height",
  ) as SelectableOptionWithPrice[];
  const { width, height } = getProductVariantWidthAndHeight(selectedOptions);
  if (widthOptions?.length && !width) {
    throw new UserInputError(
      `Product ${productVariant.name} has options that are calculated based on width, but no Width value exists`,
    );
  }
  if (heightOptions?.length && !height) {
    throw new UserInputError(
      `Product ${productVariant.name} has options that are calculated based on height, but no Height value exists`,
    );
  }
  const hasPerimeterOrSqmOptions =
    perimeterOptions.length > 0 || sqmOptions.length > 0;
  // Sqm and Perimeter always require a width and height option, so if those options exist, but no width or height is given, throw an error
  if (hasPerimeterOrSqmOptions && (!width || !height)) {
    throw new UserInputError(
      `Product ${productVariant.name} has options that are calculated based on Sqm or Perimeter, but no Height or Width options exist to calculate the Sqm or Perimeter`,
    );
  }
  // Calculate static based pricing
  staticOptions.forEach((staticOption) => {
    const staticPrice = staticOption.customFields.prices[0].price;
    const retailFactor = getRetailFactor(
      staticOption.customFields.retailIncrease,
    ); // E.g. 1.1
    if (staticPrice === undefined) {
      Logger.error(
        `No price found for option ${staticOption.name}. Can not calculate price for ${productVariant.name} correctly`,
        loggerCtx,
      );
    }
    tradePrice += staticPrice || 0; // For static prices we only have 1 price
    retailPrice += staticPrice * retailFactor;
  });
  if (!sqmOptions.length && !perimeterOptions.length) {
    // This means we only have a static price
    return {
      trade: tradePrice,
      retail: retailPrice,
    };
  }
  if (!width || !height) {
    // This should never happen, because we check for this above with `hasPerimeterOrSqmOptions`. But we do it anyway to please TS
    throw new UserInputError(
      `No width or height given for product ${productVariant.name}`,
    );
  }
  // Calculate Perimeter based pricing
  const perimeter = getPerimeter(width, height);
  perimeterOptions.forEach((perimeterOption) => {
    const lowestPrice = getLowestPrice(
      perimeter,
      perimeterOption.customFields.prices,
    );
    const minUnitPrice = perimeterOption?.customFields?.minUnitPrice ?? 0;
    const retailFactor = getRetailFactor(
      perimeterOption.customFields.retailIncrease,
    ); // E.g. 1.1
    const totalOptionPrice = lowestPrice * perimeter;
    if (minUnitPrice > totalOptionPrice) {
      // Smaller than unit price, so we should use unit price
      tradePrice += minUnitPrice;
      retailPrice += minUnitPrice * retailFactor;
    } else {
      tradePrice += totalOptionPrice;
      retailPrice += totalOptionPrice * retailFactor;
    }
  });
  // Calculate sqMeter based pricing
  const sqMeter = getSquareMeter(width, height);
  sqmOptions.forEach((sqmOption) => {
    const lowestPrice = getLowestPrice(sqMeter, sqmOption.customFields.prices);
    const minUnitPrice = sqmOption?.customFields?.minUnitPrice ?? 0;
    const retailFactor = getRetailFactor(sqmOption.customFields.retailIncrease); // E.g. 1.1
    const totalOptionPrice = lowestPrice * sqMeter;
    if (minUnitPrice > totalOptionPrice) {
      tradePrice += minUnitPrice;
      retailPrice += minUnitPrice * retailFactor;
    } else {
      tradePrice += totalOptionPrice;
      retailPrice += totalOptionPrice * retailFactor;
    }
  });
  // Calculate width based pricing
  widthOptions.forEach((widthOption) => {
    const lowestPrice = getLowestPrice(width, widthOption.customFields.prices);
    const minUnitPrice = widthOption?.customFields?.minUnitPrice ?? 0;
    const retailFactor = getRetailFactor(
      widthOption.customFields.retailIncrease,
    ); // E.g. 1.1
    const totalOptionPrice = lowestPrice * (width / 1000);
    if (minUnitPrice > totalOptionPrice) {
      tradePrice += minUnitPrice;
      retailPrice += minUnitPrice * retailFactor;
    } else {
      tradePrice += totalOptionPrice;
      retailPrice += totalOptionPrice * retailFactor;
    }
  });
  // Calculate height based pricing
  heightOptions.forEach((heightOption) => {
    const lowestPrice = getLowestPrice(
      height,
      heightOption.customFields.prices,
    );
    const minUnitPrice = heightOption?.customFields?.minUnitPrice ?? 0;
    const retailFactor = getRetailFactor(
      heightOption.customFields.retailIncrease,
    ); // E.g. 1.1
    const totalOptionPrice = lowestPrice * (height / 1000);
    if (minUnitPrice > totalOptionPrice) {
      tradePrice += minUnitPrice;
      retailPrice += minUnitPrice * retailFactor;
    } else {
      tradePrice += totalOptionPrice;
      retailPrice += totalOptionPrice * retailFactor;
    }
  });
  const minOrderPrice = getMinOrderLinePrice(selectedOptions);
  if (tradePrice * quantity < minOrderPrice) {
    // Use the min order price as the price per unit
    const unitPrice = minOrderPrice / quantity;
    tradePrice = roundMoney(unitPrice);
  }

  if (retailPrice * quantity < minOrderPrice) {
    // Use the min order price as the price per unit
    const unitPrice = minOrderPrice / quantity;
    retailPrice = roundMoney(unitPrice);
  }

  return {
    trade: tradePrice,
    retail: retailPrice,
  };
}

/**
 * Gets the highest minimum order line price from the selected options.
 */
export function getMinOrderLinePrice(
  selectedOptions: SelectedOption[],
): number {
  let minOrderPrice = 0;
  for (const option of selectedOptions) {
    if (
      (option as SelectableOptionWithPrice).customFields?.minOrderPrice >
      minOrderPrice
    ) {
      minOrderPrice = (option as SelectableOptionWithPrice).customFields
        ?.minOrderPrice;
    }
  }
  return minOrderPrice;
}

/**
 * Transforms the given retail increase to a factor. E.g. 10% becomes 1.1
 * Defaults to 1
 */
export function getRetailFactor(retailIncrease: number = 0): number {
  return 1 + retailIncrease / 100;
}
