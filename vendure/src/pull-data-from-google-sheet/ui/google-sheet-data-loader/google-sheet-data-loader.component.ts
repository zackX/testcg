import {
  Component,
  Renderer2,
  Inject,
  ChangeDetectorRef,
  OnInit,
} from "@angular/core";
import {
  DeactivateAware,
  NotificationService,
  DataService,
} from "@vendure/admin-ui/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DOCUMENT } from "@angular/common";
import { GET_ACTIVE_GOOGLE_SHEET_META_DATA, READ_GOOGLE_SHEET } from "../query";
import { catchError, of } from "rxjs";
@Component({
  selector: "google-sheet-data-loader",
  templateUrl: "./google-sheet-data-loader.component.html",
  styleUrls: ["./google-sheet-data-loader.component.scss"],
})
export class GoogleSheetDataLoaderComponent implements DeactivateAware, OnInit {
  dataFormGroup: FormGroup = this.fb.group({
    url: ["", Validators.required],
    sheetName: ["", Validators.required],
  });

  loading = false;

  constructor(
    private readonly fb: FormBuilder,
    private readonly dataService: DataService,
    private readonly ns: NotificationService,
    private readonly cdr: ChangeDetectorRef,
    private readonly _renderer2: Renderer2,
    @Inject(DOCUMENT) private readonly _document: Document,
  ) {}

  ngOnInit(): void {
    this.dataService
      .query(GET_ACTIVE_GOOGLE_SHEET_META_DATA)
      .single$.subscribe((data: any) => {
        if (
          data?.activeGoogleSheetMetaData?.url &&
          data?.activeGoogleSheetMetaData?.sheetName
        ) {
          this.dataFormGroup.setValue({
            url: data.activeGoogleSheetMetaData.url,
            sheetName: data.activeGoogleSheetMetaData.sheetName,
          });
        }
      });
  }

  canDeactivate(): boolean {
    return true;
  }

  pullData(): void {
    this.loading = true;
    this.cdr.detectChanges();
    this.dataService
      .mutate(READ_GOOGLE_SHEET, {
        sheetInput: {
          sheetName: this.dataFormGroup.get("sheetName")?.value,
          url: this.dataFormGroup.get("url")?.value,
        },
      })
      .pipe(catchError(() => of({})))
      .subscribe((data: any) => {
        if (data?.readGoogleSheet?.success) {
          this.ns.success("Configureable products updated successfully");
        } else {
          this.ns.error("Unable to read google sheet");
        }
        this.loading = false;
        this.cdr.detectChanges();
      });
  }
}
