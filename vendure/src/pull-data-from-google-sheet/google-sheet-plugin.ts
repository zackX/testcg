import { PluginCommonModule, Type, VendurePlugin } from "@vendure/core";
import { AdminUiExtension } from "@vendure/ui-devkit/compiler";
import path from "path";
import { GoogleSheetService } from "./api/google-sheet.service";
import { PullDataFromGoogleSheetResolver } from "./api/api.resolver";
import { adminApiExtention } from "./api/schema";
import { PLUGIN_INIT_OPTIONS } from "./constants";
import { GoogleSheetMetaData } from "./api/entities/google-sheet-meta-data.entity";
import { PullDataFromGoogleSheetPluginConfig } from "./types";
@VendurePlugin({
  imports: [PluginCommonModule],
  entities: [GoogleSheetMetaData],
  providers: [
    GoogleSheetService,
    {
      provide: PLUGIN_INIT_OPTIONS,
      useFactory: () => PullDataFromGoogleSheetPlugin.config,
    },
  ],
  adminApiExtensions: {
    resolvers: [PullDataFromGoogleSheetResolver],
    schema: adminApiExtention,
  },
  configuration: (config) => {
    return config;
  },
})
export class PullDataFromGoogleSheetPlugin {
  static config: PullDataFromGoogleSheetPluginConfig;

  static init(
    config: PullDataFromGoogleSheetPluginConfig,
  ): Type<PullDataFromGoogleSheetPlugin> {
    PullDataFromGoogleSheetPlugin.config = {
      ...config,
    };
    return this;
  }

  static uiExtensions: AdminUiExtension = {
    extensionPath: path.join(__dirname, "ui"),

    ngModules: [
      {
        type: "shared" as const,
        ngModuleFileName: "ui-extension.module.ts",
        ngModuleName: "UiExtensionModule",
      },
      {
        type: "lazy" as const,
        route: "settings",
        ngModuleFileName: "ui-lazy.module.ts",
        ngModuleName: "UiLazyModule",
      },
    ],
  };
}
