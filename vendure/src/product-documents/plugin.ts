import {
  Asset,
  LanguageCode,
  PluginCommonModule,
  VendurePlugin,
} from "@vendure/core";
import { AdminUiExtension } from "@vendure/ui-devkit/compiler";
import path from "path";

@VendurePlugin({
  imports: [PluginCommonModule],
  configuration: (config) => {
    config.customFields.Product.push({
      name: "documents",
      list: true,
      type: "relation",
      entity: Asset,
      ui: {
        component: "select-documents",
      },
      label: [{ languageCode: LanguageCode.en, value: "Documents" }],
    });
    return config;
  },
})
export class ProductDocumentsPlugin {
  public static uiExtensions: AdminUiExtension = {
    extensionPath: path.join(__dirname, "ui"),
    providers: ["providers.ts"],
  };
}
