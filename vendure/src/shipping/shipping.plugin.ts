import { PluginCommonModule, VendurePlugin } from "@vendure/core";
import { shippingDateCustomField } from "./api/custom-fields";
import { adminApiExtensions, commonApiExtensions } from "./api/schema";
import { turnaroundMethodValidationProcess } from "./api/turnaround-method-validation-process";
import { turnaroundShippingCalculator } from "./api/calculators/turnaround-shipping-calculator";
import { EstimatedDeliveryDateResolver } from "./api/turnaround-shipping.resolver";
import path from "path";
import { AdminUiExtension } from "@vendure/ui-devkit/compiler";
import { AdminSetShippingMethodsResolver } from "./api/admin-set-shipping-method.resolver";
import { populateShippingDateOrderProcess } from "./api/populate-shipping-date-order-process";

@VendurePlugin({
  imports: [PluginCommonModule],
  shopApiExtensions: {
    schema: commonApiExtensions,
    resolvers: [EstimatedDeliveryDateResolver],
  },
  adminApiExtensions: {
    schema: adminApiExtensions,
    resolvers: [EstimatedDeliveryDateResolver, AdminSetShippingMethodsResolver],
  },
  configuration: (config) => {
    config.shippingOptions.shippingCalculators.push(
      turnaroundShippingCalculator,
    );
    config.orderOptions.process.push(
      turnaroundMethodValidationProcess,
      populateShippingDateOrderProcess,
    );
    config.customFields.Order.push(shippingDateCustomField);
    return config;
  },
})
export class ShippingPlugin {
  public static uiExtensions: AdminUiExtension = {
    extensionPath: path.join(__dirname, "ui"),
    providers: ["providers.ts"],
  };
}
