import {
  EntityHydrator,
  Logger,
  OrderProcess,
  OrderState,
} from "@vendure/core";
import { getEstimatedShippingDate } from "../util/get-estimated-delivery-date";
import { getTurnaroundDays, isTurnaroundMethod } from "../util/turnaround";

let entityHydrator: EntityHydrator;

export const populateShippingDateOrderProcess: OrderProcess<OrderState> = {
  init: (injector) => {
    entityHydrator = injector.get(EntityHydrator);
  },
  onTransitionEnd: async (_, toState, data) => {
    if (toState !== "PaymentSettled") {
      return;
    }
    const { order, ctx } = data;
    try {
      await entityHydrator.hydrate(ctx, order, {
        relations: ["shippingLines", "shippingLines.shippingMethod"],
      });
      const turnaroundMethod = order.shippingLines.find((line) =>
        isTurnaroundMethod(line.shippingMethod),
      )?.shippingMethod;
      let turnaroundDays = 5;
      if (!turnaroundMethod) {
        Logger.error(
          `No turnaround method found set on order ${order.code}, using default 5 day turnaround.`,
          "ShippingPlugin",
        );
      } else {
        turnaroundDays = getTurnaroundDays(turnaroundMethod);
      }
      const estimatedShippingDate = getEstimatedShippingDate(
        order.orderPlacedAt ?? new Date(), // Use `now` if `orderPlacedAt` is not set yet (which happens sometimes)
        turnaroundDays,
      );
      order.customFields.shippingDate = estimatedShippingDate;
      Logger.info(`Set shippingDate for ${order.code}`, "ShippingPlugin");
    } catch (e) {
      Logger.error(
        `Failed to set shippingDate for ${order.code}`,
        "ShippingPlugin",
      );
    }
  },
};
