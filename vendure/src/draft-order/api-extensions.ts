import { gql } from "graphql-tag";

export const schemaExtension = gql`
  input MollieIntentForOrderInput {
    orderCode: String!
    paymentMethodCode: String!
    redirectUrl: String!
  }
  extend type Mutation {
    createMolliePaymentIntentForOrder(input: MollieIntentForOrderInput): String!
  }
`;
