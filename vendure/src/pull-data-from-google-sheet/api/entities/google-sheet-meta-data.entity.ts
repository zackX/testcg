import {
  VendureEntity,
  ChannelAware,
  Channel,
  DeepPartial,
} from "@vendure/core";
import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
@Entity()
export class GoogleSheetMetaData extends VendureEntity implements ChannelAware {
  public constructor(input?: DeepPartial<GoogleSheetMetaData>) {
    super(input);
  }

  @ManyToMany((type) => Channel)
  @JoinTable()
  channels: Channel[];

  @Column()
  url: string;

  @Column()
  sheetName: string;
}
