import { gql } from "graphql-tag";

// Needed for codegen
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const _scalar = gql`
  scalar Success
`;

export const adminApiExtention = gql`
  input GoogleSheetInput {
    url: String!
    sheetName: String!
  }
  type GoogleSheetMetaData {
    url: String!
    sheetName: String!
  }
  extend type Mutation {
    readGoogleSheet(sheetInput: GoogleSheetInput!): Success!
  }

  extend type Query {
    activeGoogleSheetMetaData: GoogleSheetMetaData
  }
`;
