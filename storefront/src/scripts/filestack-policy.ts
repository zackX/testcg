// This is the function that was used to generate the policy and signature to be passed to the remove function of filestack client
const getPolicyAndSignature = () => {
  const currentTime = new Date();
  const expiryTime = new Date(
    currentTime.getTime() + 60000 * 60 * 24 * 365 * 10,
  );
  const unixFormat = Math.floor(expiryTime.getTime() / 1000);

  const policy = {
    expiry: unixFormat,
    call: ["remove"],
  };
  const encodedPolicy = CryptoJS.enc.Base64.stringify(
    CryptoJS.enc.Utf8.parse(JSON.stringify(policy)),
  );

  // Replace the SECRET_KEY with the real secret key
  const signature = CryptoJS.HmacSHA256(encodedPolicy, "SECRET_KEY").toString(
    CryptoJS.enc.Hex,
  );

  return {
    policy: encodedPolicy,
    signature: signature,
  };
};
