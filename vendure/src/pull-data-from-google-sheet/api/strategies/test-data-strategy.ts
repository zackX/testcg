import { Injector, RequestContext } from "@vendure/core";
import { GoogleSheetDataStrategy } from "./google-sheet-data.strategy";

export class TestDataStretegy implements GoogleSheetDataStrategy {
  async handleSheetData(
    ctx: RequestContext,
    injector: Injector,
    sheetData: string[][],
  ): Promise<void> {
    console.log(sheetData);
  }
}
