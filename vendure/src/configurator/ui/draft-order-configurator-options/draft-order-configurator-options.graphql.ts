import { gql } from "graphql-tag";
import {
  DISCOUNT_FRAGMENT,
  ORDER_DETAIL_FRAGMENT,
  ORDER_LINE_FRAGMENT,
} from "@vendure/admin-ui/core";
export const CALCULATE_CONFIGURATOR_PRICE = gql`
  query CALCULATE_CONFIGURATOR_PRICE(
    $variantId: ID!
    $quantity: Int!
    $selectedOptions: [SelectedConfiguratorOption!]!
  ) {
    calculateConfiguratorPrice(
      variantId: $variantId
      quantity: $quantity
      selectedOptions: $selectedOptions
    ) {
      retail
      trade
    }
  }
`;
export const GET_ORDER = gql`
  query OrderWithActiveChannelQuery($id: ID!) {
    order(id: $id) {
      ...OrderDetail
      lines {
        ...OrderLine
        productVariant {
          id
          name
          currencyCode
          product {
            id
            name
            configuratorOptionGroups {
              ... on ConfiguratorOptionGroupRadio {
                id
                name
                options {
                  id
                  name
                  customFields
                }
              }
              ... on ConfiguratorOptionGroupCheckBox {
                id
                name
                options {
                  id
                  name
                  customFields
                }
              }
              ... on ConfiguratorOptionGroupNumber {
                id
                name
                min
                max
              }
            }
            isConfigurable
            customFields {
              configuratorOptionGroups
            }
          }
        }
        customFields {
          selectedConfiguratorOptions
          customerNote
        }
      }
    }
    activeChannel {
      defaultCurrencyCode
    }
  }
  ${DISCOUNT_FRAGMENT}
  ${ORDER_LINE_FRAGMENT}
  ${ORDER_DETAIL_FRAGMENT}
`;
export const ADJUST_ORDER_LINE = gql`
  mutation AdjustDraftOrderLine(
    $orderLineId: ID!
    $orderId: ID!
    $quantity: Int!
    $selectedConfiguratorOptions: [String]
    $additionalInformation: String
    $customUnitPriceExVAT: Int
  ) {
    adjustDraftOrderLine(
      orderId: $orderId
      input: {
        orderLineId: $orderLineId
        quantity: $quantity
        customFields: {
          selectedConfiguratorOptions: $selectedConfiguratorOptions
          customerNote: $additionalInformation
          customUnitPriceExVAT: $customUnitPriceExVAT
        }
      }
    ) {
      ... on Order {
        ...OrderDetail
      }
      ... on ErrorResult {
        message
      }
    }
  }
  ${ORDER_DETAIL_FRAGMENT}
`;
