import { OrderService, RequestContextService, bootstrap } from "@vendure/core";
import { PrimaryCollectionHelperService } from "@pinelab/vendure-plugin-primary-collection/dist/api/primary-collections-helper.service";

/**
 * This script resets ALL primary collections for products. Sets the first collection as primary collection.
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config({ path: process.env.ENV_FILE });
// eslint-disable-next-line @typescript-eslint/no-floating-promises
import("../src/vendure-config").then(async ({ config }) => {
  console.log("Populating primary collections");
  const app = await bootstrap(config);
  const orderService = await app.get(OrderService);
  const ctx = await app.get(RequestContextService).create({ apiType: "admin" });
  await orderService.updateCustomFields(ctx, "320", {
    shippingDate: new Date(),
  });
});
