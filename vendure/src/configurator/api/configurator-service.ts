import { Inject, Injectable } from "@nestjs/common";
import { ModuleRef } from "@nestjs/core";
import {
  ID,
  Injector,
  Product,
  ProductVariantService,
  RequestContext,
  UserInputError,
} from "@vendure/core";
import { ConfiguratorPluginOptions } from "../configurator-plugin";
import { PLUGIN_INIT_OPTIONS } from "../constants";
import {
  ConfiguratorPrice,
  SelectedConfiguratorOption,
} from "../ui/generated/generated-graphql-types";
import { getFullSelectedOptions } from "../util/get-full-selected-options";
import { parseConfiguratorOptionsOrFail } from "../util/parse-configurator-options";

export function isConfigurableProduct(product: Product): boolean {
  if (
    product.customFields.configuratorOptionGroups &&
    product.customFields.configuratorOptionGroups.length > 0
  ) {
    return true;
  }
  return false;
}

@Injectable()
export class ConfiguratorService {
  constructor(
    @Inject(PLUGIN_INIT_OPTIONS)
    private readonly options: ConfiguratorPluginOptions,
    private readonly productVariantService: ProductVariantService,
    private readonly moduleRef: ModuleRef,
  ) {}

  /**
   * Calculate the price for a product variant with the given selected options
   * based on the variants listPrice. This means that the price is inc. or ex tax
   * depending on your channel settings
   */
  async calculate(
    ctx: RequestContext,
    variantId: ID,
    quantity: number,
    selectedOptionInputs: SelectedConfiguratorOption[],
  ): Promise<ConfiguratorPrice> {
    const variantWithProduct = await this.productVariantService.findOne(
      ctx,
      variantId,
      ["product"],
    );
    if (!variantWithProduct) {
      throw new UserInputError(`Product variant ${variantId} doesn't exist`);
    }
    if (!this.options.calculation) {
      throw Error(`No calculation configured`);
    }
    const optionGroupsString =
      variantWithProduct.product.customFields.configuratorOptionGroups;
    if (!optionGroupsString) {
      throw Error(
        `No option groups configured for product ${variantWithProduct.product.slug}`,
      );
    }
    const optionGroups = parseConfiguratorOptionsOrFail(
      variantWithProduct.product,
    );
    const selectedOptions = getFullSelectedOptions(
      optionGroups,
      selectedOptionInputs,
    );
    const prices = await this.options.calculation.calculateFn(
      ctx,
      new Injector(this.moduleRef),
      variantWithProduct,
      quantity,
      selectedOptions,
    );
    const taxMultiplier = 1 + variantWithProduct.taxRateApplied.value / 100;
    return {
      trade: Math.round(prices.trade),
      tradeWithTax: Math.round(prices.trade * taxMultiplier),
      retail: Math.round(prices.retail),
      retailWithTax: Math.round(prices.retail * taxMultiplier),
    };
  }
}
