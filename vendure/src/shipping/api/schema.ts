import { gql } from "graphql-tag";

// Needed for codegen
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const _scalar = gql`
  scalar Money
`;
export const commonApiExtensions = gql`
  extend type Order {
    """
    An estimated delivery date for the order to be used in the checkout process.
    For definitive dates see order.customFields.shippingDate
    """
    estimatedDeliveryDate: DateTime!
  }

  extend type ShippingMethodQuote {
    """
    An estimated delivery date per shipping method.
    For definitive dates see order.customFields.shippingDate
    """
    estimatedDeliveryDate: DateTime
  }
`;

export const adminApiExtensions = gql`
  ${commonApiExtensions}
  extend type Mutation {
    setOrderShippingMethod(
      shippingMethodId: [ID!]!
      orderId: ID!
    ): SetOrderShippingMethodResult!
  }
`;
