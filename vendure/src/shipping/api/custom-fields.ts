import { CustomFieldConfig, LanguageCode } from "@vendure/core";

export const shippingDateCustomField: CustomFieldConfig = {
  name: "shippingDate",
  type: "datetime",
  readonly: false,
  public: true,
  internal: false,
  nullable: true,
  label: [
    {
      languageCode: LanguageCode.en,
      value: "Shipping Date",
    },
  ],
};
