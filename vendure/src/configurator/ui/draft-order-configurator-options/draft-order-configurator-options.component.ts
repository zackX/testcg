import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ElementRef,
} from "@angular/core";
import { UntypedFormGroup } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import {
  CustomDetailComponent,
  DataService,
  NotificationService,
  ModalService,
} from "@vendure/admin-ui/core";
import { Observable } from "rxjs";
import { ID } from "@vendure/common/lib/shared-types";
import { GET_ORDER } from "./draft-order-configurator-options.graphql";
import { DraftOrderLineConfiguratorModalComponent } from "../draft-order-line-configurator-modal/draft-order-line-configurator-modal.component";
import { NotConfigurableProductComponent } from "../not-configurable-product/not-configurable-product.component";
export type MeasurementUnit = "mm" | "cm" | "meters" | "in" | "ft";
@Component({
  selector: "draft-order-configurator-options",
  template: ``,
  changeDetection: ChangeDetectionStrategy.Default,
})
export class DraftOrderConfiguratorOptionsComponent
  implements CustomDetailComponent, OnInit
{
  entity$: Observable<any>;
  draftOrderLines: any[];
  currencyCode: string;
  configurableDraftOrderLines: any[];
  orderId: ID;

  selectedLine: any;
  detailForm: UntypedFormGroup;
  showConfigurationForm = false;
  modalIsShowing: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly ns: NotificationService,
    private readonly dataService: DataService,
    private readonly elementRef: ElementRef,
    private readonly modalService: ModalService,
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id");
    if (id) {
      this.dataService
        .query(GET_ORDER, { id })
        .stream$.subscribe((data: any) => {
          this.draftOrderLines = data.order.lines;
          this.configurableDraftOrderLines = this.draftOrderLines.filter(
            (l: any) => l?.productVariant?.product?.isConfigurable,
          );
          this.orderId = data.order.id;
          this.showConfigurationForm =
            data.order.state === "AddingItems" || data.order.state === "Draft";
          if (this.configurableDraftOrderLines?.length) {
            this.currencyCode = data.activeChannel.defaultCurrencyCode;
          }
          this.cdr.markForCheck();
          this.listenForClickEvents();
        });
    }
  }

  isOrderLineConfigurable(draftOrderLine: any): boolean {
    return draftOrderLine?.productVariant?.product?.isConfigurable;
  }

  listenForClickEvents(): void {
    const possibleOrderTableElement: Element =
      this.elementRef.nativeElement.parentElement?.children[0]?.children[0]
        ?.children[0]?.children[1];
    if (possibleOrderTableElement.tagName === "VDR-ORDER-TABLE") {
      possibleOrderTableElement.addEventListener("click", (event) => {
        this.handleOrderLineRowClickEvents(event);
      });
    }
  }

  handleOrderLineRowClickEvents(event: Event): void {
    const target = event.target;
    let isDeleteOrIncrementAction = false;
    if (target instanceof Element) {
      let nearestTr: HTMLTableRowElement | null;
      if (target.tagName === "TR") {
        nearestTr = target as HTMLTableRowElement;
      } else {
        if (
          target.tagName === "input".toUpperCase() &&
          target.getAttribute("type") === "number" &&
          target?.classList?.contains("draft-qty")
        ) {
          isDeleteOrIncrementAction = true;
        }
        const possibleDeleteButton = target.closest("button");
        if (possibleDeleteButton?.classList[0] === "icon-button") {
          isDeleteOrIncrementAction = true;
        }
        nearestTr = target.closest("tr");
      }
      // let's make sure it is not a surcharge,sub-total,shipping or total row
      if (
        nearestTr?.getAttribute("class") &&
        nearestTr?.getAttribute("class") !== "ng-star-inserted"
      ) {
        return;
      }
      const orderLinePosition = nearestTr?.rowIndex;
      if (
        orderLinePosition &&
        this.showConfigurationForm &&
        !this.modalIsShowing &&
        !isDeleteOrIncrementAction
      ) {
        const selectedDraftOrderLine =
          this.draftOrderLines[orderLinePosition - 1];
        if (this.isOrderLineConfigurable(selectedDraftOrderLine)) {
          this.selectedLine = selectedDraftOrderLine;
          this.launchOrderLineConfiguratorationModal();
        } else {
          this.launchNoConfigurableProductModal(selectedDraftOrderLine);
          this.selectedLine = undefined;
        }
      }
      this.cdr.markForCheck();
    }
    event.stopPropagation();
  }

  launchOrderLineConfiguratorationModal(): void {
    this.modalIsShowing = true;
    this.modalService
      .fromComponent(DraftOrderLineConfiguratorModalComponent, {
        size: "md",
        locals: {
          selectedLine: this.selectedLine,
          orderId: this.orderId,
          currencyCode: this.currencyCode,
        },
      })
      .subscribe((data) => {
        this.modalIsShowing = false;
        this.cdr.markForCheck();
      });
  }

  launchNoConfigurableProductModal(selectedDraftOrderLine: any): void {
    this.modalIsShowing = true;
    this.modalService
      .fromComponent(NotConfigurableProductComponent, {
        size: "md",
        locals: {
          productName: selectedDraftOrderLine.productVariant.product.name,
        },
      })
      .subscribe((data) => {
        this.modalIsShowing = false;
        this.cdr.markForCheck();
      });
  }
}
