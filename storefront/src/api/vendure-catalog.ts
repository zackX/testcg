import type { Collection, Product } from "../model.ts";
import {
  CollectionFieldsFragment,
  ProductFieldsFragment,
} from "./graphql/generated.ts";
import { vendureClient } from "./graphql/vendure-client.ts";
import { Cache } from "./util/cache.ts";
import {
  createCollectionTree,
  deduplicate,
  getBreadcrumbsForCollection,
  getBreadcrumbsForProduct,
  getChildCollection,
  getPathForBreadcrumb,
  getPriceRange,
} from "./util/catalog-util.ts";

interface CatalogData {
  products: Product[];
  collectionTree: Collection[];
  allCollections: Collection[];
}

/**
 * Fetch all collections from remote
 */
async function fetchAllCollections(): Promise<Collection[]> {
  const {
    data: { collections },
  } = await vendureClient.AllCollections();
  const collectionsWithPath = collections.items.map((c) => ({
    ...c,
    path: `${getPathForBreadcrumb(c.breadcrumbs)}/`, // Make sure it has trailing slash
  }));
  const collectionsWithPathAndBreadcrumb = collectionsWithPath.map((c) => ({
    ...c,
    breadcrumbs: getBreadcrumbsForCollection(c, collectionsWithPath),
    // Get full child collections based on id's of children
    children: c.children?.map((child) =>
      getChildCollection(child, collectionsWithPath),
    ),
  }));
  return collectionsWithPathAndBreadcrumb as Collection[];
}

/**
 * Fetch all products from remote
 */
async function fetchAllProducts(
  allCollections: Collection[],
): Promise<Product[]> {
  const products: ProductFieldsFragment[] = [];
  let hasMore = true;
  let page = 1;
  let skip = 0;
  const take = 500;
  while (hasMore) {
    const {
      data: { products: productList },
    } = await vendureClient.AllProducts({
      options: {
        skip,
        take,
      },
    });
    products.push(...productList.items);
    skip = page * take;
    page++;
    hasMore = productList.totalItems > products.length;
  }
  // Set product path
  const productsWithPath = products.map((p) => {
    const parentPath = p.primaryCollection
      ? getPathForBreadcrumb(p.primaryCollection.breadcrumbs)
      : "";
    return {
      ...p,
      path: `${parentPath}/${p.slug}/`,
    };
  });
  // Set breadcrumbs and prices
  return productsWithPath.map((p) => {
    const { retailWithTax, tradeWithTax } = getLowestPrices(p);
    return {
      ...p,
      breadcrumbs: getBreadcrumbsForProduct(p, allCollections),
      retailPriceWithTax: retailWithTax,
      tradePriceWithTax: tradeWithTax,
    };
  });
}

async function populateCache(): Promise<CatalogData> {
  console.log("Loading Vendure products and collections");
  const collections = await fetchAllCollections();
  const products = await fetchAllProducts(collections);
  console.log(
    `Loaded ${products.length} products and ${collections.length} collections from Vendure`,
  );
  return {
    collectionTree: createCollectionTree(collections),
    allCollections: collections,
    products,
  };
}

/**
 * All products and collections from Vendure in a cached catalog
 */
export const catalog = new Cache(populateCache);

/**
 * Find a collection by slug
 */
export async function getCollectionBySlug(
  slug: string,
): Promise<Collection | undefined> {
  return (await catalog.get()).allCollections.find((c) => c.slug === slug);
}

/**
 * Find a collection by array of slug
 */
export async function getCollectionsBySlug(
  slugs: string[],
): Promise<Collection[]> {
  return (await catalog.get()).allCollections.filter(
    (c) => slugs?.includes(c.slug),
  );
}

export async function getProduct(slug: string): Promise<Product | undefined> {
  return (await catalog.get()).products.find((p) => p.slug === slug);
}

/**
 * Get products with facet 'popular-product'
 */
export async function getPopularProducts(): Promise<Product[]> {
  return (await catalog.get()).products.filter((p) =>
    p.facetValues.some((fv) => fv.code === "popular-product"),
  );
}

/**
 * Get products of collection based on `collection.variants`
 */
export async function getProductsForCollection(
  collection: CollectionFieldsFragment,
): Promise<Product[]> {
  const allProducts = (await catalog.get()).products;
  let productsPerCollection = collection.productVariants.items
    .map((variant) =>
      allProducts.find((product) => product.id === variant.product.id),
    )
    .filter((product): product is Product => !!product);
  productsPerCollection = deduplicate(productsPerCollection);
  return productsPerCollection;
}

/**
 * Get primary collection of a product
 */
export function getPrimaryCollection(
  product: ProductFieldsFragment,
  allCollections: Collection[],
): CollectionFieldsFragment | undefined {
  return allCollections.find((c) => c.id === product.primaryCollection?.id);
}

export function getLowestPrices(product: ProductFieldsFragment): {
  retailWithTax: number;
  tradeWithTax: number;
} {
  const isNumber = (price: number | undefined): price is number => {
    return !!price;
  };
  const retailPrices = product.variants
    .map((v) => v.defaultConfiguratorPrice?.retailWithTax)
    .filter(isNumber);
  const tradePrices = product.variants
    .map((v) => v.defaultConfiguratorPrice?.tradeWithTax)
    .filter(isNumber);
  let retailPrice = retailPrices.length ? Math.min(...retailPrices) : undefined;
  let tradePrice = tradePrices.length ? Math.min(...tradePrices) : undefined;
  if (!retailPrice || !tradePrice) {
    // If no price is set, use the lowest price of the product variants
    const { lowestPrice } = getPriceRange(product);
    retailPrice = lowestPrice;
    tradePrice = lowestPrice;
  }
  return {
    retailWithTax: retailPrice,
    tradeWithTax: tradePrice,
  };
}
