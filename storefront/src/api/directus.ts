import { GraphQLClient, gql } from "graphql-request";

export interface Blog {
  id: number;
  title: string;
  slug: string;
  metaTitle: string;
  metaKeywords: string;
  metaDescription: string;
  publishedAt: string;
  featuredImage?: {
    id: number;
  };
  featuredImageUrl?: string;
  content: string;
  categories: string;
  tags: string;
}

export interface StaticPage {
  id: number;
  title: string;
  slug: string;
  content: string;
}

export const directus = new GraphQLClient(
  `${import.meta.env.DIRECTUS_HOST}/graphql`,
);

let blogs: Blog[] | undefined;
let getAllBlogsPromise: Promise<Blog[]> | undefined;

let staticPages: StaticPage[] | undefined;
let getAllStaticPromise: Promise<StaticPage[]> | undefined;

/**
 * Get all blogs
 */
export async function getBlogs(): Promise<Blog[]> {
  if (blogs) {
    return blogs;
  } else if (getAllBlogsPromise) {
    const result = await getAllBlogsPromise;
    return addFeaturedImageUrl(result);
  } else {
    const getBlogsPromise = directus
      .request(allBlogsQuery, {
        limit: 1000,
        sort: ["-publishedAt", "-date_created"],
      })
      .then((data) => data.cg_blogs);
    getAllBlogsPromise = getBlogsPromise;
    const result = await getAllBlogsPromise;
    if (result.length === 1000) {
      throw Error(
        `Too many blogs, we are only able to handle 1000 blogs at the moment!`,
      );
    }

    return addFeaturedImageUrl(result);
  }
}

export async function getBlog(slug: string): Promise<Blog> {
  const blogs = await getBlogs();
  const blog = blogs.find((blog) => blog.slug === slug);
  if (!blog) {
    throw new Error(`Blog with slug ${slug} not found`);
  }
  return blog;
}

export async function getStatic(): Promise<StaticPage[]> {
  if (staticPages) {
    return staticPages;
  } else if (getAllStaticPromise) {
    return await getAllStaticPromise;
  } else {
    const getStaticPromise = directus
      .request(getStaticPages)
      .then((data) => data.cg_static_pages);
    getAllStaticPromise = getStaticPromise;
    const result = await getAllStaticPromise;

    return result;
  }
}

function addFeaturedImageUrl(blogs: Blog[]): Blog[] {
  return blogs.map((blog) => {
    const imageId = (blog.featuredImage as any | undefined)?.id;
    return {
      ...blog,
      featuredImageUrl: imageId
        ? `${import.meta.env.DIRECTUS_HOST}/assets/${imageId}?key=default`
        : undefined,
    };
  });
}

const allBlogsQuery = gql`
  query ($limit: Int, $sort: [String]) {
    cg_blogs(limit: $limit, sort: $sort) {
      id
      title
      slug
      metaTitle
      metaKeywords
      metaDescription
      publishedAt
      featuredImage {
        id
      }
      content
      categories
      tags
    }
  }
`;

const getStaticPages = gql`
  query {
    cg_static_pages {
      id
      title
      slug
      content
    }
  }
`;
