import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { StringCustomFieldConfig } from "@vendure/common/lib/generated-types";
import { FormInputComponent } from "@vendure/admin-ui/core";

@Component({
  selector: "draft-order-line-empty-ui",
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraftOrderLineEmptyUI
  implements FormInputComponent<StringCustomFieldConfig>
{
  isListInput?: boolean | undefined;
  readonly: boolean;
  formControl: FormControl;
  config: StringCustomFieldConfig;
  constructor(private readonly cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.formControl.value) {
      return;
    }
    if (this.config.type === "string") {
      this.formControl.setValue("");
    } else if (this.config.type === "float" || this.config.type === "int") {
      this.formControl.setValue(0);
    } else {
      throw new Error(`Unexpectd type "${this.config.type}"`);
    }
  }
}
