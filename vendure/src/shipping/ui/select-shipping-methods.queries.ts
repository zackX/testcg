import { gql } from "graphql-tag";
import { ORDER_DETAIL_FRAGMENT } from "@vendure/admin-ui/core";
export const SET_SHIPPING_METHODS = gql`
  mutation SetShippingMethodMutation($shippingMethodId: [ID!]!, $orderId: ID!) {
    setOrderShippingMethod(
      shippingMethodId: $shippingMethodId
      orderId: $orderId
    ) {
      ... on Order {
        ...OrderDetail
      }
      ... on ErrorResult {
        message
      }
    }
  }
  ${ORDER_DETAIL_FRAGMENT}
`;
