import { SimpleGraphQLClient } from "@vendure/testing";
import { CREATE_SHIPPING_METHOD } from "./queries";
import {
  defaultShippingCalculator,
  defaultShippingEligibilityChecker,
} from "@vendure/core";
import { turnaroundShippingCalculator } from "../src/shipping/api/calculators/turnaround-shipping-calculator";
import { weightAndCountryChecker } from "@pinelab/vendure-plugin-shipping-extensions";

export interface TurnaroundShippingCalculatorArgValues {
  taxRate: number;
  percentageOfCart: number;
  minimumPrice: number;
}

export interface WeightBasedShippingCalculatorArgValues {
  taxRate: number;
  minimumPrice: number;
  pricePerGrams: number;
}

export interface WeightBasedShippingCheckerArgValues {
  minWeight: number;
  maxWeight: number;
  countries: string[];
  excludeCountries: string[];
}

export async function createTurnaroundShippingMethod(
  adminClient: SimpleGraphQLClient,
  options: TurnaroundShippingCalculatorArgValues,
  threeDayTurnaroundShippingMethodCode: string,
  shippingMethodName: string,
) {
  const res = await adminClient.query(CREATE_SHIPPING_METHOD, {
    input: {
      code: threeDayTurnaroundShippingMethodCode,
      checker: {
        code: defaultShippingEligibilityChecker.code,
        arguments: [{ name: "orderMinimum", value: "0" }],
      },
      calculator: {
        code: turnaroundShippingCalculator.code,
        arguments: [
          {
            name: "taxRate",
            value: String(options.taxRate),
          },
          {
            name: "percentageOfCart",
            value: String(options.percentageOfCart),
          },
          {
            name: "minimumPrice",
            value: String(options.minimumPrice),
          },
        ],
      },
      fulfillmentHandler: "manual-fulfillment",
      customFields: {},
      translations: [
        {
          languageCode: "en",
          name: shippingMethodName,
          description: "",
          customFields: {},
        },
      ],
    },
  });
  return res.createShippingMethod;
}

//FIX ME this and the anove mthod should be calling another createShippingMethod function
export async function createShippingMethodWithWeightChecker(
  adminClient: SimpleGraphQLClient,
  checkerArgs: WeightBasedShippingCheckerArgValues,
  shippingMethodCode: string,
  shippingMethodName: string,
) {
  const res = await adminClient.query(CREATE_SHIPPING_METHOD, {
    input: {
      code: shippingMethodCode,
      checker: {
        code: weightAndCountryChecker.code,
        arguments: [
          { name: "minWeight", value: String(checkerArgs.minWeight) },
          { name: "maxWeight", value: String(checkerArgs.maxWeight) },
          { name: "countries", value: JSON.stringify(checkerArgs.countries) },
          {
            name: "excludeCountries",
            value: JSON.stringify(checkerArgs.excludeCountries),
          },
        ],
      },
      calculator: {
        code: defaultShippingCalculator.code,
        arguments: [
          {
            name: "rate",
            value: String("123"),
          },
          {
            name: "includesTax",
            value: String("auto"),
          },
          {
            name: "taxRate",
            value: String("0"),
          },
        ],
      },
      fulfillmentHandler: "manual-fulfillment",
      customFields: {},
      translations: [
        {
          languageCode: "en",
          name: shippingMethodName,
          description: "",
          customFields: {},
        },
      ],
    },
  });
  return res.createShippingMethod;
}
