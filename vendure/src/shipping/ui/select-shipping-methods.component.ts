import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable, switchMap, combineLatest } from "rxjs";
import { FormGroup } from "@angular/forms";
import {
  DataService,
  CustomDetailComponent,
  SharedModule,
  DraftOrderEligibleShippingMethodsQuery,
  ModalService,
} from "@vendure/admin-ui/core";
import { SelectShippingMethodDialogComponent } from "@vendure/admin-ui/order";
import { take } from "rxjs/operators";
import { SET_SHIPPING_METHODS } from "./select-shipping-methods.queries";

@Component({
  template: `
    <vdr-card title="Shipping and Turnaround">
      <button class="button-small" (click)="setNonTurnaroundShippingMethod()">
        {{ "order.set-shipping-method" | translate }}
      </button>
      <span
        *ngFor="
          let shippingLine of filterNonTurnaroundShippingMethods(
            (entity$ | async).shippingLines
          )
        "
      >
        {{ shippingLine.shippingMethod.name }}
      </span>
      <br />
      <br />
      <button class="button-small" (click)="setTurnaroundShippingMethod()">
        Set turnaround shipping method
      </button>
      <span
        *ngFor="
          let shippingLine of filterTurnaroundShippingMethods(
            (entity$ | async).shippingLines
          )
        "
      >
        {{ shippingLine.shippingMethod.name }}
      </span>
    </vdr-card>
  `,
  standalone: true,
  imports: [SharedModule],
  styleUrls: ["./select-shipping-methods.component.scss"],
})
export class SelectShippineMethodsComponent
  implements CustomDetailComponent, OnInit
{
  entity$: Observable<any>;
  detailForm: FormGroup;
  selectedNonTurnaroundShippingMethodId: string;
  selectedTurnaroundShippingMethodId: string;
  eligibleShippingMethods$: Observable<
    DraftOrderEligibleShippingMethodsQuery["eligibleShippingMethodsForDraftOrder"]
  >;

  constructor(
    private readonly dataService: DataService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly modalService: ModalService,
  ) {}

  ngOnInit(): void {
    const coreSetShippingMethodButton = document.querySelectorAll(
      ".button-small.mr-2 ~ .button-small",
    );
    if (coreSetShippingMethodButton?.length) {
      (coreSetShippingMethodButton[0] as HTMLButtonElement).disabled = true;
    } else {
      throw new Error(
        "vendure core draft-order-detail page set-shipping-method button couldn't be located",
      );
    }
    this.entity$.subscribe((entity) => {
      const configuedTurnaroundShippingMethods =
        this.filterTurnaroundShippingMethods(entity.shippingLines);
      const configuedNonTurnaroundShippingMethods =
        this.filterNonTurnaroundShippingMethods(entity.shippingLines);
      if (configuedTurnaroundShippingMethods?.length) {
        this.selectedTurnaroundShippingMethodId =
          configuedTurnaroundShippingMethods[0].shippingMethod.id;
      }
      if (configuedNonTurnaroundShippingMethods?.length) {
        this.selectedNonTurnaroundShippingMethodId =
          configuedNonTurnaroundShippingMethods[0].shippingMethod.id;
      }
    });

    this.eligibleShippingMethods$ = this.entity$.pipe(
      switchMap((order) =>
        this.dataService.order
          .getDraftOrderEligibleShippingMethods(order.id)
          .mapSingle(
            ({ eligibleShippingMethodsForDraftOrder }) =>
              eligibleShippingMethodsForDraftOrder,
          ),
      ),
    );
  }

  showDialog(
    eligibleShippingMethodsFilterCallback: (methods: any[]) => any[],
    setResultCallback: (result: string) => void,
    currentSelectionId: string,
  ): void {
    combineLatest(this.entity$, this.eligibleShippingMethods$)
      .pipe(
        take(1),
        switchMap(([order, methods]) =>
          this.modalService.fromComponent(SelectShippingMethodDialogComponent, {
            locals: {
              eligibleShippingMethods:
                eligibleShippingMethodsFilterCallback(methods),
              currencyCode: order.currencyCode,
              currentSelectionId,
            },
          }),
        ),
      )
      .subscribe((result) => {
        if (result) {
          setResultCallback(result);
          this.setShippingMethods();
        }
      });
  }

  setNonTurnaroundShippingMethod(): void {
    this.showDialog(
      (methods) => methods.filter((m) => !this.isTurnaroundShippingMethod(m)),
      (result) => (this.selectedNonTurnaroundShippingMethodId = result),
      this.selectedNonTurnaroundShippingMethodId,
    );
  }

  setTurnaroundShippingMethod(): void {
    this.showDialog(
      (methods) => methods.filter((m) => this.isTurnaroundShippingMethod(m)),
      (result) => (this.selectedTurnaroundShippingMethodId = result),
      this.selectedTurnaroundShippingMethodId,
    );
  }

  isTurnaroundShippingMethod(method: any): boolean {
    return method?.code?.includes("turnaround");
  }

  filterTurnaroundShippingMethods(shippingLines: any[]): any[] {
    return shippingLines.filter((shippingLine) =>
      this.isTurnaroundShippingMethod(shippingLine.shippingMethod),
    );
  }

  filterNonTurnaroundShippingMethods(shippingLines: any[]): any[] {
    return shippingLines.filter(
      (shippingLine) =>
        !this.isTurnaroundShippingMethod(shippingLine.shippingMethod),
    );
  }

  setShippingMethods(): void {
    const orderId = this.activatedRoute.snapshot.paramMap.get("id");
    const shippingMethodId = [
      this.selectedNonTurnaroundShippingMethodId,
      this.selectedTurnaroundShippingMethodId,
    ].filter((v) => v !== null && v !== undefined);
    this.dataService
      .mutate(SET_SHIPPING_METHODS, { shippingMethodId, orderId })
      .subscribe();
  }
}
