import { LoadDataFn } from "@pinelab/vendure-plugin-picklist/dist/load-data-fn";
import {
  EntityHydrator,
  Injector,
  Order,
  RequestContext,
  UserInputError,
  translateEntity,
} from "@vendure/core";
import { resolveHumanReadableOptions } from "../configurator/util/resolve-human-readable-options";
import { isTurnaroundMethod } from "../shipping/util/turnaround";
import handlebars from "handlebars";

export const picklistLoadDataFn: LoadDataFn = async (
  ctx: RequestContext,
  injector: Injector,
  order: Order,
): Promise<any> => {
  handlebars.registerHelper("inc", function (value: number) {
    return value + 1;
  });
  order.lines.forEach((line) => {
    line.productVariant = translateEntity(
      line.productVariant,
      ctx.languageCode,
    );
  });
  const entityHydrator = injector.get(EntityHydrator);
  await entityHydrator.hydrate(ctx, order, {
    relations: ["lines.productVariant.product", "shippingLines.shippingMethod"],
  });
  if (!order.customer?.emailAddress) {
    throw new UserInputError(
      `Order '${order.code}' doesn't have a customer.email set!`,
    );
  }
  // Set shipping as billing address if no billing address is set
  const billingAddress = order.billingAddress.postalCode
    ? order.billingAddress
    : order.shippingAddress;
  return {
    orderDate: order.orderPlacedAt
      ? new Intl.DateTimeFormat("nl-NL").format(order.orderPlacedAt)
      : new Intl.DateTimeFormat("nl-NL").format(order.updatedAt),
    customerEmail: order.customer.emailAddress,
    turnaroundMethod: order.shippingLines.find((line) =>
      isTurnaroundMethod(line.shippingMethod),
    )?.shippingMethod.name,
    shippingMethod: order.shippingLines.find(
      (line) => !isTurnaroundMethod(line.shippingMethod),
    )?.shippingMethod.name,
    shippingDate:
      order.customFields.shippingDate &&
      new Intl.DateTimeFormat("en-GB", {
        weekday: "short",
        day: "numeric",
        month: "long",
        year: "numeric",
      }).format(order.customFields.shippingDate),
    order: {
      ...order,
      billingAddress,
      lines: order.lines.map((line) => {
        line.customFields.selectedConfiguratorOptions =
          resolveHumanReadableOptions(
            ctx,
            line.productVariant.product,
            line.customFields.selectedConfiguratorOptions,
          ) as any;
        return line;
      }),
    },
  };
};
