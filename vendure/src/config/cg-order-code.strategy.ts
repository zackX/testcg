import {
  DefaultOrderCodeStrategy,
  OrderCodeStrategy,
  RequestContext,
} from "@vendure/core";
import dayjs from "dayjs";

/**
 * Generates id's like CG-140923-J7HG8
 */
export class CGOrderCodeStrategy
  extends DefaultOrderCodeStrategy
  implements OrderCodeStrategy
{
  generate(ctx: RequestContext): string {
    const alphanumericCode = super.generate(ctx).slice(0, 5);
    // Do magic here
    return `CG-${dayjs(new Date()).format("DDMMYY")}-${alphanumericCode}`;
  }
}
