import {
  ExportInput,
  ExportStrategy,
} from "@pinelab/vendure-plugin-order-export";
import { InternalServerError, Logger, Injector } from "@vendure/core";
import { ModuleRef } from "@nestjs/core";
import { createObjectCsvWriter } from "csv-writer";
import os from "os";
import path from "path";
import { getConfigurableOrderTotalWeightInGrams } from "../config/configurable-item-weight-calculator";
import { appContext } from "../index";

const loggerCtx = "ShippingOrderExport";

interface ShippingRow {
  "Order Number": string;
  "Company name": string;
  "Contact Name": string;
  "Contact Telephone": string;
  "Contact Email": string;
  Postcode: string;
  Address1: string;
  Address2: string;
  Address3: string;
  City: string;
  State: string;
  Country: string;
  Weight: string | number;
  Service: string;
  "Shipping Date": string;
}

export class ShippingOrderExport implements ExportStrategy {
  readonly name = "Shipping";
  readonly contentType = "text/csv";
  readonly fileExtension = "csv";

  async createExportFile({
    ctx,
    startDate,
    endDate,
    orderService,
  }: ExportInput): Promise<string> {
    const orders = await orderService.findAll(
      ctx,
      {
        filter: {
          updatedAt: {
            between: {
              start: startDate,
              end: endDate,
            },
          },
          orderPlacedAt: {
            isNull: false,
          },
        },
        take: 500,
      },
      [
        "customer",
        "lines",
        "lines.productVariant",
        "lines.productVariant.product",
        "shippingLines",
        "shippingLines.shippingMethod",
      ],
    );
    if (orders.totalItems === 500) {
      // This is just a sample strategy, so this is oke
      throw new InternalServerError(
        "Too many orders. Max 500 orders can currently be exported.",
      );
    }
    Logger.info(`Exporting ${orders.items.length} orders`, loggerCtx);
    const rows: ShippingRow[] = [];
    const injector = new Injector(appContext.get(ModuleRef)); // FIXME Export strategy should receive Injector
    for (const order of orders.items) {
      const shippingDate = order.customFields?.shippingDate
        ? order.customFields.shippingDate.toISOString()
        : "";
      let totalOrderWeight = await getConfigurableOrderTotalWeightInGrams(
        ctx,
        order,
        injector,
      );
      totalOrderWeight = Math.round(totalOrderWeight);
      rows.push({
        "Order Number": order.code,
        "Company name": order.shippingAddress.company ?? "",
        "Contact Name": order.shippingAddress.fullName ?? "",
        "Contact Telephone": order.customer?.phoneNumber ?? "",
        "Contact Email": order.customer?.emailAddress ?? "",
        Postcode: order.shippingAddress.postalCode ?? "",
        Address1: order.shippingAddress.streetLine1 ?? "",
        Address2: order.shippingAddress.streetLine2 ?? "",
        Address3: order.shippingAddress.customFields?.addressline3 ?? "",
        City: order.shippingAddress.city ?? "",
        State: order.shippingAddress.province ?? "",
        Country: order.shippingAddress.countryCode ?? "",
        Weight: totalOrderWeight,
        Service: "",
        "Shipping Date": shippingDate,
      });
    }
    // Write to file
    const fileName = `${new Date().getTime()}-${startDate.getTime()}-${endDate.getTime()}.${
      this.fileExtension
    }`;
    const exportFile = path.join(os.tmpdir(), fileName);
    const csvWriter = createObjectCsvWriter({
      path: exportFile,
      header: Object.keys(rows[0]).map((key) => ({ id: key, title: key })),
    });
    await csvWriter.writeRecords(rows);
    return exportFile;
  }
}
