export function validateEmail(email: string): boolean {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
}

export function isRequired(value?: string | null): boolean {
  return value?.trim() !== "";
}

export function validatePhoneNumber(phoneNumber: string): boolean {
  const re = /^[0-9]{10}$/;
  return re.test(phoneNumber);
}

export function isStrongPassword(password: string): boolean {
  const re = /^.{8,50}$/;
  return re.test(password);
}

export function isValidPhoneNumber(phoneNumber: string): boolean {
  const re = /^\d{5,}$/;
  return re.test(phoneNumber);
}
