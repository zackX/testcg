import { Component } from "@angular/core";
import { Dialog } from "@vendure/admin-ui/core";

@Component({
  selector: "not-configurable-product-modal",
  template: `
    <ng-template vdrDialogTitle>Not Configurable</ng-template>

    <p>The Product '{{ productName }}' is not configurable</p>

    <ng-template vdrDialogButtons>
      <button type="button" class="btn btn-primary" (click)="okay()">
        Okay
      </button>
    </ng-template>
  `,
})
export class NotConfigurableProductComponent implements Dialog<any> {
  productName: string;
  resolveWith: (result?: any) => void;

  okay(): void {
    this.resolveWith();
  }
}
