import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { SharedModule, CanDeactivateDetailGuard } from "@vendure/admin-ui/core";
import { GoogleSheetDataLoaderComponent } from "./google-sheet-data-loader/google-sheet-data-loader.component";

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: "google-sheet-data-loader",
        component: GoogleSheetDataLoaderComponent,
        canDeactivate: [CanDeactivateDetailGuard],
        data: {
          breadcrumb: () => {
            return [
              {
                label: "Google Sheet Data Loader",
                link: ["/extensions", "settings", "google-sheet-data-loader"],
              },
            ];
          },
        },
      },
    ]),
  ],
  declarations: [GoogleSheetDataLoaderComponent],
})
export class UiLazyModule {}
