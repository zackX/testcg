import { Injectable, OnModuleInit } from "@nestjs/common";
import {
  EventBus,
  ForbiddenError,
  Logger,
  Order,
  OrderService,
  OrderState,
  OrderStateTransitionError,
  OrderStateTransitionEvent,
  ProcessContext,
  RequestContext,
  isGraphQlErrorResult,
} from "@vendure/core";
import { filter } from "rxjs";

export const loggerCtx = "CustomStateTransitionService";

@Injectable()
export class CustomStateTransitionService implements OnModuleInit {
  constructor(
    private readonly orderService: OrderService,
    private readonly eventBus: EventBus,
    private readonly processContext: ProcessContext,
  ) {}

  onModuleInit(): void {
    this.eventBus
      .ofType(OrderStateTransitionEvent)
      .pipe(filter((event) => event.toState === "PaymentSettled"))
      .subscribe(({ ctx, order }) => {
        this.transitionAfterPayment(ctx, order).catch((err) => {
          Logger.error(
            `Failed to transition order after payment: ${err}`,
            loggerCtx,
          );
        });
      });
  }

  /**
   * Transition to the "AwaitingArtwork" state after payment has been settled,
   * or directly to AwaitingPrepress if artwork has already been received.
   */
  async transitionAfterPayment(
    ctx: RequestContext,
    order: Order,
  ): Promise<void> {
    let toState: OrderState = "AwaitingArtwork";
    if (order.customFields.artworkReceived) {
      // Skip awaiting artwork if artwork has already been received
      toState = "AwaitingPrepress";
    }
    const result = await this.orderService.transitionToState(
      ctx,
      order.id,
      toState,
    );
    if (isGraphQlErrorResult(result)) {
      Logger.error(
        `Failed to transition Order with id ${order.id} to "${toState}": ${result.errorCode}`,
        loggerCtx,
      );
      return;
    }
    Logger.info(
      `Transitioned Order '${order.code}' to "${result.state}"`,
      loggerCtx,
    );
  }

  async transitionOrderToAwaitingPrepress(
    ctx: RequestContext,
    orderCode: string,
    emailAddress: string,
  ): Promise<Order | OrderStateTransitionError | undefined> {
    const order = await this.orderService.findOneByCode(ctx, orderCode);
    if (order?.customer?.emailAddress === emailAddress) {
      return await this.orderService.transitionToState(
        ctx,
        order.id,
        "AwaitingPrepress",
      );
    }
    throw new ForbiddenError();
  }
}
