import fs from "fs";
import fetch from "node-fetch";

const site = process.argv[2];
const urlFile = process.argv[3];
const excludes = process.argv[4]?.split(",") || [];
const statusFile = "scripts/seo/data/" + process.argv[3] + "_status.csv";

(async () => {
  try {
    fs.unlinkSync(statusFile);
  } catch {}

  const paths = fs
    .readFileSync("scripts/seo/data/" + urlFile, "utf8")
    .split("\n");
  for (const path of paths) {
    const shouldSkip = excludes.some((exclude) => path.indexOf(exclude) > -1);
    if (shouldSkip) {
      continue;
    }
    const url = site + path;
    const res = await fetch(url);
    if (!res.ok) {
      console.error(res.status, path);
      fs.appendFile(statusFile, `${path}, ${res.status}\n`, function (err) {
        if (err) throw err;
      });
    }
  }
})();
