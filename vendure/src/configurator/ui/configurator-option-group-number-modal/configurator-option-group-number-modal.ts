import { Component } from "@angular/core";
import { Dialog } from "@vendure/admin-ui/core";
import { FormGroup } from "@angular/forms";
import { ConfiguratorOptionGroup } from "../generated/generated-graphql-types";
@Component({
  selector: "app-component-overview",
  templateUrl: "./configurator-option-group-number-modal.html",
})
export class GroupOptionNumberModalComponent
  implements Dialog<ConfiguratorOptionGroup>
{
  optionGroupFormGroup: FormGroup;
  resolveWith: (result?: ConfiguratorOptionGroup) => void;

  validateOptionGroupNumberModalForm(): boolean {
    const idControl = this.optionGroupFormGroup.get("id");
    const nameControl = this.optionGroupFormGroup.get("name");
    const minControl = this.optionGroupFormGroup.get("min");
    const maxControl = this.optionGroupFormGroup.get("max");
    if (!idControl || !nameControl || !minControl || !maxControl) {
      throw new Error("Missing input component");
    }
    return (
      idControl.valid &&
      nameControl.valid &&
      minControl.valid &&
      maxControl.valid
    );
  }
}
