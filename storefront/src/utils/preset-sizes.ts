const presetSizes = [
  { name: "Custom Size", height: 1000, width: 1000 },
  { name: "A0 ~ 841 mm x 1189 mm", height: 841, width: 1189 },
  { name: "A1 ~ 594 mm x 841 mm", height: 594, width: 841 },
  { name: "A2 ~ 420 mm x 594 mm", height: 420, width: 594 },
  { name: "A3 ~ 297 mm x 420 mm", height: 297, width: 420 },
  { name: "A4 ~ 210 mm x 297 mm", height: 210, width: 297 },
];

export default presetSizes;
