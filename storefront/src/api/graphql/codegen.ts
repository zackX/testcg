import type { CodegenConfig } from "@graphql-codegen/cli";

/**
 * Configuration for graphql code generation
 */
const config: CodegenConfig = {
  overwrite: true,
  schema: "http://localhost:3050/shop-api",
  documents: ["src/api/graphql/*.graphql"],
  generates: {
    "./src/api/graphql/generated.ts": {
      plugins: [
        "typescript",
        "typescript-operations",
        "typescript-graphql-request",
      ],
    },
  },
  config: {
    rawRequest: true,
    extensionsType: "unknown",
    scalars: {
      ID: "number | string",
      Money: "number",
    },
  },
};

export default config;
