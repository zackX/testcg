import {
  LanguageCode,
  RequestContext,
  ShippingCalculator,
  TaxSetting,
} from "@vendure/core";

export const turnaroundShippingCalculator = new ShippingCalculator({
  code: "turnaround-shipping-calculator",
  description: [
    { languageCode: LanguageCode.en, value: "Turnaround Shipping Calculator" },
  ],
  args: {
    percentageOfCart: {
      type: "float",
      ui: { component: "number-form-input", suffix: "%", min: 0, max: 100 },
      label: [{ languageCode: LanguageCode.en, value: "Percentage of Cart" }],
    },
    minimumPrice: {
      type: "float",
      ui: { component: "currency-form-input", min: 0 },
      label: [{ languageCode: LanguageCode.en, value: "Minimum Price" }],
    },
    includesTax: {
      type: "string",
      defaultValue: TaxSetting.auto,
      ui: {
        component: "select-form-input",
        options: [
          {
            label: [{ languageCode: LanguageCode.en, value: "Includes tax" }],
            value: TaxSetting.include,
          },
          {
            label: [{ languageCode: LanguageCode.en, value: "Excludes tax" }],
            value: TaxSetting.exclude,
          },
          {
            label: [
              {
                languageCode: LanguageCode.en,
                value: "Auto (based on Channel)",
              },
            ],
            value: TaxSetting.auto,
          },
        ],
      },
      label: [{ languageCode: LanguageCode.en, value: "Price includes tax" }],
    },
    taxRate: {
      type: "float",
      ui: { component: "number-form-input", suffix: "%", min: 0, max: 100 },
      label: [{ languageCode: LanguageCode.en, value: "Tax rate" }],
    },
  },
  calculate: async (ctx, order, args) => {
    let rateBeforeTax = order.subTotal * (args.percentageOfCart / 100);
    if (rateBeforeTax < args.minimumPrice) {
      rateBeforeTax = args.minimumPrice;
    }
    return {
      price: rateBeforeTax,
      priceIncludesTax: getPriceIncludesTax(ctx, args.includesTax as any),
      taxRate: args.taxRate,
    };
  },
});

function getPriceIncludesTax(
  ctx: RequestContext,
  setting: TaxSetting,
): boolean {
  switch (setting) {
    case TaxSetting.auto:
      return ctx.channel.pricesIncludeTax;
    case TaxSetting.exclude:
      return false;
    case TaxSetting.include:
      return true;
  }
}
