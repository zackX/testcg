import {
  ID,
  InternalServerError,
  Logger,
  Order,
  RequestContext,
  TransactionalConnection,
  idsAreEqual,
} from "@vendure/core";
import os from "os";
import path from "path";
import { createObjectCsvWriter } from "csv-writer";
import {
  ExportInput,
  ExportStrategy,
} from "@pinelab/vendure-plugin-order-export";
import { InvoiceEntity } from "@pinelab/vendure-plugin-invoices";
import { In } from "typeorm";
import { appContext } from "../";

const loggerCtx = "XeroOrderExport";

interface XeroRow {
  // This will be used for Customer Reference (Purchase Order Number)
  POAddressLine4?: string;
  ContactName: string;
  EmailAddress: string;
  POAddressLine1: string;
  POAddressLine2?: string;
  POAddressLine3?: string;
  POCity: string;
  PORegion?: string;
  POPostalCode: string;
  POCountry: string;
  InvoiceNumber: string | number;
  Reference?: string;
  InventoryItemCode: string;
  Description: string;
  Quantity: string | number;
  UnitAmount: string;
  Discount?: string;
  AccountCode: string;
  TaxType: string;
  TaxAmount: string;
  TrackingName1?: string;
  TrackingOption1?: string;
  TrackingName2?: string;
  TrackingOption2?: string;
  Currency: string;
  BrandingTheme?: string;
  InvoiceDate?: string;
  DueDate?: string;
  OrderCode: string;
}

export class XeroOrderExport implements ExportStrategy {
  readonly name = "Xero";
  readonly contentType = "text/csv";
  readonly fileExtension = "csv";

  async createExportFile({
    ctx,
    startDate,
    endDate,
    orderService,
  }: ExportInput): Promise<string> {
    const orders = await orderService.findAll(
      ctx,
      {
        filter: {
          orderPlacedAt: {
            between: {
              start: startDate,
              end: endDate,
            },
          },
          state: {
            notEq: "Cancelled",
          },
          // FIXME: this is just to filter out migrated orders. This can be removed after April 1 2024
          code: {
            contains: "CG-",
          },
        },
        take: 500,
      },
      [
        "customer",
        "lines",
        "lines.productVariant",
        "shippingLines",
        "shippingLines.shippingMethod",
        "payments",
      ],
    );
    if (orders.totalItems > 500) {
      throw new InternalServerError(
        "Too many orders. Max 500 orders can currently be exported.",
      );
    }
    const invoices = await this.getInvoicesForOrders(ctx, orders.items);
    Logger.info(`Exporting ${orders.items.length} orders`, loggerCtx);
    const rows: XeroRow[] = [];
    orders.items.forEach((order) => {
      const paymentMethod = order.payments.find((p) => p.state === "Settled")
        ?.method;
      // The base of each row is the same, but below we add the orderLine and shippingLine specific rows
      const baseRow = {
        POAddressLine4: order.code,
        ContactName: `${order.customer?.firstName} ${order.customer?.lastName}`,
        EmailAddress: order.customer?.emailAddress ?? "",
        POAddressLine1: order.shippingAddress.streetLine1 ?? "",
        POAddressLine2: order.shippingAddress.streetLine2 ?? "",
        POAddressLine3: order.shippingAddress.customFields?.addressline3 ?? "",
        POCity: order.shippingAddress.city ?? "",
        PORegion: order.shippingAddress.province ?? "",
        POPostalCode: order.shippingAddress.postalCode ?? "",
        POCountry: order.shippingAddress.country ?? "",
        InvoiceNumber:
          this.getMostRecentInvoiceNumber(invoices, order.id) ?? "",
        Reference: `${order.code} ${paymentMethod ?? "unknown-payment-method"}`,
        Currency: order.currencyCode,
        InvoiceDate: this.formatDateTime(order.orderPlacedAt),
        DueDate: this.formatDateTime(order.orderPlacedAt),
        OrderCode: order.code,
      };
      // Add order line
      order.lines.forEach((line) => {
        rows.push({
          ...baseRow,
          InventoryItemCode: line.productVariant.sku,
          Description: line.productVariant.name,
          Quantity: line.quantity,
          UnitAmount: this.formatCurrency(line.proratedUnitPrice),
          AccountCode: "0102",
          TaxType: this.getTaxType(line.taxRate),
          TaxAmount: this.formatCurrency(line.lineTax),
        });
      });
      // Add shipping line
      order.shippingLines.forEach((line) => {
        rows.push({
          ...baseRow,
          InventoryItemCode: line.shippingMethod.code,
          Description: line.shippingMethod.name,
          Quantity: 1,
          UnitAmount: this.formatCurrency(line.discountedPrice),
          AccountCode: "0103",
          TaxType: this.getTaxType(line.taxRate),
          TaxAmount: this.formatCurrency(
            line.discountedPriceWithTax - line.discountedPrice,
          ),
        });
      });
    });
    // Write to file
    const fileName = `${new Date().getTime()}-${startDate.getTime()}-${endDate.getTime()}.${
      this.fileExtension
    }`;
    const exportFile = path.join(os.tmpdir(), fileName);
    const csvWriter = createObjectCsvWriter({
      path: exportFile,
      header: Object.keys(rows[0]).map((key) => ({ id: key, title: key })),
    });
    await csvWriter.writeRecords(rows);
    return exportFile;
  }

  async getInvoicesForOrders(
    ctx: RequestContext,
    orders: Order[],
  ): Promise<InvoiceEntity[]> {
    // FIXME Export strategy should receive Injector
    const connection = appContext.get(TransactionalConnection);
    const invoices = await connection
      .getRepository(ctx, InvoiceEntity)
      .find({ where: { orderId: In(orders.map((o) => o.id)) } });
    return invoices;
  }

  getMostRecentInvoiceNumber(
    invoices: InvoiceEntity[],
    orderId: ID,
  ): number | undefined {
    const invoice = invoices
      .filter((i) => idsAreEqual(i.orderId, orderId))
      .sort((a, b) => b.invoiceNumber - a.invoiceNumber)[0];
    return invoice ? invoice.invoiceNumber : undefined;
  }

  // Formats date to DD/MM/YYYY
  private formatDateTime(date?: Date): string | undefined {
    if (!date) {
      return;
    }
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  }

  private formatCurrency(value: number): string {
    return (value / 100).toFixed(2);
  }

  private getTaxType(taxRate: number): string {
    if (taxRate === 0) {
      return "No VAT";
    } else if (!taxRate) {
      return "No VAT";
    } else if (taxRate === 20) {
      return "20% (VAT on Income)";
    } else {
      throw Error(`Invalid tax rate '${taxRate}'`);
    }
  }
}
