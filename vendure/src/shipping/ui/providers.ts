import { registerCustomDetailComponent } from "@vendure/admin-ui/core";
import { SelectShippineMethodsComponent } from "./select-shipping-methods.component";

export default [
  registerCustomDetailComponent({
    locationId: "draft-order-detail",
    component: SelectShippineMethodsComponent,
  }),
];
