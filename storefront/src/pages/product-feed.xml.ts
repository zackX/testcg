import type { APIContext } from "astro";
import { removeTrailingAndLeadingSlash } from "../api/util/catalog-util";
import { catalog } from "../api/vendure-catalog";
import { filters } from "../utils/filters";

const { products } = await catalog.get();

export async function get(context: APIContext): Promise<{ body: string }> {
  if (!context.site) {
    throw Error(
      `Can not generate product feed XML without site name. Please specify 'site' in your Astro config`,
    );
  }
  const domain = removeTrailingAndLeadingSlash(context.site?.href);
  const productXML = products
    .map((product) => {
      return `
        <item>
            <g:id>product_${product.id}</g:id>
            <g:title><![CDATA[${product.name}]]></g:title>
            <g:description><![CDATA[${product.description}]]></g:description>
            <g:link>${domain}${product.path}</g:link> 
            <g:image_link>${
              product.featuredAsset?.preview ?? product.featuredAsset?.thumbnail
            }
            </g:image_link> 
            <g:condition>new</g:condition>
            <g:availability>in stock</g:availability>
            <g:price>${filters.price(product.retailPriceWithTax)}</g:price>
            <g:gtin><![CDATA[${product.slug}-${product.id}]]></g:gtin>
            <g:mpn><![CDATA[${product.slug}-${product.id}]]></g:mpn>
            <g:google_product_category><![CDATA[${product.primaryCollection
              ?.name}]]></g:google_product_category>
        </item>
    `;
    })
    .join("\n");

  return {
    body: `<?xml version="1.0" encoding="UTF-8"?>
            <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
                <channel>
                    <title>Colour Graphics</title>
                    <description>Colour Graphics' Google Product Feed</description>
                    ${productXML}
                </channel>
            </rss>
    `,
  };
}
