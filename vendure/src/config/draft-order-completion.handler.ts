import { OrderStateTransitionEvent } from "@vendure/core";
import { EmailEventListener } from "@vendure/email-plugin";
import { mockOrderStateTransitionEvent } from "@vendure/email-plugin/lib/src/mock-events";

mockOrderStateTransitionEvent.fromState = "Draft";
mockOrderStateTransitionEvent.toState = "ArrangingPayment";

export const draftOrderCompletionHandler = new EmailEventListener(
  "draft-order-completion",
)
  .on(OrderStateTransitionEvent)
  .filter(
    (event) =>
      event.toState === "ArrangingPayment" &&
      event.fromState === "Draft" &&
      !!event.order.customer?.emailAddress,
  )
  .loadData(async ({ event, injector }) => {
    if (event.order.customFields.customerReference) {
      return {
        subject: `We've placed order #${event.order.code} for you`,
        bodyText: `We've placed order #${event.order.code} for you. Click the button below to upload artwork and view the order details.`,
        buttonText: "View Order",
      };
    }
    return {
      subject: `Order #${event.order.code} is awaiting payment`,
      bodyText: `Your order #${event.order.code} is awaiting payment.`,
      buttonText: "Make Payment",
    };
  })
  .setTemplateVars((event) => {
    return {
      order: event.order,
      ...event.data,
    };
  })
  .setRecipient((event) => event.order.customer?.emailAddress ?? "")
  .setFrom("{{ fromAddress }}")
  .setSubject("{{ subject }}")
  .setMockEvent(mockOrderStateTransitionEvent);
