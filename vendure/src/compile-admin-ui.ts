import { compileUiExtensions, setBranding } from "@vendure/ui-devkit/compiler";
import * as path from "path";
import { ConfiguratorPlugin } from "./configurator/configurator-plugin";
import { PullDataFromGoogleSheetPlugin } from "./pull-data-from-google-sheet/google-sheet-plugin";
import { WebhookPlugin } from "@pinelab/vendure-plugin-webhook";
import { PrimaryCollectionPlugin } from "@pinelab/vendure-plugin-primary-collection";
import { ProductDocumentsPlugin } from "./product-documents/plugin";
import { SimplifiedCollectionsPlugin } from "vendure-simplified-collections-plugin";
import { ShippingPlugin } from "./shipping/shipping.plugin";
import { InvoicePlugin } from "@pinelab/vendure-plugin-invoices";
import { PicklistPlugin } from "@pinelab/vendure-plugin-picklist";
import { OrderExportPlugin } from "@pinelab/vendure-plugin-order-export";

compileUiExtensions({
  outputPath: path.join(__dirname, "../__admin-ui"),
  extensions: [
    PrimaryCollectionPlugin.ui,
    WebhookPlugin.ui,
    ProductDocumentsPlugin.uiExtensions,
    SimplifiedCollectionsPlugin.uiExtensions,
    ShippingPlugin.uiExtensions,
    ConfiguratorPlugin.uiExtensions,
    PullDataFromGoogleSheetPlugin.uiExtensions,
    InvoicePlugin.ui,
    PicklistPlugin.ui,
    OrderExportPlugin.ui,
    setBranding({
      // The small logo appears in the top left of the screen
      smallLogoPath: path.join(__dirname, "admin-branding/logo.webp"),
      // The large logo is used on the login page
      largeLogoPath: path.join(__dirname, "admin-branding/logo.webp"),
      faviconPath: path.join(__dirname, "admin-branding/logo.webp"),
    }),
    {
      translations: {
        en: path.join(__dirname, "translations/en.json"),
      },
    },
  ],
})
  .compile?.()
  .then(() => {
    process.exit(0);
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
