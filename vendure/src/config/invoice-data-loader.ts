import {
  EntityHydrator,
  Injector,
  Order,
  RequestContext,
  translateEntity,
} from "@vendure/core";
import {
  LoadDataFn,
  CreditInvoiceInput,
} from "@pinelab/vendure-plugin-invoices";
import { resolveHumanReadableOptions } from "../configurator/util/resolve-human-readable-options";
import handlebars from "handlebars";

export const invoiceLoadDataFn: LoadDataFn = async (
  ctx: RequestContext,
  injector: Injector,
  order: Order,
  mostRecentInvoiceNumber?: number,
  shouldGenerateCreditInvoice?: CreditInvoiceInput,
): Promise<any> => {
  handlebars.registerHelper("inc", function (value: number) {
    return value + 1;
  });
  // Increase order number
  let newInvoiceNumber = mostRecentInvoiceNumber ?? 0;
  newInvoiceNumber += 1;
  const orderDate = new Intl.DateTimeFormat("nl-NL").format(order.updatedAt);
  order.lines.forEach((line) => {
    line.productVariant = translateEntity(
      line.productVariant,
      ctx.languageCode,
    );
  });
  const entityHydrator = injector.get(EntityHydrator);
  await entityHydrator.hydrate(ctx, order, {
    relations: [
      "lines.productVariant.product",
      "shippingLines.shippingMethod",
      "payments",
    ],
  });
  const payment = order.payments.find((payment) => payment.state === "Settled");
  // Set shipping as billing address if no billing address is set
  const billingAddress = order.billingAddress.postalCode
    ? order.billingAddress
    : order.shippingAddress;
  if (!shouldGenerateCreditInvoice) {
    // Normal debit invoice
    return {
      orderDate,
      invoiceNumber: newInvoiceNumber,
      payment,
      order: {
        ...order,
        billingAddress,
        lines: order.lines.map((line) => {
          if (
            line.customFields.selectedConfiguratorOptions?.length &&
            typeof line.customFields.selectedConfiguratorOptions[0] === "string"
          ) {
            line.customFields.selectedConfiguratorOptions =
              resolveHumanReadableOptions(
                ctx,
                line.productVariant.product,
                line.customFields.selectedConfiguratorOptions,
              ) as any;
          }
          return line;
        }),
      },
    };
  }
  // Create credit invoice
  const { previousInvoice, reversedOrderTotals } = shouldGenerateCreditInvoice;
  return {
    orderDate,
    invoiceNumber: newInvoiceNumber,
    isCreditInvoice: true,
    // Reference to original invoice because this is a credit invoice
    originalInvoiceNumber: previousInvoice.invoiceNumber,
    order: {
      ...order,
      billingAddress,
      total: reversedOrderTotals.total,
      totalWithTax: reversedOrderTotals.totalWithTax,
      taxSummary: reversedOrderTotals.taxSummaries,
      lines: order.lines.map((line) => {
        if (
          line.customFields.selectedConfiguratorOptions?.length &&
          typeof line.customFields.selectedConfiguratorOptions[0] === "string"
        ) {
          line.customFields.selectedConfiguratorOptions =
            resolveHumanReadableOptions(
              ctx,
              line.productVariant.product,
              line.customFields.selectedConfiguratorOptions,
            ) as any;
        }
        return line;
      }),
    },
  };
};
