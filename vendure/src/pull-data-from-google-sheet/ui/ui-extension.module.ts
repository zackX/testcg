import { NgModule } from "@angular/core";
import { addNavMenuItem } from "@vendure/admin-ui/core";
@NgModule({
  declarations: [],
  providers: [
    addNavMenuItem(
      {
        id: "google-sheet-data-loader",
        label: "Google Sheet Data Loader",
        routerLink: ["/extensions/settings/google-sheet-data-loader"],
        icon: "download",
      },
      "settings",
      "settings",
    ),
  ],
})
export class UiExtensionModule {}
