import { gql } from "graphql-tag";

export const shopApiExtension = gql`
  extend type Mutation {
    transitionOrderToAwaitingPrepress(
      orderCode: String!
      emailAddress: String!
    ): TransitionOrderToStateResult
  }
`;
